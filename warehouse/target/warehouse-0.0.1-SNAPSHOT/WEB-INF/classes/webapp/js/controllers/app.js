var mainApp = angular.module('mainApp', ['ngSanitize', 'ngRoute','userServices','ui.bootstrap','ngAnimate']); //, 'spring-security-csrf-token-interceptor']);

mainApp.controller('homeControl', [function() {}]);

mainApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
	when('/', {
		templateUrl : 'views/admin_login.html',
		controller : 'LoginController',
		activeTab : ''
	}).
	when('/admins/:adminId', {
		templateUrl : 'views/admin_main.html',
		controller : 'usersController',
		activeTab : 'home'
	}).
	when('/admins/:adminId/profile', {
		templateUrl: 'views/admin_profile.html',
		controller: 'adminProfileController',
		activeTab : 'home'
	}).
	when('/admins/:adminId/addUser', {
		templateUrl: 'views/newUser.html',
		controller: 'usersController',
		activeTab : 'home'
	}).
	when('/admins/:adminId/users/:userId',{
		templateUrl:'views/editUser.html',
		controller:'editUserController',
		activeTab:'home'
	}).
	when('/admins/:adminId/search',{
		templateUrl:'views/search.html',
		controller:'searchController',
		activeTab:'home'
	}).
	when('/admins/:adminId/search/:description',{
		templateUrl:'views/search.html',
		controller:'searchController',
		activeTab:'home'
	}).
	when('/admins/:adminId/users/:userId/boxes', {
		templateUrl: 'views/userBoxes.html',
		controller: 'boxesController',
		activeTab : 'home'
	}).
	when('/admins/:adminId/users/:userId/newBox',{
		templateUrl:'views/newBox.html',
		controller:'createBoxController',
		activeTab:'home'
	}).
	when('/admins/:adminId/users/:userId/boxes/:boxId/newItem',{
		templateUrl:'views/newItem.html',
		controller:'createItemController',
		activeTab:'home'
	}).
	when('/admins/:adminId/users/:userId/boxes/:boxId/items',{
		templateUrl:'views/boxItems.html',
		controller:'editBoxAndItemController',
		activeTab:'home'
	}).
	when('/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId/editItem',{
		templateUrl:'views/editItem.html',
		controller:'editItemController',
		activeTab:'home'
	}).
	when('/admins/:adminId/imageManagement',{
		templateUrl:'views/imageManagement.html',
		controller:'imagesController',
		activeTab:'home'
	})
}]);

 