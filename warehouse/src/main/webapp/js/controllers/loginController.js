'use strict';

mainApp.factory('AuthService', function ($http, Session) {

	  var authService = { user : undefined }
	  authService.isAuthenticated = function () {
	    return user != undefined;

	  };
	  authService.isAuthorized = function (authorizedRoles) {
	    if (!angular.isArray(authorizedRoles)) {
	      authorizedRoles = [authorizedRoles];
	    }
	    return (authService.isAuthenticated() &&
	      authorizedRoles.indexOf(authService.user.roles) !== -1);

	  };

	  authService.setUser = function(user) {
		  authService.user = user;
		  sessionStorage.user = JSON.stringify(user);
		  
	  }

	  authService.getUser = function() { 
		  	if(!authService.user){
		  		authService.user = JSON.parse(sessionStorage.user);
		  	}
		  	return authService.user;
	  };
	  return authService;
	});


mainApp.service('Session', function () {
	  this.create = function (sessionId, userId, userRole) {
		    this.id = sessionId;
		    this.userId = userId;
		    this.userRole = userRole;
		  };
		  this.destroy = function () {
		    this.id = null;
		    this.userId = null;
		    this.userRole = null;
		  };
		  return this;
		});

mainApp.controller('LoginController', function ($scope, $http, $window, $location, AuthService,$cookies) {
	$scope.credentials = {
			username: '',
			password: ''
	};
	
	$scope.error = false;
	//$scope.message = undefined;
    $scope.login = function(credentials) {        	
    	var tokenValue = $('#csrf_token').val();
    	var tokenHeaderName = $('#csrf_token').attr('data-header');
    	
    	$http.defaults.headers.common[tokenHeaderName] = tokenValue;
    	var payload = 'username=' + credentials.username + '&password=' + credentials.password;
    	$http({
    			method : 'POST',
    			data : payload,
    			url : 'login',
    			headers : { "Content-Type" : 'application/x-www-form-urlencoded' }   
    		})
        	.success(function (data) {
        		console.log('login success');
        		console.log(data);
        		$scope.error = data.status === 'error';
        		$scope.error = data.error;
        		AuthService.setUser(data);
        		
        		if(data.status == 'error'){
    				$scope.error="username or password not correct";
    			}else if(data.status == false){
    				$scope.error="user has been inactivated";
    			}else{
    				$window.sessionStorage.setItem('adminId', data.id);
        			$location.path('/admins/' + data.id);
    			}
        	})
        	.error(function (data) {
        		$scope.error = "login fail";
            });
    };
});

mainApp.factory('AuthInterceptor', function ($q, $injector) {
	var HEADER_NAME = undefined;
	function getCsrfHeaderName(response) {
		HEADER_NAME =  response.headers('X-CSRF-HEADER');
		return HEADER_NAME;
	};
	
	function getCsrfToken(response) {
		return response.headers(getCsrfHeaderName(response));
	};
	
	return {
		response : function(response) {			
//			console.log(response.headers('X-CSRF-TOKEN'));
//			var $http = $injector.get('$http');
//			var headerName = getCsrfHeaderName( response );
//			var token = getCsrfToken( response );
//			console.log( 'AuthInterceptor.response', headerName, token );
//			$http.defaults.headers.common[getCsrfHeaderName(response)] = getCsrfToken(response) ;
//			return response || $q.when(response);
			
			console.log(response.headers('X-CSRF-TOKEN'));
					
			if(response.status<400){
				var $http = $injector.get('$http');
				$http.defaults.headers.common[getCsrfHeaderName(response)] = getCsrfToken(response) ;
				
			}else{
				var $http = $injector.get('$http');
				console.log(HEADER_NAME);
				delete $http.defaults.headers.common[HEADER_NAME];
			}
			return response || $q.when(response);
		}
	};
});

// Register the previously created AuthInterceptor.
mainApp.config(['$httpProvider',function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
}]);