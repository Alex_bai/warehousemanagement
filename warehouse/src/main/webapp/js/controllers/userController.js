userServices = angular.module('userServices',['ngResource','UserValidation','tqq.ui']);

angular.module('UserValidation', []).directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.form.password.$viewValue
                ctrl.$setValidity('noMatch', !noMatch)
            })
        }
    }
})

userServices.factory('TotalUserNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/totalUserNum')
}])

userServices.factory('Users',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId',{},{
		update:{
			method:'PUT'
		}
	});
}])

userServices.factory('UserStatus',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/status',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.factory('UserRole',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/userRole',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.factory('logout',['$resource',function($resource){
	return $resource('api/logout')
}]);

userServices.controller('usersController',['$scope','$routeParams','AuthService','$route','$location','Users','TotalUserNum','UserStatus','UserRole','logout',
                                          function($scope,$routeParams,AuthService,$route,$location,Users,TotalUserNum,UserStatus,UserRole,logout){
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.admin = AuthService.getUser();
	$scope.id = $scope.admin.id;
	$scope.username = $scope.admin.userName;

	$scope.currentPage = 1;
	
	$scope.totalUsers = 0;
	
	Users.query({ adminId:$scope.adminId, pageNum:$scope.currentPage },function(response){
		$scope.users = response;
	});
	
	TotalUserNum.get({adminId:$scope.adminId},function(response){
		$scope.totalUsers = response.num;
	});

	
	$scope.gotoPage = function() {
		$scope.users = Users.query( { adminId: $scope.adminId, pageNum : $scope.currentPage });
	}
	
	$scope.checkRole = function(role){
		if(role == "ROLE_ADMIN"){
			return true;
		}
	}
	
	$scope.userSelf = function(id){
		if(id == $scope.adminId){
			return true;
		}
		return false;
	}


	$scope.deleteUser = function(userId){
		Users.remove({ 
			adminId:$scope.adminId, 
			userId:userId,
			pageNum:$scope.currentPage
		}, function(response){
			if(response != null){
				$scope.users = response.totalUsers;
			}else{
				alert("delete failed");
			}
		});
	};
	
	$scope.updateStatus = function(userId){
		UserStatus.update({adminId: $scope.adminId,userId:userId},$scope.user)
	};
	
	$scope.noOperation = function(userId){
		if(userId == $scope.adminId){
			return true;
		}
		return false;
	}
	
	
	$scope.changeRole = function(userId){
			if(userId == $scope.adminId){
				$scope.noOperation = true;
			}else{
				UserRole.update({adminId: $scope.adminId,userId:userId},$scope.user)
			}
	};
	
	$scope.logout = function(){		
		sessionStorage.clear();
		logout.save();
		$location.path('/');	
	}
	
}]);


userServices.controller('createUserController',['$scope','$routeParams','AuthService','$route','$location','Users','logout',
                                     function($scope,$routeParams,AuthService,$route,$location,Users,logout){
	
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	
	$scope.adminId = $routeParams.adminId;
	$scope.admin = AuthService.getUser();
	$scope.id = $scope.admin.id;
	$scope.username = $scope.admin.userName;

	$scope.addUser = function(){
		Users.save({ 
				adminId : $scope.adminId } ,
				$scope.user,
				function(response){					
					
				if(response != null){
					$scope.u = response;
					if($scope.u.userName == null){
						alert("username cannot be duplicated");
					}else{
						$location.path('/admins/'+$scope.adminId);
					}
				}else{
					alert("is null");
				}
		});
	};
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
	
}]);




userServices.controller('editUserController',['$scope','$routeParams','Users','AuthService','$route','$location','logout',
                                              function($scope,$routeParams,Users,AuthService,$route,$location,logout){
	
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.admin = AuthService.getUser();
	$scope.username = $scope.admin.userName;
	
	$scope.singleUser = Users.get( {adminId:$scope.adminId, userId:$scope.userId });
	
	$scope.editUser = function(){
		
		console.log("single user: "+$scope.singleUser);
		
			Users.update({
				adminId:$scope.adminId,
				userId: $scope.userId
				}, $scope.singleUser, function(response){
				if(response != null){
					$location.path('/admins/'+$scope.adminId);
				}else{
					alert("update failed");
				}
			});
		};
		
		$scope.logout = function(){		
			logout.save();
			sessionStorage.clear();
			$location.path('/');	
		}
		
}])


userServices.factory('Admins',['$resource',function($resource){
	return $resource('api/admins/:adminId',{},{
		update:{
			method:'PUT'
		}
	});
}])



userServices.controller('adminProfileController', ['$scope', '$routeParams', 'AuthService','$location','$route','Admins','logout',
                                             function ($scope, $routeParams, AuthService,$location, $route,Admins,logout) {
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	
	$scope.adminId = $routeParams.adminId;
	$scope.admin = AuthService.getUser();
	$scope.id = $scope.admin.id;
	$scope.username = $scope.admin.userName;
	
	$scope.admin = Admins.get({adminId:$scope.id})
	
	$scope.updateProfile = function(){
		Admins.update({adminId:$scope.adminId},$scope.admin, function(response){
			if(response != null){
				$location.path('/admins/'+$scope.adminId);
			}
		});
	};
	
	$scope.checkLength = function(password){
		if(password != null){
			if($scope.newPassword.length < 6){
				return true;
			}
		}		
		return false;
	}
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
	
}]);


userServices.factory('UserBoxNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/userBoxNum')
}])

userServices.factory('Boxes',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId')
}]);

userServices.factory('UpdateBoxInfo',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);


userServices.factory('BoxStatus',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/status',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.factory('updateBoxImage',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/image',{},{
		update:{
			method:'PUT'
		}
	})
}])



userServices.controller('boxesController',['$scope','$routeParams','AuthService','$route','$location','Boxes','UserBoxNum','BoxStatus','logout',
                                       function($scope,$routeParams,AuthService,$route,$location,Boxes,UserBoxNum,BoxStatus,logout){
	
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.boxId = $routeParams.boxId;
	$scope.admin = AuthService.getUser();
	$scope.username = $scope.admin.userName;
	
	$scope.userBoxNum = 0;
	$scope.currentPage = 1;
	
	$scope.boxes = Boxes.query( { adminId:$scope.adminId, userId:$scope.userId, pageNum:$scope.currentPage });
	
	UserBoxNum.get({adminId:$scope.adminId, userId:$scope.userId},function(response){
		$scope.userBoxNum = response.userBoxNum;
	});
	
	$scope.gotoPage = function() {
		$scope.boxes = Boxes.query( { adminId: $scope.adminId, userId:$scope.userId, pageNum : $scope.currentPage });
	}
	
	$scope.deleteBox = function(boxId){
		Boxes.remove({
			adminId:$scope.adminId,
			userId:$scope.userId,
			boxId:boxId,
			pageNum: $scope.currentPage
		},function(response){
			if(response != null){
				$scope.boxes = response.userBoxes;
			}else{
				alert("delete failed");
			}
		});
	};

	$scope.updateBoxStatus = function(boxId){
	BoxStatus.update({adminId: $scope.adminId,userId:$scope.userId, boxId:boxId},$scope.box)};	
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
	
}]);


userServices.factory('ChooseImage',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/images/:imageId')
}]);

userServices.factory('ImageNumCanChoose',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/imageNumCanChoose')
}]);

userServices.controller('createBoxController',['$scope','$routeParams','AuthService','$route','$location','Boxes','ChooseImage','ImageNumCanChoose','logout',
                                               function($scope,$routeParams,AuthService,$route,$location,Boxes,ChooseImage,ImageNumCanChoose,logout){
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.boxId = $routeParams.boxId;
	$scope.admin = AuthService.getUser();
	$scope.username = $scope.admin.userName;
	
	$scope.imageNumCanChoose = 0;
	$scope.currentPage = 1;	
	
	
	$scope.images = ChooseImage.query( { adminId:$scope.adminId, userId:$scope.userId,pageNum:$scope.currentPage });	
	
	ImageNumCanChoose.get({adminId:$scope.adminId,userId:$scope.userId},function(response){
		$scope.imageNumCanChoose = response.imageNumCanChoose;
	});
	
	$scope.gotoPage = function() {
		$scope.images = ChooseImage.query( { adminId: $scope.adminId,userId:$scope.userId, pageNum : $scope.currentPage });
	}	
	
	$scope.addBox = function(){	
		if($scope.box.userImage == null){
			$scope.noImage = 'Image must be choose';
		}else{
			$scope.noImage = '';
			Boxes.save({
				adminId: $scope.adminId,
				userId: $scope.userId
				}, $scope.box, function(response){
				if(response != null){
					$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes');
				}else{
					alert("create box failed");
				}
			});
		}			
	};
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
	
}])


userServices.factory('BoxItemNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/BoxItemNum');
}]);

userServices.controller('editBoxAndItemController',['$scope','$routeParams','AuthService','$route','$location','$uibModal','$log','Items','BoxItemNum','Boxes','ChooseImage','UpdateBoxInfo','updateBoxImage','ImageNumCanChoose','logout',
                                                function($scope,$routeParams,AuthService,$route,$location,$uibModal,$log,Items,BoxItemNum,Boxes,ChooseImage,UpdateBoxInfo,updateBoxImage,ImageNumCanChoose,logout){
			if(sessionStorage.getItem("adminId")==null){
				$location.path('/');		
			}
			$scope.adminId = $routeParams.adminId;
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.admin = AuthService.getUser();
		  	$scope.username = $scope.admin.userName;
		  	
			$scope.currentPage = 1;		
			$scope.boxItemNum = 0;
			$scope.imageNumCanChoose = 0;
			var initialBoxName;
			var initialBoxDescription;			
			
			$scope.animationEnabled = true;
			
			$scope.showImage = false;
			$scope.hasUserImage = true;
		
			Boxes.get({adminId:$scope.adminId, userId:$scope.userId, boxId:$scope.boxId},function(response){
				$scope.box = response;	
				if($scope.box.userImage != null){
					$scope.imageAddress="api/admins/"+$scope.adminId+"/userImage/"+$scope.box.userImage.id;
		    	}
				initialBoxName = $scope.box.name;
				initialBoxDescription = $scope.box.description;
			});
			
			BoxItemNum.get({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId},function(response){
				$scope.boxItemNum = response.boxItemNum;
			});
			
			$scope.items = Items.query( { adminId:$scope.adminId, userId:$scope.userId, boxId:$scope.boxId, pageNum:$scope.currentPage });									
			
			$scope.gotoPage = function() {
				$scope.items = Items.query( { adminId: $scope.adminId, userId:$scope.userId, boxId:$scope.boxId, pageNum : $scope.currentPage });
			}
			
			$scope.gotoPageImage = function() {
				$scope.images = ChooseImage.query({adminId:$scope.adminId,userId:$scope.userId, pageNum : $scope.currentPage });
			}
			
			
			
			$scope.updateBoxInfo = function(){
				UpdateBoxInfo.update({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
				},$scope.box)
			}
			
			
			$scope.changeName = function(){
				$scope.updateBoxInfo();
				initialBoxName = $scope.box.name;
				$scope.change = false;
			}
			
			$scope.cancelName = function(){
				$scope.box.name = initialBoxName;
				$scope.change = false;
			}	
			
			$scope.changeDescription = function(){
				$scope.updateBoxInfo();
				initialBoxDescription = $scope.box.description;
				$scope.changeDes = false;
			}
			
			$scope.cancelDescription = function(){
				$scope.box.description = initialBoxDescription;
				$scope.changeDes = false;
			}
			
			
			$scope.selectedImage = function(image){
				$scope.boxImage = image;
			}
			
			$scope.updateImage = function(){
				$scope.box.userImage = $scope.boxImage;
				updateBoxImage.update({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId
				},$scope.box, function(response){
					if(response != null){
						$scope.box = response;
						$scope.imageAddress="api/admins/"+$scope.adminId+"/userImage/"+$scope.box.userImage.id;
						$('#myModal').modal('hide')
					}
				});
				
				
			}
			
			$scope.tanModal = function(){
				$('#myModal').modal();
				$scope.images = ChooseImage.query({adminId:$scope.adminId,userId:$scope.userId,pageNum:$scope.currentPage });
				ImageNumCanChoose.get({adminId:$scope.adminId,userId:$scope.userId},function(response){
					$scope.imageNumCanChoose = response.imageNumCanChoose;
				});
			}

			
			
			$scope.createItem = function(){
				if($scope.box.status == false){
					$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/newItem');
				}else{
					$scope.infom="Box is full, no more items can be added";
				}
			}
			
			$scope.deleteItem = function(itemId){
				Items.remove({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:itemId,
					pageNum: $scope.currentPage
				},function(response){
					if(response != null){
						$scope.items = response.userItems;
					}else{
						alert("delete failed");
					}
				});
			};
			
			$scope.logout = function(){		
				logout.save();
				sessionStorage.clear();
				$location.path('/');	
			}
}]);


userServices.factory('Items',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateItemInfo',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);


userServices.factory('Photos',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId/photos/:photoId')
}]);

userServices.controller('createItemController', [ '$scope','$routeParams','AuthService','$route','$http','$location','logout',
                                             		function($scope, $routeParams,AuthService, $route,$http,$location,logout) {
		if(sessionStorage.getItem("adminId")==null){
			$location.path('/');		
		}
		
		$scope.adminId = $routeParams.adminId;
		$scope.userId = $routeParams.userId;
        $scope.boxId = $routeParams.boxId;
        $scope.admin = AuthService.getUser();
        $scope.username = $scope.admin.userName;
                      	
        $scope.filesChanged = function(elm){
               $scope.files = elm.files;
               $scope.$apply();
        }        
                      	
        $scope.addItem = function() {
              var fd = new FormData();               		
              angular.forEach($scope.myFiles, function(file){              	  
              fd.append('files', file);                      			
        });
                      		
        fd.append("name", $scope.name);
        fd.append("description", $scope.description);
                      		
        var createItem = 'api/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items';
                      		
        $http.post(createItem, fd, {
                transformRequest : angular.identity,
                headers : {
                   	'Content-Type' : undefined
                }
                }).success(function(response) {
                   	if(response != null){
                   	   $location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
                   	}
                }).error(function(response) {	
                });               		
            };
            
            $scope.logout = function(){		
        		logout.save();
        		sessionStorage.clear();
        		$location.path('/');	
        	}
        	
}]);

userServices.controller('editItemController',['$scope','$routeParams','AuthService','$route','$location','$http','Items','Photos','updateItemInfo','$rootScope','Boxes','logout',
                                                function($scope,$routeParams,AuthService,$route,$location,$http,Items,Photos,updateItemInfo,$rootScope,Boxes,logout){
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}		
	$scope.adminId = $routeParams.adminId;
    $scope.userId = $routeParams.userId;
    $scope.boxId = $routeParams.boxId;
    $scope.itemId = $routeParams.itemId;
    $scope.admin = AuthService.getUser();
    $scope.username = $scope.admin.userName;
	var initialItemName;
	var initialItemDescription;
    $scope.currentPage = 1;
		  	
	$scope.photos = Photos.query({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId,pageNum:$scope.currentPage})
			
	Items.get({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId},function(response){
		$scope.item = response;		
		initialItemName = $scope.item.name;
		initialItemDescription = $scope.item.description;
	});	
			
	Boxes.get({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId}, function(response){
		if(response.userImage != null){
			$scope.userImageId = response.userImage.id;
			$scope.boxImage = 'api/admins/'+$scope.adminId+'/userImage/'+$scope.userImageId;
		}
	});
			
		$scope.updateItemInfo = function(){
			updateItemInfo.update({
			adminId:$scope.adminId,
			userId:$scope.userId,
			boxId:$scope.boxId,
			itemId:$scope.itemId,
		},$scope.item)}
			
		$scope.changeName = function(){
			$scope.updateItemInfo();
			initialItemName = $scope.item.name;
			$scope.change = false;
		}
			$scope.cancelName = function(){
			$scope.item.name = initialItemName;
			$scope.change = false;
		}
			
			
		$scope.changeDescription = function(){
			$scope.updateItemInfo();
			initialItemDescription = $scope.item.description;
			$scope.changeDes = false;
		}
			$scope.cancelDescription = function(){
			$scope.item.description = initialItemDescription;
			$scope.changeDes = false;
		}
		  	$scope.filesChanged = function(elm){
			$scope.showInfo = false;
          	$scope.files = elm.files;
          	$scope.$apply();
        }        
          	
          	$scope.uploadFile = function() {
          		
          	if($scope.files == null){
          		$scope.showInfo = true;
          		$scope.Info = "please choose file";
          	}
          		
          		
	        if($scope.files != null){
	            var fd = new FormData();               		
              	angular.forEach($scope.files, function(file){                 			
              		fd.append('files', file);              			
              	});
              	var uploadUrl = 'api/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items/'+$scope.itemId+'/photos';
		          		
		       	$http.post(uploadUrl, fd, {
		       		transformRequest : angular.identity,
		       		headers : {
		       			'Content-Type' : undefined
		       		}
		       		}).success(function(response) {
		       			$scope.item = Items.get({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});       				
						$scope.photos = Photos.query({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId})
		       	    }).error(function(response) {
		       			
		       		});               		
          	};	
          	}
          	
		  	$scope.deletePhoto = function(photoId){
				Photos.remove({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
					photoId:photoId
				},function(response){
					if(response != null){	
						$scope.item = response;	
						$scope.photos = Photos.query({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId})
					}
					
				})
			}   
          	
		  	$scope.editItem = function(){	
					Items.update({
						adminId: $scope.adminId,
						userId: $scope.userId,
						boxId:$scope.boxId,
						itemId:$scope.itemId
						}, $scope.item, function(response){
							if(response != null){
								$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
							}
					});		
			}
		  
		  	$scope.logout = function(){		
				logout.save();
				sessionStorage.clear();
				$location.path('/');	
			}
			
}]);

userServices.factory('BoxNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/BoxNum');
}]);

userServices.factory('ItemNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/ItemNum');
}]);


userServices.factory('SearchBox',['$resource',function($resource){
	return $resource('api/admins/:adminId/searchBox');
}]);

userServices.factory('SearchItem',['$resource',function($resource){
	return $resource('api/admins/:adminId/searchItem');
}]);


userServices.factory('singleBox',['$resource',function($resource){
	return $resource('api/admins/:adminId/singleBox/:boxId');
}]);

userServices.factory('singleItem',['$resource',function($resource){
	return $resource('api/admins/:adminId/singleItem/:itemId');
}]);


userServices.controller('searchController',['$scope','$routeParams','AuthService','$route','$location','SearchBox','SearchItem','BoxNum','ItemNum','Boxes','Items','singleBox','singleItem','logout',
                                               function($scope,$routeParams,AuthService,$route,$location,SearchBox,SearchItem,BoxNum,ItemNum,Boxes,Items,singleBox,singleItem,logout){
		if(sessionStorage.getItem("adminId")==null){
			$location.path('/');		
		}
		$scope.adminId = $routeParams.adminId;
     	$scope.admin = AuthService.getUser();
       	$scope.username = $scope.admin.userName;
       	
       	$scope.BoxNum = 0;
       	$scope.ItemNum = 0;
		$scope.currentBoxPage = 1;
		$scope.currentItemPage = 1;
		$scope.search = "";
		$scope.specificUser = "";
		
		$scope.boxes = SearchBox.query({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search, pageNum:$scope.currentBoxPage});
		
		$scope.items = SearchItem.query({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search,pageNum:$scope.currentItemPage});
		
	
		$scope.getBoxNum = function(){
			BoxNum.get({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search},function(response){
				$scope.BoxNum = response.boxNum;			
			});
		}
		
		$scope.getItemNum = function(){
			ItemNum.get({adminId:$scope.adminId,specificUser:$scope.specificUser, search:$scope.search},function(response){				
				$scope.ItemNum = response.itemNum;				
			})
		}
		
			
		$scope.gotoPageBox = function() {
			SearchBox.query({
    			adminId:$scope.adminId,
    			specificUser:$scope.specificUser,
    			search:$scope.search,
    			pageNum: $scope.currentBoxPage
    		},function(response){
    			if(response != null){
    				$scope.boxes = response;
    			}
    		})
		}
		$scope.gotoPageItem = function() {
			$scope.items = SearchItem.query({
    			adminId:$scope.adminId,
    			specificUser:$scope.specificUser,
    			search:$scope.search,
    			pageNum:$scope.currentItemPage
    		},function(response){
    			if(response != null){
    				$scope.items = response;
    			}
    		});	
		}
		
		$scope.deleteBox = function(boxId){
			singleBox.remove({
				adminId:$scope.adminId,
				boxId:boxId,
				pageNum: $scope.currentBoxPage
			},function(response){
				if(response != null){
					$scope.boxes = response.boxes;
				}else{
					alert("delete failed");
				}
			});
		};
		
		
		$scope.getItem = function(boxId,itemId){
			singleItem.get({
				adminId:$scope.adminId,
				itemId:itemId
			},function(response){
				if(response != null){
					$location.path('/admins/'+$scope.adminId+'/users/'+response.userId+'/boxes/'+boxId+'/items/'+itemId+'/editItem' );
				}
			})
		}
		
		
		$scope.deleteItem = function(itemId){
			singleItem.remove({
				adminId:$scope.adminId,
				itemId:itemId,
				pageNum:$scope.currentItemPage
			},function(response){
				$scope.items = response.items;
			})
		}
		
   
    	$scope.searchProduct = function(){
    			$scope.getBoxNum();
    			$scope.getItemNum();
    			if(($scope.search==null || $scope.search=="") && ($scope.specificUser == null ||$scope.specificUser=="")){
    				$scope.boxes = SearchBox.query({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search, pageNum:$scope.currentBoxPage});	
    				$scope.items = SearchItem.query({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search,pageNum:$scope.currentItemPage});
        		}else if(($scope.search==null || $scope.search=="") && ($scope.specificUser != null ||$scope.specificUser !="")){
        			$scope.errorInfo = true;
    	    		$scope.error = "must input the phrases...";
    	    		$scope.boxes = SearchBox.query({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search, pageNum:$scope.currentBoxPage});	
    				$scope.items = SearchItem.query({adminId:$scope.adminId,specificUser:$scope.specificUser,search:$scope.search,pageNum:$scope.currentItemPage});
    	    	}else if($scope.specificUser == null || $scope.specificUser==""){
        			SearchBox.query({
    	    			adminId:$scope.adminId,
    	    			specificUser:$scope.specificUser,
    	    			search:$scope.search,
    	    			pageNum: $scope.currentBoxPage
    	    		},function(response){
    	    			if(response != null){
    	    				$scope.boxes = response;
    	    			}
    	    		});
    	    		
    	    		$scope.items = SearchItem.query({
    	    			adminId:$scope.adminId,
    	    			specificUser:$scope.specificUser,
    	    			search:$scope.search,
    	    			pageNum:$scope.currentItemPage
    	    		},function(response){
    	    			if(response != null){
    	    				$scope.items = response;
    	    			}
    	    		});
    	    	}else if(($scope.search!=null && $scope.search!="") && ($scope.specificUser != null && $scope.specificUser!="")){
    	    		$scope.errorInfo = false;
    	    		SearchBox.query({
    	    			adminId:$scope.adminId,
    	    			specificUser:$scope.specificUser,
    	    			search:$scope.search,
    	    			pageNum: $scope.currentBoxPage
    	    		},function(response){
    	    			$scope.boxes = response;
    	    			if($scope.boxes.length==0){
    	    				$scope.BoxNum = 0;
    	    			}  	    			
    	    		})

    	    		SearchItem.query({
    	    			adminId:$scope.adminId,
    	    			specificUser:$scope.specificUser,
    	    			search:$scope.search,
    	    			pageNum:$scope.currentItemPage
    	    		},function(response){
    	    			$scope.items = response;
    	    			if(response.length == 0){
    	    				$scope.ItemNum = 0;
    	    			}
    	    		});
    	    	}
    	};
    	
    	$scope.getBoxNum();
    	$scope.getItemNum();
    	
    	$scope.logout = function(){		
    		logout.save();
    		sessionStorage.clear();
    		$location.path('/');	
    	}
    	
}]);


//only use in the image management 
userServices.factory('TotalImageNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/totalImageNum');
}])


userServices.factory('Images',['$resource',function($resource){
	return $resource('api/admins/:adminId/images/:imageId')
}]);


userServices.controller('imagesController',['$scope','$routeParams','AuthService','$route','$http','Images','TotalImageNum','$location','$rootScope','loading','logout',
                                           function($scope,$routeParams,AuthService,$route,$http,Images,TotalImageNum,$location,$rootScope,loading,logout){
	if(sessionStorage.getItem("adminId")==null){
		$location.path('/');		
	}
	
	$scope.adminId = $routeParams.adminId;
	$scope.admin = AuthService.getUser();
	$scope.username = $scope.admin.userName;
	
	$scope.totalImageNum = 0;
	$scope.currentPage = 1;
	
	$scope.images = Images.query( { adminId:$scope.adminId, pageNum:$scope.currentPage });
	TotalImageNum.get({adminId:$scope.adminId},function(response){
		$scope.totalImageNum = response.totalImageNum;
	});	
	
	$scope.gotoPage = function() {
		$scope.images = Images.query( { adminId: $scope.adminId, pageNum : $scope.currentPage });
	}
	
	$scope.filesChanged = function(elm){
  		$scope.showInfo = false;
  		$scope.files = elm.files;
  		$scope.$apply();
  	}        
  	
  	$scope.uploadFile = function() {  
  		if($scope.files == null){
  			$scope.showInfo = true;
  			$scope.Info="please choose file";
  		}else{
  			loading.show();
  			if($scope.files != null){
  	      		var fd = new FormData();               		
  	      		angular.forEach($scope.files, function(file){                 			
  	      			fd.append('files', file);              			
  	      	});              		              	
  	      	var uploadUrl = 'api/admins/'+$scope.adminId+'/images';
  			$http.post(uploadUrl, fd, {
  				transformRequest : angular.identity,
  				headers : {
  					'Content-Type' : undefined
  				}
  			}).success(function(response) {   
  				if(response.length!=0){    
  					$scope.showInfo = true;
  					$scope.Info="has already exist "+response.length+" images in chose images";  		   			
  				}
  				$scope.images = Images.query( { adminId:$scope.adminId, pageNum:$scope.currentPage });
  				loading.hide();
  			}).error(function(response) {
  			});               		
  	  		};
  		}
  	}
	
	
	$scope.deleteImage = function(imageId){
		Images.remove({
			adminId: $scope.adminId,
			imageId: imageId,
			pageNum: $scope.currentPage			
		}, function(response){
			if(response != null){
				$scope.images = response.images;
			}			
		});
	};	
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
	
}]);


userServices.directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {	
			
			element.bind('change', function() {
				$parse(attrs.fileModel).assign(scope, element[0].files);
				scope.$apply();
			});
		}
	};
	
}]);








