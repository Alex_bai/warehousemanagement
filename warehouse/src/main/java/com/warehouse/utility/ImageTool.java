package com.warehouse.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;

public class ImageTool {

	
	/**
	 * transfer file to byte array, and
	 * transfer byte array to string with base64 type
	 * @param file
	 * @return
	 */
	public static String TransferFileToByte(File file)
	{
		byte[] buffer = null;
		try {
			FileInputStream fis = new FileInputStream(file);			
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int n;
			while((n = fis.read(b)) != -1)
			{
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			buffer = bos.toByteArray();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return new String(Base64.encodeBase64(buffer));
	}
}
