package com.warehouse.utility;

public class MathUtility {

	
	public static int[] StringToInt(String data[])
	{
		int res[] = new int[data.length];
		for(int i=0;i<data.length;i++)
		{
			res[i] = Integer.parseInt(data[i]);
		}
		return res;
	}
}
