package com.warehouse.controllers;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.warehouse.annotations.CacheControl;
import com.warehouse.annotations.CachePolicy;
import com.warehouse.domain.Box;
import com.warehouse.domain.Image;
import com.warehouse.domain.Item;
import com.warehouse.domain.Photo;
import com.warehouse.domain.User;
import com.warehouse.domain.UserImage;
import com.warehouse.exceptions.DuplicateItemException;
import com.warehouse.exceptions.DuplicateUserException;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.image.ImageRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.photo.PhotoRepository;
import com.warehouse.repositories.user.UserRepository;
import com.warehouse.repositories.userImage.UserImageRepository;
import com.warehouse.services.BoxService;
import com.warehouse.services.ImageService;
import com.warehouse.services.ItemService;
import com.warehouse.services.PhotoService;
import com.warehouse.services.QrCodeService;
import com.warehouse.services.UserDataService;
import com.warehouse.services.UserImageService;


@RestController
@RequestMapping("/api")
@JsonFormat
@CacheControl(maxAge=300, policy={CachePolicy.PUBLIC})
@PropertySource("classpath:config.properties")
@PreAuthorize("hasRole('ROLE_ADMIN')")

public class AdminController {
	@Autowired
	private UserDataService userDataService;
	
	@Autowired
	private BoxService boxService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private PhotoService photoService;
	
	@Autowired
	private QrCodeService qrCodeService;
	
	@Autowired
	private UserImageService userImageService;	
	
	@Autowired
	private UserImageRepository userImageRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private PhotoRepository photoRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private ImageRepository imageRepository;
	
	
	@Value("${pageSize}")
	private int pageSize;
	
	public List<String> rolesAsList(String ...roles) {
		return Arrays.asList(roles);
	}
		
	@RequestMapping(method = RequestMethod.GET, value = "/profile")
	@ResponseBody
	public User login(Principal principal) {		
		User user = userRepository.findByUserName(principal.getName());
		user.setPassword(null);
		return user;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/logout")
	@ResponseBody
	public Map<String,String> logout(HttpServletRequest req) {
		req.getSession().invalidate();
		Map<String, String> result = new HashMap<>();
		result.put("status",  "ok");
		return result;
	}
	
	
	
	/**
	 * get the admin information
	 * @param adminId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}")
	@PreAuthorize("principal.id == #adminId")	
	public User adminInfo(@PathVariable String adminId) {		
		return userDataService.getUser(adminId);
	}	
	
	
	
	/**
	 * update admin information
	 * @param adminId
	 * @param admin
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT,value = "/admins/{adminId}")
	@PreAuthorize("principal.id == #adminId")
	public User updateAdmin(@PathVariable String adminId, @RequestBody User admin,HttpServletResponse response){
		System.out.println("come into update profile of admin");
		return userDataService.updateAdmin(admin);
	}
	

	/**
	 * get total number of users
	 * @param adminId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET,value = "/admins/{adminId}/totalUserNum")	
	@PreAuthorize("principal.id == #adminId")
	public Map<String, Integer> getUserNum(@PathVariable String adminId){
		Map<String, Integer> map = new HashMap<String, Integer>();
		long totalUsers = userRepository.count();
		int a = (int) totalUsers;
		System.out.println(a);
		map.put("num", a);
		return map;
	}
	

	/**
	 * get all the users
	 * @param adminId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET,value = "/admins/{adminId}/users")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public List<User> getUsers(@PathVariable String adminId, @RequestParam String pageNum){		
		Page<User> page = userRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));		
		List<User> users  = new ArrayList<User>(page.getContent());			
		return users;
	}
	

	/**
	 * create a user
	 * @param adminId
	 * @param user
	 * @param response
	 * @return
	 * @throws DuplicateUserException
	 */
	@RequestMapping(method = RequestMethod.POST,value = "/admins/{adminId}/users")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public User createUser(@PathVariable String adminId, @RequestBody User user)throws DuplicateUserException{		
		if(userRepository.findByUserName(user.getUserName()) != null){
			User u = new User();
			return u;
		}else{
			User u = userDataService.saveUser(user);
			return u;
		}		
	}
	


	/**
	 * update the user status in user repository
	 * @param adminId
	 * @param userId
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/admins/{adminId}/users/{userId}/status")	
	@PreAuthorize("principal.id == #adminId")	
	@ResponseBody
	public void changeUserStatus(@PathVariable String adminId, @PathVariable String userId) {		
		userRepository.update(userId);
	}
	

	@RequestMapping(method = RequestMethod.PUT, value="admins/{adminId}/users/{userId}/userRole")
	@PreAuthorize("principal.id == #adminId")
	public User updateUserRole(@PathVariable String adminId,@PathVariable String userId){
		return userDataService.updateUserRole(userId);
	}
	
	

	/**
	 * update the user information
	 * @param adminId
	 * @param userId
	 * @param singleUser
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT,value = "/admins/{adminId}/users/{userId}")
	@PreAuthorize("principal.id == #adminId")
	public User updateUser(@PathVariable String adminId, @PathVariable String userId,@RequestBody User singleUser){
		return userDataService.updateUser(singleUser);
	}
	
	

	/**
	 * get the user information
	 * @param adminId
	 * @param userId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET,value = "/admins/{adminId}/users/{userId}")
	@PreAuthorize("principal.id == #adminId")
	public User getUser(@PathVariable String adminId, @PathVariable String userId){
		return userDataService.getUser(userId);
	}
	

	/**
	 * delete a user
	 * @param adminId
	 * @param userId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE,value = "/admins/{adminId}/users/{userId}")
	@PreAuthorize("principal.id == #adminId")
	public Map<String, List<User>> deleteUser(@PathVariable String adminId, @PathVariable String userId,@RequestParam String pageNum){
		Map<String, List<User>> map = new HashMap<String, List<User>>();
		userDataService.deleteUser(userId);
		Page<User> page = userRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		List<User> users = page.getContent();
		map.put("totalUsers",users);
		return map;
	}
	

	/**
	 * get the number of boxes belong to a user
	 * @param adminId
	 * @param userId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/userBoxNum")
	@PreAuthorize("principal.id == #adminId")	
	@ResponseBody
	public Map<String,Integer> getUserBoxNum(@PathVariable String adminId,@PathVariable String userId) {	
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<Box> boxes= userRepository.findById(userId).getBoxes();
		if(boxes == null)
		{
			map.put("userBoxNum", 0);
			return map;
		}else{
			int userBoxNum = boxes.size();
			map.put("userBoxNum", userBoxNum);
			return map;
		}
	}
	
	/**
	 * get the user boxes to show
	 * @param adminId
	 * @param userId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/boxes")
	@PreAuthorize("principal.id == #adminId")	
	public List<Box> getBoxes(@PathVariable String adminId,@PathVariable String userId,@RequestParam String pageNum) {
		List<Box> boxes = boxRepository.findByOwnerId(userId, new PageRequest(Integer.parseInt(pageNum)-1,pageSize));		
		return boxes;
	}	
	
	/**
	 * create a new box
	 * @param adminId
	 * @param userId
	 * @param box
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/admins/{adminId}/users/{userId}/boxes")
	@PreAuthorize("principal.id == #adminId")	
	public Box addBox(@PathVariable String adminId,@PathVariable String userId,@RequestBody Box box){		
		return boxService.addBox(userId, box);
	}
	

	/**
	 * get the box information
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}")
	@PreAuthorize("principal.id == #adminId")	
	public Box updateBoxInfo(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId) {
	    Box box = boxRepository.findById(boxId);
		return box;
	}	
	

	/**
	 * get the images and the images will remove the images that has already exists
	 * @param adminId
	 * @param userId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "admins/{adminId}/users/{userId}/images" )
	@PreAuthorize("principal.id == #adminId")
	public List<Image> getImages(@PathVariable String adminId,@PathVariable String userId,@RequestParam String pageNum){
						
		List<Box> boxes = userRepository.findById(userId).getBoxes();	
		
		if(boxes == null || boxes.isEmpty()){						
			Page<Image> page = imageRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
			List<Image> images = page.getContent();
			return  images;
		}		
		
		List<String> imageDel = new ArrayList<String>();
		for(Box box: boxes){
			if(box.getUserImage() != null){				
				UserImage image = box.getUserImage();				
				imageDel.add(image.getName());			
			}													
		}
		List<Image> images = imageService.getImages(imageDel, Integer.parseInt(pageNum), pageSize);
		return images;	
	}
	

	/**
	 * delete a box
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}")
	@PreAuthorize("principal.id == #adminId")	 
	public Map<String,List<Box>> deleteBox(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId,@RequestParam String pageNum) {
		Map<String, List<Box>> map = new HashMap<String,List<Box>>();
		boxService.deleteBox(userId, boxId);
		List<Box> boxes = boxRepository.findByOwnerId(userId, new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		map.put("userBoxes",boxes);
		return map;
	}	
	
	
	@RequestMapping(method = RequestMethod.PUT, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/info")
	@PreAuthorize("principal.id == #adminId")
	public Box updateBoxInfo(@PathVariable String adminId, @PathVariable String userId,@PathVariable String boxId,@RequestBody Box box){
		return boxService.updateBoxInfo(box);
	}
	
	
	@RequestMapping(method = RequestMethod.PUT, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/image")
	@PreAuthorize("principal.id == #adminId")
	public Box updateBoxImage(@PathVariable String adminId, @PathVariable String userId,@PathVariable String boxId,@RequestBody Box box){
		System.out.println("box userimage infor"+box.getUserImage().getBoxId()+"...."+box.getUserImage().getId()+box.getUserImage().getUrl()+box.getUserImage().getAacs());
		
		
		//old box
		Box b = boxRepository.findById(boxId);			
		Box res = null;
		if(b.getUserImage() == null){
			res = boxService.updateBoxImage(userId,"",box);
		}else{
			//get old box id			
			String userImageId = b.getUserImage().getId();
			res = boxService.updateBoxImage(userId,userImageId,box);
		}
		return res;
	}
	

	/**
	 * change the sttus of boxes
	 * @param adminId
	 * @param userId
	 * @param boxId
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/status")	
	@PreAuthorize("principal.id == #adminId")	
	public void changeBoxStatus(@PathVariable String adminId, @PathVariable String userId,@PathVariable String boxId) {		
		 boxRepository.update(boxId);
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/BoxItemNum")
	@PreAuthorize("principal.id == #adminId")	
	@ResponseBody
	public Map<String,Integer> boxItemNum(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId) {		
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<Item> items= boxRepository.findById(boxId).getItems();
		if(items == null){
			map.put("boxItemNum", 0);
			return map;
			
		}else{
			int num = items.size();
			map.put("boxItemNum", num);
			return map;
		}
	}
	

	/**
	 * get the items
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items")
	@PreAuthorize("principal.id == #adminId")	
	public List<Item> getItems(@PathVariable String adminId,@PathVariable String userId,
			@PathVariable String boxId, @RequestParam String pageNum) {
		List<Item> items= itemRepository.findByBoxId(boxId,new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		return items;
	}	

	/**
	 * create a new item
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param name
	 * @param description
	 * @param itemId
	 * @param photos
	 * @param files
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items")
	@PreAuthorize("principal.id == #adminId")
	public Item createItem(@PathVariable String adminId,@PathVariable String userId,  
			@PathVariable String boxId,	@RequestParam String name, @RequestParam String description,
			@RequestParam MultipartFile[] files){		
		
		List<Photo> photos = new ArrayList<>();
		
		String itemId = UUID.randomUUID().toString();
		
		if(files != null && files.length != 0)
		{			
			
			for(int i=0;i<files.length;i++)
			{
				MultipartFile file = files[i];
				
				try {					
					Photo photo = photoService.savePhoto(itemId,file);
					photos.add(photo);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}	
		
		
	
		Item item = new Item.Builder()
				.id(itemId)
				.name(name)
				.description(description)
				.boxId(boxId)
				.photos(photos)
				.build();	
		
		
	    item =  itemService.addItem(item);
		
		return item;
	}



	/**
	 * get the item information
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items/{itemId}")
	@PreAuthorize("principal.id == #adminId")	
	public Item itemInfo(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId) {
		Item item = itemRepository.findById(itemId);
		return item;
	}	

	/**
	 * delete the item
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items/{itemId}")
	@PreAuthorize("principal.id == #adminId")	
	public Map<String,List<Item>> deleteItem(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId,@RequestParam String pageNum) {
		Map<String, List<Item>> map = new HashMap<String,List<Item>>();
		itemService.deleteItem(boxId, itemId);
		List<Item> items = itemRepository.findByBoxId(boxId,new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		map.put("userItems",items);
		return map;
	}	
	
	

	/**
	 * upload photos
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @param files
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items/{itemId}/photos")
	@PostAuthorize("principal.id == #adminId")			
	public Item uploadPhoto(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId,  
			@RequestParam(value="files", required=true) MultipartFile[] files){									

		if(files != null && files.length != 0)
		{											
			for(int i=0;i<files.length;i++)
			{
				MultipartFile file = files[i];			
				try {															
					   Photo photo = photoService.savePhoto(itemId,file);
					   itemService.addPhotoInItem(itemId, photo);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}	
		}
		return itemRepository.findById(itemId);
	}
	


	/**
	 * get the photos per item
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items/{itemId}/photos")
	@PostAuthorize("principal.id == #adminId")	
	public List<Photo> getPhotos(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId,
							@PathVariable String itemId){
		List<Photo> photos = photoRepository.findByItemId(itemId);
		return photos;
	}
	
	

	@RequestMapping(method = RequestMethod.PUT, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items/{itemId}/info")
	@PreAuthorize("principal.id == #adminId")
	public Item updateBoxInfo(@PathVariable String adminId, @PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId,@RequestBody Item item){
		return itemService.updateItemInfo(item);
	}

	/**
	 * delete photo in the item
	 * @param adminId
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @param photoId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/admins/{adminId}/users/{userId}/boxes/{boxId}/items/{itemId}/photos/{photoId}")
	@PreAuthorize("principal.id == #adminId")	
	public Item deletePhoto(@PathVariable String adminId,@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId, @PathVariable String photoId) {
		Item item = photoService.deletePhoto(itemId,photoId);
		return item;
	}
	

	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/searchBox")
	@PostAuthorize("principal.id == #adminId")	
	public List<Box> searchBoxes(@PathVariable String adminId, @RequestParam String specificUser, @RequestParam String search, @RequestParam String pageNum) {
		return boxService.searchBoxByInfo(search, specificUser,Integer.parseInt(pageNum), pageSize);
	}

	@RequestMapping(method = RequestMethod.DELETE, value="/admins/{adminId}/singleBox/{boxId}")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public Map<String, List<Box>> deleteBoxes(@PathVariable String adminId,@PathVariable String boxId,@RequestParam String pageNum){
		Map<String, List<Box>> map = new HashMap<String,List<Box>>();
		String userId = boxRepository.findById(boxId).getOwnerId();
		boxService.deleteBox(userId, boxId);
		Page<Box> page = boxRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		List<Box> boxes = page.getContent();
		map.put("boxes", boxes);
		return map;
	}
	
	/**
	 * get box num
	 * @param adminId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/BoxNum")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public Map<String,Integer> getTotalBoxNum(@PathVariable String adminId,@RequestParam String specificUser,@RequestParam String search) {		
	    return 	boxService.searchBoxNum(search, specificUser);
	}
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/searchItem")
	@PostAuthorize("principal.id == #adminId")	
	public List<Item> searchItems(@PathVariable String adminId, @RequestParam String specificUser, @RequestParam String search, @RequestParam String pageNum) {
		return itemService.searchItemByInfo(search, specificUser, Integer.parseInt(pageNum), pageSize);
	}
	

	@RequestMapping(method = RequestMethod.GET, value="/admins/{adminId}/singleItem/{itemId}")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public Map<String,String> getItems(@PathVariable String adminId,@PathVariable String itemId){
		Map<String,String> map = new HashMap<String,String>();
		String boxId = itemRepository.findById(itemId).getBoxId();
		String userId = boxRepository.findById(boxId).getOwnerId();
		map.put("userId", userId);
		return map;
	}
	

	@RequestMapping(method = RequestMethod.DELETE, value="/admins/{adminId}/singleItem/{itemId}")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public Map<String, List<Item>> deleteItem(@PathVariable String adminId,@PathVariable String itemId,@RequestParam String pageNum){
		Map<String, List<Item>> map = new HashMap<String,List<Item>>();
		String boxId = itemRepository.findById(itemId).getBoxId();
		itemService.deleteItem(boxId, itemId);
		Page<Item> pageItems = itemRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		List<Item> items = pageItems.getContent();
		map.put("items",items);
		return map;
	}
	

	
	/**
	 * get the total items number
	 * @param adminId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/ItemNum")
	@PreAuthorize("principal.id == #adminId")	
	@ResponseBody
	public Map<String,Integer> getTotalItemNum(@PathVariable String adminId,@RequestParam String specificUser,@RequestParam String search) {		
	    return 	itemService.searchItemNum(search, specificUser);
	}
	


	/**
	 * get the total image number
	 * @param adminId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/totalImageNum")
	@PreAuthorize("principal.id == #adminId")
	@ResponseBody
	public Map<String,Integer> getTotalImageNum(@PathVariable String adminId) {		
		Map<String, Integer> map = new HashMap<String, Integer>();
		int totalImageNum = (int)imageRepository.count();	
		map.put("totalImageNum", totalImageNum);
		return map;
	}

	

	/**
	 * get the images to show
	 * @param adminId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "admins/{adminId}/images" )
	@PreAuthorize("principal.id == #adminId")
	public List<Image> getImages(@PathVariable String adminId,@RequestParam String pageNum){
		Page<Image> page = imageRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));		
		List<Image> images  = page.getContent();
		for(Image image : images){
			List<UserImage> userImages = userImageRepository.findByUrl(image.getUrl());
			image.setUsedNum(userImages.size());
		}
		return images;
	}
	

	/**
	 * delete the image
	 * @param adminId
	 * @param imageId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "admins/{adminId}/images/{imageId}")
	@PreAuthorize("principal.id == #adminId")
	public Map<String, List<Image>> deleteImages(@PathVariable String adminId, 
			@PathVariable String imageId, @RequestParam String pageNum){
				
		Image image = imageRepository.findById(imageId);
			String imagePath = image.getUrl();
			if(new File(imagePath).delete())
			{
				List<UserImage> userImages = userImageRepository.findByUrl(image.getUrl());
				for(UserImage userImage: userImages){
					userImageRepository.delete(userImage.getId());
				}
				
				
				imageRepository.delete(imageId);
				Page<Image> page = imageRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));		
				List<Image> images  = page.getContent();
				
				Map<String, List<Image>> res = new HashMap<String, List<Image>>();
				res.put("images", images);
				return res;
			}else{
				return null;
			}	
					
	}
	
		/**
		 * update some new images
		 * @param adminId
		 * @param files
		 * @return
		 */
		@RequestMapping(method = RequestMethod.POST, value = "/admins/{adminId}/images")
		@PostAuthorize("principal.id == #adminId")			
		public List<String> uploadImage(@PathVariable String adminId,  
				@RequestParam(value="files", required=true) MultipartFile[] files){									
			List<String> sameImages = new ArrayList<String>();
			if(files != null && files.length != 0)
			{											
				for(int i=0;i<files.length;i++)
				{
					MultipartFile file = files[i];			
					try {															
						String res = imageService.saveImage(file);
						if(res == "same")
						{
							sameImages.add(file.getName());
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}	
			}
			return sameImages;
		}
	
	@RequestMapping(method = RequestMethod.GET, value = "/admins/{adminId}/users/{userId}/imageNumCanChoose")
	@PostAuthorize("principal.id == #adminId")
	public Map<String,Integer> imageNumCanChoose(@PathVariable String adminId,@PathVariable String userId){		
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		List<Box> boxes = userRepository.findById(userId).getBoxes();	
		if(boxes == null || boxes.isEmpty()){						
			int num = (int) imageRepository.count();
			map.put("imageNumCanChoose", num);
			return map;
		}		
		
		List<String> imageDel = new ArrayList<String>();
		for(Box box: boxes){
			if(box.getUserImage() != null){				
				UserImage image = box.getUserImage();				
				imageDel.add(image.getName());	
			}													
		}
		int num = (int)imageRepository.count()-imageDel.size();
		map.put("imageNumCanChoose", num);
		return map;
	}
	

	
	
	/**
	 * get the image from local file in the box
	 * @param adminId
	 * @param imageId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/admins/{adminId}/userImage/{imageId}" )	
	@PreAuthorize("principal.id == #adminId")
	public ResponseEntity<byte[]> getUserImage(@PathVariable String adminId, 			
			@PathVariable String imageId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( userImageService.getImageData(imageId), headers, HttpStatus.OK );
	}
	
	/**
	 * get the images from local file in the imageRepository
	 * @param adminId
	 * @param imageId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/admins/{adminId}/images/{imageId}/image" )	
	@PreAuthorize("principal.id == #adminId")
	public ResponseEntity<byte[]> getImage(@PathVariable String adminId, 			
			@PathVariable String imageId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( imageService.getImageData(imageId), headers, HttpStatus.OK );
	}
	
	/**
	 * get the item photos form local file
	 * @param adminId
	 * @param userId
	 * @param photoId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/admins/{adminId}/photos/{photoId}/photo" )	
	@PreAuthorize("principal.id == #adminId")
	public ResponseEntity<byte[]> getPhoto(@PathVariable String adminId,		
			@PathVariable String photoId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( photoService.getPhotoData(photoId), headers, HttpStatus.OK );
	}
	
	
	/**
	 * get the qrCode from local file
	 * @param adminId
	 * @param qrCodeId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/admins/{adminId}/qrCodes/{qrCodeId}/qrCode" )	
	@PreAuthorize("principal.id == #adminId")
	public ResponseEntity<byte[]> getQrCode(@PathVariable String adminId, 			
			@PathVariable String qrCodeId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( qrCodeService.getQrCodeData(qrCodeId), headers, HttpStatus.OK );
	}
}

	
	

