package com.warehouse.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.zxing.WriterException;
import com.warehouse.domain.Box;
import com.warehouse.domain.Image;
import com.warehouse.domain.Item;
import com.warehouse.domain.Photo;
import com.warehouse.domain.QrCode;
import com.warehouse.domain.User;
import com.warehouse.match.factory.QRCodeFactory;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.image.ImageRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.photo.PhotoRepository;
import com.warehouse.repositories.qrCode.QrCodeRepository;
import com.warehouse.repositories.user.UserRepository;

@Controller
@RequestMapping("/api")
@JsonFormat
//@PropertySource("classpath:config.properties")
public class MockDataController {
	@Autowired
	UserRepository userRepository;
	@Autowired
	BoxRepository boxRepository;
	@Autowired
	ItemRepository itemRepository;
	@Autowired
	ImageRepository imageRepository;
	@Autowired
	PhotoRepository photoRepository;
	@Autowired
	QrCodeRepository qrCodeRepository;
	
	public boolean ACTIVE = true;
	public boolean FULL = false;
	
	//private static String SAVE_IMAGE_URL = "/Users/yuanyuankang/Documents/warehouseData/images/";
	private static String SAVE_IMAGE_URL = "/home/centos/warehouseData/images/";
	//private static String SAVE_PHOTO_URL = "/Users/yuanyuankang/Documents/warehouseData/photos/";
	
	private static String SAVE_PHOTO_URL = "/home/centos/warehouseData/photos/";	
	
	//private static String SAVE_QRCODE_URL = "/Users/yuanyuankang/Documents/warehouseData/qrCodes/";
	private static String SAVE_QRCODE_URL = "/home/centos/warehouseData/qrCodes/";
	
	private static String info = "hello world";
	
	private static int size = 400;
	
	private static String imageType = "png";
	
	private static String[] userData = 
			{ "yuan", "kang", "Alina","kang.yuanyua","@uwlax.edu", "123"};

	
	private static String[] itemData = {"Alina's sockets","winter sockets"};

	
	public static String[] boxData = {"sockets","winterSockets"};
	
	//private String imageName = "j2.png";
	
	private String photoName = "j4.png";
	
	@RequestMapping(method = RequestMethod.GET, value = "/create")
	public List<User> create() {
		
		List<String> roles = Arrays.asList(new String[]{"ROLE_ADMIN"});			
		
		
//			String urlImage = SAVE_IMAGE_URL+imageName;
//			
//			Image image = new Image.Builder()
//					.name(imageName)
//					.url(urlImage)					
//					.build();
//			imageRepository.save(image);
		
		
			String urlPhoto = SAVE_PHOTO_URL+photoName;
			
			Photo photo = new Photo.Builder()
					.name(photoName)
					.url(urlPhoto)					
					.build();
			photoRepository.save(photo);
	
		
		for(int i=0; i <5;i++){	
			
			String firstName = userData[0]+i;
			String lastName=userData[1]+i;
			String userName=userData[2]+i;
			String email = userData[3]+i+userData[4];
			String password = userData[5];	
			
			//generate user
			User newUser = new User.Builder()
					.firstName(firstName)
					.lastName(lastName)
					.roles(roles)
					//.boxes(boxes)
					.password(password)
					.userName(userName)
					.email(email)
					.status(ACTIVE)										
					.build();
			
			userRepository.save(newUser);
			
			
//			
//			List<Box> boxes = new ArrayList<>();
//			
//			for(int m=0; m<5; m++){
//					
//					List<Item> items = new ArrayList<>();
//					
//					for(int n=0; n<5;n++){
//					
//						String name = itemData[0]+n;
//						String description=itemData[1]+n;
//						
//						//generate item
//						Item newItem = new Item.Builder().name(name)
//								.description(description)
//								.photos(photo)
//								.boxId(image.getBoxId())
//								.build();			
//						itemRepository.save(newItem);
//						items.add(newItem);
//					}	
//						
//					
//					
//					String qrName = UUID.randomUUID().toString();
//					String qrUrl = SAVE_QRCODE_URL + qrName + "." + imageType;
//					
//					try {
//						QRCodeFactory.createQRCode(qrUrl, info, size, imageType);
//					} catch (WriterException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					
//					QrCode qrCode = new QrCode.Builder()
//							.name(qrName)
//							.url(qrUrl)
//							.
//							.build();
//					
//					qrCodeRepository.save(qrCode);
//							
//					
//					String name = boxData[0]+m;
//					String description=boxData[1]+m;														
//					
////					//generate box
//					Box newBox = new Box.Builder().name(name)
//							.description(description)
//							.qrCode(qrCode)
//							.images(images)					
//							.items(items)
//							.status(FULL)
//							.build();
//					
//					boxRepository.save(newBox);
//					boxes.add(newBox);
//				}	

//			String firstName = userData[0]+i;
//			String lastName=userData[1]+i;
//			String userName=userData[2]+i;
//			String email = userData[3]+i+userData[4];
//			String password = userData[5];	
//			
//			//generate user
//			User newUser = new User.Builder()
//					.firstName(firstName)
//					.lastName(lastName)
//					.roles(roles)
//					.boxes(boxes)
//					.password(password)
//					.userName(userName)
//					.email(email)
//					.status(ACTIVE)										
//					.build();
//			
//			userRepository.save(newUser);
		}			   		   
		return userRepository.findAll();
	}
}
