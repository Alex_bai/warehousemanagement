package com.warehouse.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.warehouse.match.domain.ShapeContext;
import com.warehouse.utility.MathUtility;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class AreaAndContext {

	private float area;
	private int[] context;	
		
	private final float areaMatchThreshold = 0.5f;
		
	public boolean isAreaSimilar(ShapeContext context)
	{
		float min = this.area*(1-areaMatchThreshold);
		float max = this.area*(1+areaMatchThreshold);
		float tempArea = context.connectedComponent.getArea();
		if(tempArea<max && tempArea>=min)
		{
			return true;
		}
		return false;
	}
	
	public int getDifference(AreaAndContext tac) {
		int difference = 0;
		int tacContext[] = tac.getContext();
		int contextData[] = context;
		for(int i=0;i<contextData.length;i++)
		{			
			int distance = contextData[i]-tacContext[i];
			
			double molecule = distance*distance;			
			double denominator = contextData[i]+tacContext[i];
			
			if(denominator != 0)
			{				
				difference += molecule/denominator;
			}				
		}				
		return difference/2;
	}

	public float getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	public int[] getContext() {
		return context;
	}

	public void setContext(int[] context) {
		this.context = context;
	}
		
	public AreaAndContext(){}
	
	private AreaAndContext(AreaAndContext.Builder builder)
	{
		this.area = builder.area;
		this.context = builder.context;
	}
	
	public static class Builder{
		private float area;
		private int[] context;	
		
		public Builder(){			
		}
		
		public Builder area(float area)
		{
			this.area = area;
			return this;
		}
		
		public Builder context(int[] context)
		{
			this.context = context;
			return this;
		}
		
		public AreaAndContext build() {
			return new AreaAndContext(this);
		}
	}
		
	public String toString()
	{
		StringBuffer result = new StringBuffer();
		result.append(this.area).append("\n").append(context);
		
		return result.toString();
	}
}
