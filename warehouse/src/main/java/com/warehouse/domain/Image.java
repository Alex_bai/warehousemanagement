package com.warehouse.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

//@JsonAutoDetect(getterVisibility = Visibility.NONE, fieldVisibility = Visibility.ANY)
//@JsonFilter(value="tag")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class Image {
		
	private String name;
	private String url;	

	private List<AreaAndContext> aacs;
	
	private int usedNum;
	
	@Id
	private String id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<AreaAndContext> getAacs() {
		return aacs;
	}

	public void setAacs(List<AreaAndContext> aacs) {
		this.aacs = aacs;
	}	

	public int getUsedNum() {
		return usedNum;
	}

	public void setUsedNum(int usedNum) {
		this.usedNum = usedNum;
	}

	public Image(){}
	
	private Image(Image.Builder builder){
		this.name = builder.name;
		this.url = builder.url;
		//this.shapeContext = builder.shapeContext;		
		this.id = builder.id;
		this.aacs = builder.aacs;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Image other = (Image) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}


	public static class Builder{
		private String name;
		private String url;
		private String id;		
		private List<AreaAndContext> aacs;
	
	
	public Builder(){
		
	}
	
	public Builder name(String name){
		this.name = name;
		return this;
	}
	
	public Builder url(String url){
		this.url = url;
		return this;
	}
	
	public Builder aacs(List<AreaAndContext> aacs)
	{
		this.aacs = aacs;
		return this;
	}
	
	public Builder id(String id){
		this.id = id;
		return this;
	}
	
	public Image build(){
		return new Image(this);
	}
	
  }
	
}
