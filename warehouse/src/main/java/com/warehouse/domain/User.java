package com.warehouse.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

//@JsonAutoDetect(getterVisibility = Visibility.NONE, fieldVisibility = Visibility.ANY)
//@JsonFilter(value="tag")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class User {
	
	private String firstName;
	private String lastName;
	private String newPassword;
	
	private List<String> roles;
	
	@DBRef
	private List<Box> boxes;

	private String password;
	private String salt;
	
	@Indexed(unique = true)
	private String userName;
	private String email;
	private boolean status;//what is this mean?
	
	
	@Id
	private String id;
	
	public String getSalt() {
		return salt;
	}
	
	public void setSalt(String salt) {
		this.salt = salt;
	}

	
	public List<Box> getBoxes() {
		return boxes;
	}
	
	public void setBoxes(List<Box> boxes) {
		this.boxes = boxes;
	}
	
	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public User() { }
	
	public boolean getStatus() {
		return this.status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}
	
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	private User(User.Builder builder) {
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.email = builder.email;
		this.userName = builder.userName;
		this.status = builder.status;
		this.id = builder.id;
		this.password = builder.password;
		this.roles = builder.roles;	
		this.boxes = builder.boxes;
		this.salt = builder.salt;
		
//		if(this.salt == null) {
//			this.salt = BCrypt.gensalt(14);
//		}
//		
//		if(this.password == null) {
//			this.password = BCrypt.hashpw(this.password, this.salt);
//		}
	}
	
	public static class Builder {
		private String firstName;
		private String lastName;
		private String userName;
		private String email;
		private boolean status;
		private String id;
		private List<String> roles;
		private List<Box> boxes;
		private String password;
		private String salt;
		
		public Builder() {			
		}
		
		public Builder salt(String salt) { 
			this.salt = salt;
			return this;
		}
		
		public Builder password(String password) {
			this.password = password;
			return this;
		}
		
		public Builder roles(List<String> roles) {
			this.roles = roles;
			return this;
		}
		
		public Builder boxes(List<Box> boxes){
			this.boxes = boxes;
			return this;
		}
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
		
		public Builder status(boolean status) {
			this.status = status;
			return this;
		}
		
		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		
		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		
		public Builder userName(String userName) {
			this.userName = userName;
			return this;
		}
		
		public Builder email(String email) {
			this.email = email;
			return this;
		}
		
		public User build() {
			return new User(this);
		}
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
