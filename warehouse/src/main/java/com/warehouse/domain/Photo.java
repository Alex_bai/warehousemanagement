package com.warehouse.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
//@JsonAutoDetect(getterVisibility = Visibility.NONE, fieldVisibility = Visibility.ANY)
//@JsonFilter(value="tag")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class Photo {

	private String name;
	private String url;
	private String itemId;
	
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@Id
	private String id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Photo(){}
	

	
	private Photo(Photo.Builder builder){
		this.name = builder.name;
		this.url = builder.url;
		this.id = builder.id;
		this.itemId = builder.itemId;
	}	
	
	public static class Builder{
		private String name;
		private String url;
		private String itemId;
		private String id;
	
	
	public Builder(){
		
	}
	
	public Builder name(String name){
		this.name = name;
		return this;
	}
	
	public Builder url(String url){
		this.url = url;
		return this;
	}
	
	public Builder itemId(String itemId){
		this.itemId = itemId;
		return this;
	}
	
	public Builder id(String id){
		this.id = id;
		return this;
	}
	
	public Photo build(){
		return new Photo(this);
	}
  }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Photo other = (Photo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("id").append(id).append("name").append(name)
			.append("url").append(url);
		return sb.toString();
	}
}
