package com.warehouse.repositories.user;


public interface UpdateableUserRepository {
	public void update(String id);
}

