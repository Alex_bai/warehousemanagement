package com.warehouse.repositories.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import com.warehouse.domain.User;

public class UserRepositoryImpl implements UpdateableUserRepository {
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	@Override
	public void update(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		User oldUser = mongo.findOne(query,  User.class);
		oldUser.setStatus(!oldUser.getStatus());
		mongo.save(oldUser);
	}
}
