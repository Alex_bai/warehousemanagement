package com.warehouse.repositories.qrCode;

import org.springframework.data.mongodb.repository.MongoRepository;


import com.warehouse.domain.QrCode;

public interface QrCodeRepository extends MongoRepository<QrCode,String>{
	QrCode findById(String id);
}