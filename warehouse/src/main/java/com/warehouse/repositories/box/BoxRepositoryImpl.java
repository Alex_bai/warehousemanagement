package com.warehouse.repositories.box;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.warehouse.domain.Box;
import com.warehouse.domain.User;
import com.warehouse.repositories.user.UserRepository;

public class BoxRepositoryImpl implements UpdateableBoxRepository{
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;

	@Override
	public void update(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		Box oldBox = mongo.findOne(query,  Box.class);
		oldBox.setStatus(!oldBox.isStatus());
		mongo.save(oldBox);
	}	
}