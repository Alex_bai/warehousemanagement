package com.warehouse.repositories.item;

import com.warehouse.domain.Item;

public interface UpdateableItemRepository {
	public void update(Item item);
}

