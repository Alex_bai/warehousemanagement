package com.warehouse.repositories.userImage;

import com.warehouse.domain.UserImage;

public interface UpdateableUserImageRepository {
	public void update(UserImage userImage);
}
