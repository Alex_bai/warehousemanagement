package com.warehouse.repositories.userImage;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.warehouse.domain.UserImage;

public interface UserImageRepository extends MongoRepository<UserImage,String>,UpdateableUserImageRepository{
	
	UserImage findById(String id);
	Long deleteByBoxId(String boxId);
	List<UserImage> findByUrl(String url);
}
