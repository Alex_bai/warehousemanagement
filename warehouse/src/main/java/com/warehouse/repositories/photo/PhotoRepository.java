package com.warehouse.repositories.photo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.warehouse.domain.Photo;

public interface PhotoRepository extends MongoRepository<Photo,String>{
	Photo findById(String id);
	List<Photo> findByItemId(String itemId);	
}
