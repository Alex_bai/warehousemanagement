package com.warehouse.match.ops;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;

public class GaussianOp extends NullOp implements PluggableImageOp{

	private double alpha;
	private double sigma;
	
	public GaussianOp(double alpha, double sigma)
	{
		this.alpha = alpha;
		this.sigma = sigma;
	}
	
	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{
		int width = src.getWidth();
		int height = src.getHeight();
		if(dest == null)
		{
			dest = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		}			
		//create kernel
		float kernel[] = createKernel();
		//convolve image
		dest = new ConvolveImageOp(kernel).filter(src, null);
				
		return dest;
	}
	
	/**
	 * create kernel
	 * @return
	 */
	private float[] createKernel()
	{		
		int w = (int) Math.ceil(alpha*sigma);
		float[] kernel = new float[w*2+1];		
		for(int n=0;n<=w;n++)
		{
			double coefficient = 
					Math.exp(-(n*n)/(2*sigma*sigma))/(Math.sqrt(2*Math.PI)*sigma);
			kernel[w+n] = (float) coefficient;
			kernel[w-n] = (float) coefficient;
		}
		return normalizeKernel(kernel);		
	}
	
	/**
	 * normalize kernel value(the sum number should be 1)
	 * @param kernel
	 * @return
	 */
	private float[] normalizeKernel(float kernel[])
	{				
		float sum = 0;
		int kernelSize = kernel.length;
		for(int i=0;i<kernelSize;i++)
		{
			sum += kernel[i];
		}
		for(int i=0;i<kernelSize;i++)
		{
			kernel[i] /= sum;
		}
		return kernel;
	}	
	
	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double getSigma() {
		return sigma;
	}
	
	public void setSigma(double sigma) {
		this.sigma = sigma;
	}

	@Override
	public String getAuthorName() {
		// TODO Auto-generated method stub
		return "Chenguang Bai";
	}

	@Override
	public BufferedImageOp getDefault(BufferedImage arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
