package com.warehouse.match.ops;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;

public class PreProcessOp extends NullOp implements PluggableImageOp{

	public double alpha;
	public double sigma;	
	public int medianFilterWidth;
	public int medianFilterHeight;	
	
	public PreProcessOp(double alpha, double sigma, int medianFilterWidth, 
			int medianFilterHeight)
	{
		this.alpha = alpha;
		this.sigma = sigma;
		this.medianFilterWidth = medianFilterWidth;
		this.medianFilterHeight = medianFilterHeight;
	}
	
	@Override
	public BufferedImage filter(BufferedImage src, BufferedImage dest) {
		// TODO Auto-generated method stub
		
		if(dest == null)
		{
			dest = createCompatibleDestImage(src, src.getColorModel());
		}
		
		//use gaussian blur to deal with the image
		src = new GaussianOp(alpha, sigma).filter(src, null);		
				
		//use median filter to deal with the image
		dest = new FastMedianOp(medianFilterWidth, medianFilterHeight).filter(src, null);		
						
		return dest;
	}

	@Override
	public String getAuthorName() {
		// TODO Auto-generated method stub
		return "Chenguang Bai";
	}

	@Override
	public BufferedImageOp getDefault(BufferedImage arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
