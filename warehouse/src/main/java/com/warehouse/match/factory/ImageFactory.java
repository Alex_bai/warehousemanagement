package com.warehouse.match.factory;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

import com.warehouse.domain.AreaAndContext;
import com.warehouse.domain.Image;
import com.warehouse.domain.UserImage;
import com.warehouse.match.domain.ConnectedComponent;
import com.warehouse.match.domain.ShapeContext;
import com.warehouse.match.ops.PreProcessOp;


public class ImageFactory {
		
	public int circles = 5; 
	public int bins = 12;
	public double alpha = 1;
	public double sigma = 0.73;		
	public int medianFilterWidth = 3;
	public int medianFilterHeight = 3;		
	//the maximum value for difference between two shapes
	public int maxDiff = circles*bins*255;
	//the number of comparing shape context
	public final int shapeContextCount = 3;		
	private final double scale = 0.8;
	
	/**
	 * write image to local file
	 * @param file
	 * @param path
	 * @throws IOException
	 */
	public void writeImageToLocal(MultipartFile file, String path) throws IOException
	{
		 byte[] bytes = file.getBytes();
		 InputStream in = new ByteArrayInputStream(bytes);
		 BufferedImage bImageFromConvert = ImageIO.read(in);
		 BufferedImage dest = this.rescaleImage(bImageFromConvert);
		 ImageIO.write(dest, "png", new File(path));	         
	}
	
	public BufferedImage rescaleImage(BufferedImage src){
		
		int newHeight = (int) (src.getHeight()*(1-scale));
		int newWidth = (int) (src.getWidth()*(1-scale));
		BufferedImage dest = new BufferedImage(newWidth, newHeight, src.getType());
		for(int row=0;row<newHeight;row++){
			for(int col=0;col<newWidth;col++){						
				int newRow = (int) (row/(1-scale));					
				int newCol = (int) (col/(1-scale));	
				int rgb = src.getRGB(newCol, newRow);					
				dest.setRGB(col, row, rgb);
			}
		}
		return dest;
	}
	
	/**
	 * get area and context
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public List<AreaAndContext> getAacs(MultipartFile file) throws IOException {		
		byte[] bytes = file.getBytes();
		InputStream is = new ByteArrayInputStream(bytes);
		BufferedImage src = ImageIO.read(is);		
				
		//generate shape context
		List<ShapeContext> contexts = generateShapeContext(src);
		
		//get area and context
		List<AreaAndContext> aacs = new ArrayList<AreaAndContext>();
		for(ShapeContext shapeContext : contexts)
		{
			float area = shapeContext.getConnectedComponent().getArea();
			int context[] = shapeContext.getContext();
			AreaAndContext aac = new AreaAndContext.Builder()
					.area(area)
					.context(context)
					.build();
					
			aacs.add(aac);
		}		
		return aacs;		
	}
	
	/**
	 * generate shape context of the image
	 * @param src
	 * @return
	 */
	public List<ShapeContext> generateShapeContext(BufferedImage src)
	{
		//do gaussian blur and median filter
		PreProcessOp preProcessOp = new PreProcessOp(alpha, sigma, medianFilterWidth, medianFilterHeight);		
		BufferedImage dest = preProcessOp.filter(src, null);
		
		//get connected component
		List<ConnectedComponent> components = new ArrayList<ConnectedComponent>(
				ConnectedComponentFactory.getComponents(dest));
		
		Collections.sort(components);
		
		//remove the background 
		components.remove(0);
		
		//generate shape context
		List<ShapeContext> shapeContexts = ShapeContextFactory.getShapeContext(components, circles, bins);
		
		return shapeContexts;
	}
	
	/**
	 * analyze image information, match the image that administrator uploads with images in the database,
	 * @param file
	 * @param images
	 * @return
	 * @throws IOException
	 */
	public boolean sameImage(MultipartFile file, List<Image> images) throws IOException {
		// TODO Auto-generated method stub						
		
		byte[] bytes = file.getBytes();
		InputStream is = new ByteArrayInputStream(bytes);
		BufferedImage src = ImageIO.read(is);				
		
		//generate shape context
		List<ShapeContext> contexts = generateShapeContext(src);	
		
		//calculate difference between source photo and images in the database
		Map<String, Integer> diffData = new HashMap<String, Integer>();
		for(Image image : images)
		{
			int diff = calculateDifference(contexts, image);
			diffData.put(image.getUrl(), diff);
		}
		
		List<Map.Entry<String, Integer>> result = 
				new ArrayList<Map.Entry<String, Integer>>(diffData.entrySet());
		
		//sort the <url, diffVal> based on the diffVal
		Collections.sort(result, new Comparator<Map.Entry<String, Integer>>(){
			@Override
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				// TODO Auto-generated method stub
				return o1.getValue() - o2.getValue();
			}			
		});				
		
		System.out.println("dfference: "+result.get(0).getValue());
		
		if(result.get(0).getValue() == 0)
		{
			return true;
		}		
		return false;
	}	
	
	/**
	 * calculate difference between initial image and images in the database
	 * @param initial
	 * @param image
	 * @return
	 */
	public int calculateDifference(List<ShapeContext> initial, Image image)
	{	
		int difference = 0;
		
		//get area and context of the image
		List<AreaAndContext> tacs = image.getAacs();
		
		//if the number of initial image's shapeContext is less than 
		//destination image's shapeContext, then it is not the same
		if(initial.size() < tacs.size())
		{
			return maxDiff;
		}
		//get the number of minimum shapeContext
		int count = Math.min(initial.size(), shapeContextCount);
		
		for(int i=0;i<count;i++)
		{			
			ShapeContext context = initial.get(i);
			
			//get the similar shapeContext based on the area size
			Map<Integer, AreaAndContext> similar = getSimilarArea(context, image);
			int diff = 0;
			
			//if there is no similar area, then they are not the same
			if(similar.isEmpty() || similar == null)
			{
				diff = maxDiff;
			}else{		
				//if the similar areas contains corresponding shapeContext,
				//add the difference
				if(i < tacs.size() && similar.keySet().contains(i))				
				{					
					diff = context.getDifference(tacs.get(i).getContext());
				}else{
					diff = maxDiff;
				}			
			}
			System.out.println(diff);
			difference += diff;
		}	
		return difference/(initial.size());
	}
	
	/**
	 * get similar shapes based on the area
	 * @param context
	 * @param image
	 * @return
	 */
	public Map<Integer, AreaAndContext> getSimilarArea(ShapeContext context, Image image)
	{
		List<AreaAndContext> l2 = image.getAacs();
		Map<Integer, AreaAndContext> result = new HashMap<Integer, AreaAndContext>();
		
		for(int i=0;i<l2.size();i++)
		{
			AreaAndContext aac = l2.get(i);
			if(aac.isAreaSimilar(context))
			{
				result.put(i, aac);
			}
		}
		return result;
	}	
}
