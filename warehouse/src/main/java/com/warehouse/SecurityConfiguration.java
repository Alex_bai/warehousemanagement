package com.warehouse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;

import com.warehouse.filters.CsrfTokenFilter;
import com.warehouse.security.AuthenticationFailure;
import com.warehouse.security.AuthenticationSuccess;
import com.warehouse.security.EntryPointUnauthorizedHandler;
import com.warehouse.services.UsersService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private UsersService detailsService;

	@Autowired
	private AuthenticationFailure authFailure;
	
	@Autowired
	private AuthenticationSuccess authSuccess;
	
	@Autowired
	private EntryPointUnauthorizedHandler unauthorizedHandler;
	
	@Autowired
	public void configAuthBuilder(AuthenticationManagerBuilder authBuilder) throws Exception {
		//System.out.println("detailsService"+detailsService);
		authBuilder.userDetailsService(detailsService);
	}
		
	protected void configure(HttpSecurity http) throws Exception {
		http
			.addFilterAfter(new CsrfTokenFilter(), CsrfFilter.class)		
			.exceptionHandling()
			.authenticationEntryPoint(unauthorizedHandler)
		.and()
			.authorizeRequests()				
				.antMatchers("images/**,fonts/**,css/**,js/**,jsp/**,views/login.html,/login").permitAll()
				//.anyRequest().authenticated()
		.and()
			.formLogin()
				.successHandler(authSuccess)
				.failureHandler(authFailure)
				.loginPage("/login")
				.loginProcessingUrl("/login")
				.defaultSuccessUrl("/api/profile")			
				.usernameParameter("username")
				.passwordParameter("password")
				.permitAll()
		.and()
			.logout()
			    .invalidateHttpSession(true)
			    .clearAuthentication(true)
				.logoutUrl("/logout")
				.logoutSuccessUrl("/api/logout")
				.permitAll();
				
	}
}