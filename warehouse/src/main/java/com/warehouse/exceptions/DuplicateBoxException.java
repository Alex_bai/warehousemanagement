package com.warehouse.exceptions;

public class DuplicateBoxException extends Exception{

private static final long serialVersionUID = 1L;
	
	public DuplicateBoxException(String msg) {
		super(msg);
	}
	
	public DuplicateBoxException() {
		super();
	}
}
