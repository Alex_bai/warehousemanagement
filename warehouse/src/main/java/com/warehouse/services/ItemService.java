package com.warehouse.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.warehouse.domain.Box;
import com.warehouse.domain.Item;
import com.warehouse.domain.Photo;
import com.warehouse.domain.User;
import com.warehouse.exceptions.DuplicateItemException;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.photo.PhotoRepository;
import com.warehouse.repositories.user.UserRepository;

@Service
public class ItemService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private PhotoRepository  photoRepository;
	
	@Autowired
	private BoxService boxService;
	
	@Autowired
	private PhotoService photoService;
	
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	/**
	 * create the item
	 * @param item
	 * @return
	 * @throws DuplicateItemException
	 */
	public Item addItem(Item item){
		
		itemRepository.save(item);
		
		//the item in box is a list, so I need to save into the box as a list
		List<Item> items = new ArrayList<Item>();
		items.add(item);
		Box box = boxRepository.findById(item.getBoxId());
		if(box.getItems() == null){
			box.setItems(items);
		}else{
			box.getItems().addAll(items);
		}
		
	    boxRepository.save(box);
		return itemRepository.findOne(item.getId());
	}
	
	public Item updateItemInfo(Item item){
		Item i = itemRepository.findById(item.getId());
		i.setName(item.getName());
		i.setDescription(item.getDescription());
		return itemRepository.save(i);
	}
	
	
	/**
	 * delete the item
	 * @param boxId
	 * @param itemId
	 * @return
	 */
	public List<Item> deleteItem(String boxId,String itemId){
		
		Item item = itemRepository.findById(itemId);
		if(item.getPhotos() != null){
			for(Photo photo : item.getPhotos())
			{
				photoService.deletePhoto(itemId, photo.getId());
			}	
		}
		itemRepository.delete(itemId);
		//remove this item in the box
		Box box = boxService.removeItemInBox(boxId, itemId);
		return box.getItems();
	}
	
	/**
	 * remove the photo in the item
	 * @param itemId
	 * @param photoId
	 * @return
	 */
	public Item removePhotoInItem(String itemId,String photoId)
	{	
		Item item = itemRepository.findById(itemId);
		Photo photo = photoRepository.findById(photoId);
		item.getPhotos().remove(photo);		
		itemRepository.save(item);
		return itemRepository.findById(itemId);
	}
	
	/**
	 * add the photos to the item
	 * @param itemId
	 * @param photo
	 * @return
	 */
	public Item addPhotoInItem(String itemId,Photo photo){
		Item item = itemRepository.findById(itemId);
		item.getPhotos().add(photo);
		itemRepository.save(item);
		return itemRepository.findById(itemId);
	}
	
public Map<String,Integer> searchItemNum(String search,String specificUser){
		
		User user = new User();
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		List<Item> items = new ArrayList<Item>();	
		Query query = new Query();

		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		Criteria criteria = new Criteria();
		
		if((search.equals("")||search == null) && (specificUser.equals("")|| specificUser == null)){
			int num = (int) itemRepository.count();
			map.put("itemNum", num);
			return map;
		}else if(!search.equals("") &&  !specificUser.equals("")){
			user = userRepository.findByUserName(specificUser);
			if(user == null || user.getBoxes()==null || user.getBoxes().isEmpty()){
				return null;
			}
			List<String> boxIds = new ArrayList<String>();
			for(Box box:user.getBoxes()){
				boxIds.add(box.getId());
			}
			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			
			criteria.orOperator(criteriaArr).and("boxId").in(boxIds);
			query.addCriteria(criteria);
			items = mongo.find(query, Item.class);
				
			if(items==null || items.isEmpty()){
				map.put("itemNum", 0);
			}else{
				map.put("itemNum", items.size());
			}
			return map;
//			if(!items.isEmpty()){
//				int num = items.size();
//				map.put("itemNum", num);
//				return map;
//			}else{
//				
//				return map;
//			}
			
		}else if(!search.equals("") && (specificUser.equals("")||specificUser == null)){
			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr);
			query.addCriteria(criteria);
			items = mongo.find(query, Item.class);
			map.put("itemNum", items.size());
			return map;
		}else{
			map.put("itemNum", 0);
			return map;
		}
	}	
	
	public List<Item> searchItemByInfo(String search, String specificUser,int pageNum, int pageSize){
		User user = new User();
		
		
		List<Item> items = new ArrayList<Item>();	
		List<Item> allItems = new ArrayList<Item>();
		Query query = new Query();

		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		Criteria criteria = new Criteria();
		
		
		if((search.equals("")||search == null) && (specificUser.equals("")|| specificUser == null)){
			Page<Item> page =  itemRepository.findAll(new PageRequest(pageNum-1,pageSize));
			return page.getContent();
		}else if(!search.equals("") &&  !specificUser.equals("")){
			user = userRepository.findByUserName(specificUser);
			if(user == null || user.getBoxes()==null || user.getBoxes().isEmpty()){
				return null;
			}	
			List<String> boxIds = new ArrayList<String>();			
			for(Box box:user.getBoxes()){
				boxIds.add(box.getId());
			}
			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr).and("boxId").in(boxIds);
			query.addCriteria(criteria).with(new PageRequest(pageNum-1,pageSize));
			items = mongo.find(query, Item.class);
			allItems.addAll(items);
			
			return allItems;
//			
//			if(!allItems.equals("")){
//				return allItems;
//			}else{
//				return null;
//			}
			
		}else if(!search.equals("") && (specificUser.equals("")||specificUser == null)){
			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr);
			query.addCriteria(criteria).with(new PageRequest(pageNum-1,pageSize));
			items = mongo.find(query, Item.class);
			return items;
		}else{
			return null;
		}
	}
	
	
}
