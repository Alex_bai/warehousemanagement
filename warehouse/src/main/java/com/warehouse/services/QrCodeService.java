package com.warehouse.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.zxing.WriterException;
import com.warehouse.domain.Box;
import com.warehouse.domain.QrCode;
import com.warehouse.match.factory.QRCodeFactory;
import com.warehouse.repositories.qrCode.QrCodeRepository;

@Service
@PropertySource("classpath:config.properties")
public class QrCodeService {
	@Autowired
	private QrCodeRepository qrCodeRepository;	
	
	@Value("${saveQrCodePath}")
	private String saveQrCodePath;
	
	@Value("${extensionName}")
	private String extensionName = "png";
	
	@Value("${saveQrCodePath}")
	private String saveQRCodePath;
	
	@Value("${qrCodeSize}")
	private int qrCodeSize;
	
	/**
	 * create qr code
	 * @param box
	 * @return
	 */
	public QrCode createQrCode(Box box)
	{
		String name = UUID.randomUUID().toString()+"."+extensionName;
		String filePath = saveQRCodePath+name;
		String info = "localhost:8080/warehouseClient/api/users/"+box.getOwnerId()+"/boxes/"+box.getId();
		String url = null;
		try {
			url = QRCodeFactory.createQRCode(filePath, info, qrCodeSize, extensionName);
		} catch (WriterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		QrCode qrCode = null;
		if(url != null)
		{
			qrCode = new QrCode.Builder()
					.name(name)
					.url(url)
					.build();
			
			qrCodeRepository.save(qrCode);
		}
		
		return qrCode;
	}
	
	/**
	 * delete the qrCode from local file and the repository
	 * @param qrCode
	 */
	public void deleteQrCode(QrCode qrCode){
		String path = qrCode.getUrl();
		if(new File(path).delete()){
			qrCodeRepository.delete(qrCode.getId());
		}
	}
	
	/**
	 * get the qrCode information to show the image
	 * @param qrCodeId
	 * @return
	 * @throws IOException
	 */
	public byte[] getQrCodeData(String qrCodeId) throws IOException {
			QrCode qrCode = qrCodeRepository.findById(qrCodeId);
			String QrCodePath = qrCode.getUrl();
			File f = new File( QrCodePath);
			if( !f.exists() ) return null;
			byte[] buffer = null;
			try {
				FileInputStream fis = new FileInputStream(f);			
				ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
				byte[] b = new byte[1000];
				int n;
				while((n = fis.read(b)) != -1)
				{
					bos.write(b, 0, n);
				}
				fis.close();
				bos.close();
				buffer = bos.toByteArray();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			return buffer;
		}
}
