package com.warehouse.services;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


import com.warehouse.domain.Box;
import com.warehouse.domain.Image;
import com.warehouse.domain.Item;
import com.warehouse.domain.QrCode;
import com.warehouse.domain.User;
import com.warehouse.domain.UserImage;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.qrCode.QrCodeRepository;
import com.warehouse.repositories.user.UserRepository;
import com.warehouse.repositories.userImage.UserImageRepository;

@Service
@PropertySource("classpath:config.properties")
public class BoxService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private UserDataService userDataService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private QrCodeService qrCodeService;
	
	@Autowired
	private UserImageService userImageService;
	
	@Autowired
	private UserImageRepository userImageRepository;
	
	@Autowired
	QrCodeRepository qrCodeRepository;
	
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	
	@Value("${saveQrCodePath}")
	private String saveQRCodePath;
	
	@Value("${extensionName}")
	private String extensionName;
	
	@Value("${qrCodeSize}")
	private int qrCodeSize;
	
	/**
	 * create an box
	 * @param userId
	 * @param box
	 * @return
	 */
	public Box addBox(String userId, Box box){
		//find the user
		User user = userRepository.findById(userId);				
		//find the userImage that the box will be added
		UserImage userImage = box.getUserImage();
		String imageId = UUID.randomUUID().toString();
		userImage.setId(imageId);
		//generate boxId
	    String boxId = UUID.randomUUID().toString();
	    System.out.println("boxid"+boxId);
	    userImage.setBoxId(boxId);
	    //save user images in the table
	    System.out.println("userImage.boxid"+userImage.getBoxId());
	    
	    userImageRepository.save(userImage);	
		QrCode qrCode = qrCodeService.createQrCode(box);
		if(qrCode != null)
		{
			qrCodeRepository.save(qrCode);			
			box.setId(boxId);
			box.setQrCode(qrCode);
		    box.setOwnerId(userId);	
			box.setUserImage(userImage);
			box.setStatus(false);
			boxRepository.save(box);		
			List<Box> boxes = new ArrayList<Box>();		
			if(user.getBoxes() == null)
			{
				boxes.add(box);
			}else{
				boxes.addAll(user.getBoxes());
				boxes.add(box);
			}						
			//save the box in user
			user.setBoxes(boxes);
			userRepository.save(user);					
			//map.put("box", boxRepository.findOne(box.getId()));
			}	
			return boxRepository.findById(boxId);			
	}
	
	
	public Box updateBoxInfo(Box box){
		Box b = boxRepository.findById(box.getId());
		b.setName(box.getName());
		b.setDescription(box.getDescription());
		return boxRepository.save(b);
	}
	

	/**
	 * @param userId
	 * @param userImageId: the old image id
	 * @param box: the new box
	 * @return
	 */
	public Box updateBoxImage(String userId,String userImageId, Box box){	
		//get the new image
		UserImage userImage = userImageService.updateImage(box.getId(), box,userImageId);
		box.setUserImage(userImage);
		boxRepository.save(box);
		return box;		
	}
	
	
	
	/**
	 * delete the box
	 * @param userId
	 * @param boxId
	 * @return
	 */
	public long deleteBox(String userId, String boxId){
		Box box = boxRepository.findById(boxId);
		
		//delete all items in the box
		List<Item> items = box.getItems();
		if(items != null)
		{
			for(Item item : box.getItems())
			{
				itemService.deleteItem(boxId, item.getId());			
			}
		}
		
		if(box.getUserImage() != null){
			UserImage userImage = box.getUserImage();
			userImageService.deleteUserImage(boxId,userImage.getId());
		}
		qrCodeService.deleteQrCode(box.getQrCode());
		//delete the box
		boxRepository.delete(boxId);
		userDataService.removeBoxInUser(userId, boxId);
		return boxRepository.count();
	}
	
	/**
	 * delete the image from the box
	 * @param boxId
	 * @return
	 */
	public Box removeImageInBox(String boxId)
	{
		Box box = boxRepository.findById(boxId);
		box.setUserImage(null);
		boxRepository.save(box);
		return boxRepository.findById(boxId);
	}
	
	/**
	 * remove the item in the box
	 * @param boxId
	 * @param itemId
	 * @return
	 */
	public Box removeItemInBox(String boxId,String itemId){
		Box box = boxRepository.findById(boxId);
		Item item = itemRepository.findById(itemId);
		box.getItems().remove(item);
		boxRepository.save(box);
		return boxRepository.findById(boxId);
	} 
	
public Map<String,Integer> searchBoxNum(String search,String specificUser){
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		List<Box> boxes = new ArrayList<Box>();		
		Query query = new Query();

		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		Criteria criteria = new Criteria();
		
		map.put("boxNum", 0);
		
		if((search.equals("")||search == null) && (specificUser.equals("")|| specificUser == null)){
			int num = (int) boxRepository.count();
			map.put("boxNum", num);
			
		}else if(!search.equals("") &&  !specificUser.equals("")){
			User user = userRepository.findByUserName(specificUser);
			if(user == null){
				return null;
			}
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr).and("ownerId").is(user.getId());		
			query.addCriteria(criteria);
			boxes = mongo.find(query, Box.class);
			int num = boxes.size();
			map.put("boxNum", num);
			System.out.println("come into specific user isnot null"+num);
		}else if(!search.equals("") && (specificUser.equals("")||specificUser == null)){		
			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr);
			query.addCriteria(criteria);
			boxes = mongo.find(query, Box.class);
			int num = boxes.size();
			map.put("boxNum", num);
		}
		return map;
	}
	
	public List<Box> searchBoxByInfo(String search,String specificUser, int pageNum, int pageSize){
		User user = new User();
		List<Box> boxes = new ArrayList<Box>();		
		Query query = new Query();
		
		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		Criteria criteria = new Criteria();
		
		
		if((search.equals("")||search == null) && (specificUser.equals("")|| specificUser == null)){
			Page<Box> page  = boxRepository.findAll(new PageRequest(pageNum-1,pageSize));		
			return page.getContent();
		}else if(!search.equals("") &&  !specificUser.equals("")){
			user = userRepository.findByUserName(specificUser);
			if(user == null){
				return null;
			}			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr).and("ownerId").is(user.getId());		
			query.addCriteria(criteria).with(new PageRequest(pageNum-1,pageSize));;
			boxes = mongo.find(query, Box.class);
			return boxes;
		}else if(!search.equals("") && (specificUser.equals("")||specificUser == null)){	
			
			criteriaName.andOperator(Criteria.where("name").regex(search));
			criteriaDesc.andOperator(Criteria.where("description").regex(search));
			Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
			
			criteria.orOperator(criteriaArr);
			query.addCriteria(criteria).with(new PageRequest(pageNum-1,pageSize));
			
			boxes = mongo.find(query, Box.class);
			return boxes;
		}else{
			return boxes;
		}
	}	
	
}
