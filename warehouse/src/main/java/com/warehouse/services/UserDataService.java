package com.warehouse.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.warehouse.domain.Box;
import com.warehouse.domain.User;
import com.warehouse.exceptions.DuplicateUserException;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.user.UserRepository;

@Service
public class UserDataService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private BoxService boxDataService;
	
	
	public User saveUser(User user) throws DuplicateUserException{
		//userName = StringUtils.capitalize(userName);
		User test = userRepository.findByUserName(user.getUserName());
		if(test != null){
			throw new DuplicateUserException("User with user name '" + user.getUserName() + "' already exists.");
		}
		List<String> roles = Arrays.asList(new String[]{"ROLE_USER"});
		boolean active = true;
		//role怎么搞定??
		User u = new User.Builder().email(user.getEmail()).firstName(user.getFirstName())
				.lastName(user.getFirstName()).password(user.getPassword()).userName(user.getUserName())
				.roles(roles)
				.status(active)
				.build();
		userRepository.save(u);
		return this.userRepository.findOne(u.getId());
	}
	
	public User updateUser(User user){
		User u = userRepository.findById(user.getId());
		u.setFirstName(user.getFirstName());
		u.setLastName(user.getLastName());
		u.setEmail(user.getEmail());
		userRepository.save(u);		
		return userRepository.findOne(user.getId());
	}
	
	
	/**
	 * update user role
	 * @param userId
	 * @return
	 */
	public User updateUserRole(String userId){
		User user = userRepository.findById(userId);
		List<String> roleUser = new ArrayList<String>();
		roleUser.add("ROLE_USER");
		List<String> roleAdmin = new ArrayList<String>();
		roleAdmin.add("ROLE_ADMIN");
		System.out.println(user.getRoles().get(0));
		if(user.getRoles().get(0).equals("ROLE_USER")){
			System.out.println(user.getRoles().get(0));
			user.setRoles(null);
			user.setRoles(roleAdmin);
		}else{
			user.setRoles(roleUser);
		}
		userRepository.save(user);
		return user;
		
	}
	

	public User updateAdmin(User admin){
		System.out.println("come into update profile admin 2");
		User user = userRepository.findById(admin.getId());
		user.setFirstName(admin.getFirstName());
		user.setLastName(admin.getLastName());
		user.setEmail(admin.getEmail());
		System.out.println("the password is"+admin.getNewPassword());
		user.setPassword(admin.getNewPassword());
		userRepository.save(user);			
		return user;
	}
	
	
	
	public User removeUserBoxes(String userId, String boxId)
	{
		User user = userRepository.findById(userId);
		Box box = boxRepository.findById(boxId);
		user.getBoxes().remove(box);
		userRepository.save(user);
		return userRepository.findById(userId);
	}
	

	
	public List<User> getUsers(){
		return this.userRepository.findAll();
	}
	
    public User getUser(String userid){
    	return this.userRepository.findOne(userid);
    }
    
    //delete a user, then delete the items belong to the boxes belong to the user
    public long deleteUser(String userId){
    	User user = userRepository.findById(userId);
    	if(user.getBoxes() != null&& !user.getBoxes().isEmpty()){
	    	//delete boxes in the box table
	    	List<Box> boxes = user.getBoxes();
	    	if(boxes != null)
	    	{
	    		for(Box box: user.getBoxes()){
	        		boxDataService.deleteBox(userId, box.getId());
	        	}
	    	} 
    	}
    	//delete the user
    	userRepository.delete(userId);
    	return userRepository.count();
    }
    
    public void removeBoxInUser(String userId, String boxId)
    {
    	User user = userRepository.findById(userId);
    	Box box = boxRepository.findById(boxId);
    	user.getBoxes().remove(box);
    	userRepository.save(user);
    }

}
