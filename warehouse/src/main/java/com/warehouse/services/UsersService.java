package com.warehouse.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.warehouse.domain.User;
import com.warehouse.repositories.user.UserRepository;
import com.warehouse.security.AuthenticationFailure;

@Service
public class UsersService implements UserDetailsService {
	
	//extends MongoRepository,UpdateableUserRepository
	@Autowired
	private UserRepository userRepository;
		
	/**
	 * 用户访问应用资源之前，将会调用此方法获取用户的登录信息及对应的权限范围（ROLE_USER,ROLE_ADMIN,ROLE_LOGIN）
	 * 实现UserDetailsService 接口，主要是在loadUserByUsername方法中验证一个用户
     * 这里需要从数据库中读取验证表单提交过来的用户
	 */
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		final User user = getUserDetail(username);
		if(user == null) throw new UsernameNotFoundException("username");
		
		if( user.getRoles().contains("ROLE_ADMIN")) {
			org.springframework.security.core.userdetails.User userDetail = 
					new WarehouseUser(user.getUserName(), user.getPassword(), true, true, true, true, getAuthorities(user), user.getId());		
			
			return userDetail;
		
		}else{
			throw new UsernameNotFoundException("No such admin");
		}
	}

	/**
	 * 获取当前用户的权限，其实用户应该拥有多个角色
	 * 用户 角色权限 资源三者可以各自创建对象并关联能实现一个非常复杂的权限控制
	 * @param user
	 * @return
	 */
	public List<GrantedAuthority> getAuthorities(User user) {
		List<String> roles = user.getRoles();
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		
		if(roles != null) {
			for(String role : roles) {
				if(role.equals("ROLE_ADMIN")){
					authList.add(new SimpleGrantedAuthority(role));
				}
			}		
		}
		return authList;
	}

	//return user 包
	private User getUserDetail(String username) {
		System.out.println("get the user information1");
		User user = userRepository.findByUserName(username);
		return  user;
	}
}