'use strict';

mainApp.factory('AuthService', function ($http, Session) {

	  var authService = { user : undefined }
	  authService.isAuthenticated = function () {
	    return user != undefined;

	  };
	  authService.isAuthorized = function (authorizedRoles) {
	    if (!angular.isArray(authorizedRoles)) {
	      authorizedRoles = [authorizedRoles];
	    }
	    return (authService.isAuthenticated() &&
	      authorizedRoles.indexOf(authService.user.roles) !== -1);

	  };

	  authService.setUser = function(user) {
		  authService.user = user;
		  sessionStorage.user = JSON.stringify(user);
	  }

	  authService.getUser = function() { 
		  	if(!authService.user){
		  		authService.user = JSON.parse(sessionStorage.user);
		  	}
		  	return authService.user;
	  };
	  
	  return authService;

	});



mainApp.service('Session', function () {
	  this.create = function (sessionId, userId, userRole) {
		    this.id = sessionId;
		    this.userId = userId;
		    this.userRole = userRole;
		  };
		  this.destroy = function () {
		    this.id = null;
		    this.userId = null;
		    this.userRole = null;
		  };
		  return this;
		});

mainApp.controller('LoginController', function ($scope, $http, $window, $location, AuthService) {
	$scope.credentials = {
			username: '',
			password: ''
	};
	
	$scope.error = false;
	//$scope.message = undefined;
	
    $scope.login = function(credentials) {        	
    	var tokenValue = $('#csrf_token').val();
    	var tokenHeaderName = $('#csrf_token').attr('data-header');
    	
    	$http.defaults.headers.common[tokenHeaderName] = tokenValue;
    	var payload = 'username=' + credentials.username + '&password=' + credentials.password;
    	$http({
    			method : 'POST',
    			data : payload,
    			url : 'login',
    			headers : { "Content-Type" : 'application/x-www-form-urlencoded' }   
    		})
        	.success(function (response) {
        		console.log("response is",response);
        		
        		if(response.status == 'error'){
    				$scope.error="login fail";
    			}else if(response.status == false){
    				console.log("user is inactive");
    				$scope.error = "user is inactive";
    			}else{
    				
            		$http({
            			method : 'GET',
            			url : 'api/profile'
            		})
            		.success(function(response){
            			console.log("get user from profile",response);
            			AuthService.setUser(response);
            			            	
            			if(response.status == false){
            				$scope.error = "user is inactive";
            			}else{
            				$window.sessionStorage.setItem('userId', response.id);
                			$location.path('/users/' + response.id+'/boxes');
            			}
            		});
    			}
        		
        		
        	})
        	.error(function (data) {
        		console.log("login failure");
        		console.log(data);
        		$scope.error = "login failed";
        		$window.sessionStorage.removeItem('token');
            });
    };
});

mainApp.factory('AuthInterceptor', function ($q, $injector) {
	function getCsrfHeaderName(response) {
		return response.headers('X-CSRF-HEADER');
		return HEADER_NAME;
	};
	
	function getCsrfToken(response) {
		return response.headers(getCsrfHeaderName(response));
	};
	
	return {
		response : function(response) {			
			console.log(response.headers('X-CSRF-TOKEN'));
			if(response.status<400){
				var $http = $injector.get('$http');
				$http.defaults.headers.common[getCsrfHeaderName(response)] = getCsrfToken(response) ;
				
			}else{
				var $http = $injector.get('$http');
				console.log(HEADER_NAME);
				delete $http.defaults.headers.common[HEADER_NAME];
			}
			return response || $q.when(response);
		}
	};
});

// Register the previously created AuthInterceptor.
mainApp.config(function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
});