userServices = angular.module('userServices',['ngResource','angularSpinner']);

userServices.factory('TotalUserNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/totalUserNum')
}])

userServices.factory('Users',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId',{},{
		update:{
			method:'PUT'
		}
	});
}])

userServices.factory('UserStatus',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/status',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.controller('usersController',['$scope','$routeParams','AuthService','$route','$location','Users','TotalUserNum','UserStatus',
                                          function($scope,$routeParams,AuthService,$route,$location,Users,TotalUserNum,UserStatus){
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.admin = AuthService.user;
	
	$scope.maxSize = 5;
	$scope.currentPage = 1;
	
	$scope.users = Users.query( { adminId:$scope.adminId, pageNum:$scope.currentPage });
	
	$scope.totalUsers = TotalUserNum.get({adminId:$scope.adminId});

	$scope.gotoPage = function() {
		$scope.users = Users.query( { adminId: $scope.adminId, pageNum : $scope.currentPage });
	}

	$scope.addUser = function(){
		Users.save( { adminId : $scope.adminId } , $scope.user ,function(response){
			if(response != null){
				alert("create user successfully");
				$location.path('/admins/'+$scope.adminId);
			}
		});
	};

	$scope.deleteUser = function(userId){
		Users.remove({ 
			adminId:$scope.adminId, 
			userId:userId,
			pageNum:$scope.currentPage
		}, function(response){
			if(response != null){
				$scope.users = response.totalUsers;
			}else{
				alert("delete failed");
			}
		});
	};
	
	$scope.updateStatus = function(userId){
		UserStatus.update({adminId: $scope.adminId,userId:userId},$scope.user)};
		
		
	$scope.userInfo = function(user){
		if(user.status == true){
			$location.path('/admins/'+$scope.adminId+'/users/'+user.id+'/boxes');
		}else{
			alert("user is canceled")
		}
	}
}]);


userServices.controller('editUserController',['$scope','$routeParams','Users','AuthService','$route','$location',
                                              function($scope,$routeParams,Users,AuthService,$route,$location){
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.admin = AuthService.user;
	
	$scope.singleUser = Users.get( {adminId:$scope.adminId, userId:$scope.userId });
	
	$scope.editUser = function(){
		
		console.log("single user: "+$scope.singleUser);
		
			Users.update({
				adminId:$scope.adminId,
				userId: $scope.userId
				}, $scope.singleUser, function(response){
				if(response != null){
					alert("update user successfully");
					$location.path('/admins/'+$scope.adminId);
				}else{
					alert("update failed");
				}
			});
		};
}])


userServices.factory('Admins',['$resource',function($resource){
	return $resource('api/admins/:adminId',{},{
		update:{
			method:'PUT'
		}
	});
}])


userServices.controller('adminProfileController', ['$scope', '$routeParams', 'AuthService','$location','$route','Admins',
                                             function ($scope, $routeParams, AuthService,$location, $route,Admins) {
	$scope.adminId = $routeParams.adminId;
	$scope.admin = AuthService.user;
	
	$scope.updateProfile = function(){
		Admins.update({adminId:$scope.adminId},$scope.admin, function(response){
			if(response != null){
				alert("update admin successfully");
				$location.path('/admins/'+$scope.adminId);
			}
		});
	};
}]);


userServices.factory('UserBoxNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/userBoxNum')
}])

userServices.factory('Boxes',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateBoxInfo',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateBoxImage',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/image',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.factory('UserImage',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/userImage/:userImageId')
}]);

userServices.factory('BoxStatus',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/status',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.controller('boxesController',['$scope','$routeParams','AuthService','$route','$location','Boxes','UserBoxNum','BoxStatus',
                                       function($scope,$routeParams,AuthService,$route,$location,Boxes,UserBoxNum,BoxStatus){
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.boxId = $routeParams.boxId;
	$scope.admin = AuthService.user;
	
	$scope.maxSize = 5;
	$scope.currentPage = 1;
	
	$scope.boxes = Boxes.query( { adminId:$scope.adminId, userId:$scope.userId, pageNum:$scope.currentPage });
	
	$scope.userBoxNum = UserBoxNum.get({adminId:$scope.adminId, userId:$scope.userId});
	
	$scope.gotoPage = function() {
		$scope.boxes = Boxes.query( { adminId: $scope.adminId, userId:$scope.userId, pageNum : $scope.currentPage });
	}
	
	$scope.deleteBox = function(boxId){
		Boxes.remove({
			adminId:$scope.adminId,
			userId:$scope.userId,
			boxId:boxId,
			pageNum: $scope.currentPage
		},function(response){
			if(response != null){
				$scope.boxes = response.userBoxes;
			}else{
				alert("delete failed");
			}
		});
	};

	$scope.updateBoxStatus = function(boxId){
		BoxStatus.update({adminId: $scope.adminId,userId:$scope.userId, boxId:boxId},$scope.box)};	

}]);


userServices.controller('createBoxController',['$scope','$routeParams','AuthService','$route','$location','Boxes','ChooseImage','TotalImageNum',
                                               function($scope,$routeParams,AuthService,$route,$location,Boxes,ChooseImage,TotalImageNum){
	
	$scope.adminId = $routeParams.adminId;
	$scope.userId = $routeParams.userId;
	$scope.boxId = $routeParams.boxId;
	$scope.admin = AuthService.user;
	
	$scope.box = null;
	$scope.maxSize = 5;
	$scope.currentPage = 1;	
	
	$scope.images = ChooseImage.query( { adminId:$scope.adminId, userId:$scope.userId,pageNum:$scope.currentPage });	
	console.log("sss"+$scope.images.length);
	
	//$scope.userImageNum = TotalImageNum.get({adminId:$scope.adminId});
	
	$scope.gotoPage = function() {
		$scope.images = ChooseImage.query( { adminId: $scope.adminId,userId:$scope.userId, pageNum : $scope.currentPage });
	}	
	
	$scope.addBox = function(){	
		if($scope.box.userImage == null){
			$scope.noImage = 'Image must be choose';
		}else{
			$scope.noImage = '';
			Boxes.save({
				adminId: $scope.adminId,
				userId: $scope.userId
				}, $scope.box, function(response){
				if(response.status=='success'){
					alert("create box successfully");
					$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes');
				}else if(response.status=='same'){
					$scope.noImage = "Image has duplicated";
				}else{
					alert("create box failed");
				}
			});
		}			
	};
}])


userServices.controller('editBoxAndItemController',['$scope','$routeParams','AuthService','$route','$location','$uibModal','$log','Items','UserItemNum','Boxes','ChooseImage','updateBoxInfo','updateBoxImage',
                                                function($scope,$routeParams,AuthService,$route,$location,$uibModal,$log,Items,UserItemNum,Boxes,ChooseImage,updateBoxInfo,updateBoxImage){
		  	$scope.adminId = $routeParams.adminId;
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.admin = AuthService.user;
		  	
		  	$scope.maxSize = 5;
			$scope.currentPage = 1;
			
			$scope.animationEnabled = true;
			
			$scope.showImage = false;
			$scope.hasUserImage = true;
			
			$scope.box = Boxes.get({adminId:$scope.adminId, userId:$scope.userId, boxId:$scope.boxId});
		
			$scope.images = ChooseImage.query( { adminId:$scope.adminId,userId:$scope.userId,pageNum:$scope.currentPage });
			console.log("images"+$scope.images.length);
			
			$scope.items = Items.query( { adminId:$scope.adminId, userId:$scope.userId, boxId:$scope.boxId, pageNum:$scope.currentPage });									
			
			$scope.userItemNum = UserItemNum.get({adminId:$scope.adminId, userId:$scope.userId,boxId:$scope.boxId});
			
			$scope.gotoPage = function() {
				$scope.items = Items.query( { adminId: $scope.adminId, userId:$scope.userId, boxId:$scope.boxId, pageNum : $scope.currentPage });
			}
			
			$scope.updateBoxInfo = function(){
				updateBoxInfo.update({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
				},$scope.box)
			}
			
			$scope.changeName = function(){
				$scope.updateBoxInfo();
				$scope.form.name.$dirty = false;
				
			}
			
			$scope.changeDescription = function(){
				$scope.updateBoxInfo();
				$scope.form.description.$dirty = false;
			}
			
			$scope.open = function(size){
				var modalInstance = $uibModal.open({
					animation:$scope.animationEnabled,
					templateUrl:'chooseImage.html',
					controller:'modalInstanceCtrl',
					size:size,
					resolve:{
						images:function(){
							return $scope.images;
						}
					}
				})
				
				
				modalInstance.result.then(function (selectedImage) {
					
				      $scope.box.userImage = selectedImage;				    				      				      				      
				      //$scope.box.userImage.boxId = $scope.box.id;
				      
				      updateBoxImage.update({
							adminId:$scope.adminId,
							userId:$scope.userId,
							boxId:$scope.boxId,
						},$scope.box,function(response){
							if(response != null){
								alert("update box info successfully");
							}
						})    
				      		
				      $scope.box = Boxes.get({adminId:$scope.adminId, userId:$scope.userId, boxId:$scope.boxId});
				      
				      console.log("after");
				}, function () {
				     // $log.info('Modal dismissed at: ' + new Date());
				});
				
				
				$scope.toggleAnimation = function(){
					$scope.animationEnabled = !$scope.animationsEnabled;
				}
						
			}
			
			$scope.createItem = function(){
				if($scope.box.status == false){
					console.log("box status is"+$scope.box.status);
					$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/newItem');
				}else{
					$scope.infom="Box is full, no more items can be added";
				}
			}
			
			$scope.deleteItem = function(itemId){
				Items.remove({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:itemId,
					pageNum: $scope.currentPage
				},function(response){
					if(response != null){
						$scope.items = response.userItems;
					}else{
						alert("delete failed");
					}
				});
			};
}]);

userServices.controller('modalInstanceCtrl',['$scope','$routeParams','AuthService','$route','$uibModalInstance', 'images',
                                             function($scope,$routeParams,AuthService,$route,$uibModalInstance, images){
	
			$scope.adminId = $routeParams.adminId;
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.admin = AuthService.user;
			
		  	$scope.maxSize = 5;
			$scope.currentPage = 1;
		  	
			$scope.images = images;
			
			$scope.selected = {
					image: $scope.images[0]
			};	
			
			$scope.ok = function(){
				$uibModalInstance.close($scope.selected.image);
			}
			
			$scope.cancel = function(){
				$uibModalInstance.dismiss('cancel');
			}
	
}]);



userServices.factory('UserItemNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/userItemNum');
}]);

userServices.factory('Items',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateItemInfo',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);


userServices.factory('Photos',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/boxes/:boxId/items/:itemId/photos/:photoId')
}]);

userServices.controller('createItemController', [ '$scope','$routeParams','AuthService','$route','$http','$location',
                                             		function($scope, $routeParams,AuthService, $route,$http,$location) {
                      	$scope.adminId = $routeParams.adminId;
						$scope.userId = $routeParams.userId;
                      	$scope.boxId = $routeParams.boxId;
                      	$scope.admin = AuthService.user;
                      	
                      	$scope.filesChanged = function(elm){
                      		$scope.files = elm.files;
                      		$scope.$apply();
                      	}        
                      	
                      	$scope.addItem = function() {
                      		
                      		var fd = new FormData();               		
                      		angular.forEach($scope.myFiles, function(file){                 			
                      			fd.append('files', file);                      			
                      		});
                      		
                      		fd.append("name", $scope.name);
                      		fd.append("description", $scope.description);
                      		
                      		var createItem = 'api/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items';
                      		
                   		$http.post(createItem, fd, {
                   			transformRequest : angular.identity,
                   			headers : {
                   				'Content-Type' : undefined
                   			}
                   		}).success(function(response) {
                   			if(response != null){
                   				alert("create item successfully");
                   				$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
                   			}
                   			
                   		}).error(function(response) {
                   			
                   		});               		
                      	};
        	}]);

userServices.controller('editItemController',['$scope','$routeParams','AuthService','$route','$location','$http','Items','Photos','updateItemInfo','usSpinnerService','$rootScope',
                                                function($scope,$routeParams,AuthService,$route,$location,$http,Items,Photos,updateItemInfo,usSpinnerService,$rootScope){
			$scope.adminId = $routeParams.adminId;
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.admin = AuthService.user;
		  	
			$scope.disabled = false;
			
			$scope.maxSize = 5;
			$scope.currentPage = 1;
			
			
		  	
		  	$scope.item = Items.get({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});	  	
		  	
		  	$scope.updateItemInfo = function(){
				updateItemInfo.update({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
				},$scope.item)}
			
			$scope.changeName = function(name){
				$scope.updateItemInfo();
				$scope.form.name.$dirty = false;
			}
			
			$scope.changeDescription = function(){
				$scope.updateItemInfo();
				$scope.form.description.$dirty = false;
			}
		  	
		  	
			$scope.filesChanged = function(elm){
				$scope.showInfo = false;
          		$scope.successfully = false;
				
          		$scope.files = elm.files;
          		$scope.$apply();
          	}        
          	
          	$scope.uploadFile = function() {
          		
          		if($scope.files == null){
          			$scope.showInfo = true;
          			$scope.Info = "please choose file";
          		}
          		
          		$scope.spinneractive = false;
          		
          		if($scope.files != null){
          			$scope.disabled = true;
          			if(!scope.spinneractive){
          				usSpinnerService.spin('spipnner-1');
          				$scope.startcounter++;
          			}
          			var fd = new FormData();               		
              		angular.forEach($scope.files, function(file){                 			
              			fd.append('files', file);              			
              		});
          		}
          		
          		
          	
          		
          	var uploadUrl = 'api/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items/'+$scope.itemId+'/photos';
          		
       		$http.post(uploadUrl, fd, {
       			transformRequest : angular.identity,
       			headers : {
       				'Content-Type' : undefined
       			}
       		}).success(function(response) {           			
       			if(response != null){
       				$scope.item = Items.get({adminId:$scope.adminId,userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});
       			}
       			
       		}).error(function(response) {
       			
       		});               		
          	};	  	
		  	
		  	
		  	$scope.deletePhoto = function(photoId){
				Photos.remove({
					adminId:$scope.adminId,
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
					photoId:photoId
				},function(response){
					if(response != null){	
						$scope.item = response;				
					}
					
				})
			}   
          	
		  	$scope.editItem = function(){	
					Items.update({
						adminId: $scope.adminId,
						userId: $scope.userId,
						boxId:$scope.boxId,
						itemId:$scope.itemId
						}, $scope.item, function(response){
							if(response != null){
								$location.path('/admins/'+$scope.adminId+'/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
							}
					});		
			}
}]);

userServices.factory('TotalBoxNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/totalBoxNum');
}]);

userServices.factory('TotalItemNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/totalItemNum');
}]);

userServices.factory('AllBoxes',['$resource',function($resource){
	return $resource('api/admins/:adminId/showBoxes');
}]);

userServices.factory('AllItems',['$resource',function($resource){
	return $resource('api/admins/:adminId/showItems');
}])
userServices.factory('SearchBox',['$resource',function($resource){
	return $resource('api/admins/:adminId/searchBox');
}]);
userServices.factory('SearchItem',['$resource',function($resource){
	return $resource('api/admins/:adminId/searchItem');
}])


userServices.controller('searchController',['$scope','$routeParams','AuthService','$route','$location','SearchBox','SearchItem','AllBoxes','AllItems','TotalBoxNum','TotalItemNum','Boxes',
                                               function($scope,$routeParams,AuthService,$route,$location,SearchBox,SearchItem,AllBoxes,AllItems,TotalBoxNum,TotalItemNum,Boxes){
     	$scope.adminId = $routeParams.adminId;
     	
       	$scope.admin = AuthService.user;
       	
    	$scope.maxSize = 5;
		$scope.currentPage = 1;
		
		$scope.boxes = AllBoxes.query({adminId:$scope.adminId,pageNum:$scope.currentPage});
		
		$scope.items = AllItems.query({adminId:$scope.adminId,pageNum:$scope.currentPage});
			
		
		$scope.totalBoxNum = TotalBoxNum.get({adminId:$scope.adminId});
	
		$scope.totalItemNum = TotalItemNum.get({adminId:$scope.adminId});
			
		$scope.gotoPageBox = function() {
				$scope.boxes = AllBoxes.query( { adminId: $scope.adminId, pageNum : $scope.currentPage });
		}
		$scope.gotoPageItem = function() {
				$scope.items = AllItems.query( { adminId: $scope.adminId,  pageNum : $scope.currentPage });	
		}
		
		$scope.deleteBox = function(ownerId,boxId){
			Boxes.remove({
				adminId:$scope.adminId,
				userId:ownerId,
				boxId:boxId,
				pageNum: $scope.currentPage
			},function(response){
				if(response != null){
					$scope.boxes = response.userBoxes;
				}else{
					alert("delete failed");
				}
			});
		};
     	  	
   
    	$scope.searchProduct = function(search){
    		SearchBox.query()
    	};
}]);


//only use in the image management 
userServices.factory('TotalImageNum',['$resource',function($resource){
	return $resource('api/admins/:adminId/totalImageNum');
}])


userServices.factory('ChooseImage',['$resource',function($resource){
	return $resource('api/admins/:adminId/users/:userId/images/:imageId')
}])


userServices.factory('Images',['$resource',function($resource){
	return $resource('api/admins/:adminId/images/:imageId')
}]);


userServices.controller('imagesController',['$scope','$routeParams','AuthService','$route','$http','Images','TotalImageNum','$location','usSpinnerService','$rootScope',
                                           function($scope,$routeParams,AuthService,$route,$http,Images,TotalImageNum,$location,usSpinnerService,$rootScope){
	$scope.adminId = $routeParams.adminId;
	$scope.admin = AuthService.user;
	
	$scope.disabled = false;
	
	$scope.maxSize = 5;
	$scope.currentPage = 1;
	$scope.images = Images.query( { adminId:$scope.adminId, pageNum:$scope.currentPage });
	$scope.totalImageNum = TotalImageNum.get({adminId:$scope.adminId});	
		
	$scope.gotoPage = function() {
		$scope.images = Images.query( { adminId: $scope.adminId, pageNum : $scope.currentPage });
	}
	
	$scope.filesChanged = function(elm){
  		$scope.showInfo = false;
  		$scope.successfully = false;
  		
  		$scope.files = elm.files;
  		$scope.$apply();
  	}        
  	
  	$scope.uploadFile = function() {  
  		
  		if($scope.files == null){
  			$scope.showInfo = true;
  			$scope.Info="please choose file";
  		}
  		
  	  $scope.spinneractive = false;
  		
  		if($scope.files != null){
      		$scope.disabled = true;
              if (!$scope.spinneractive) {
                usSpinnerService.spin('spinner-1');
                $scope.startcounter++;
              }
       
      		var fd = new FormData();               		
      		angular.forEach($scope.files, function(file){                 			
      			fd.append('files', file);              			
      		});              		              	
  		
      		var uploadUrl = 'api/admins/'+$scope.adminId+'/images';
  		
		$http.post(uploadUrl, fd, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		}).success(function(response) {   
		      		     
			$scope.disabled=false;
			if(response.length==0){    
	   			if(!$scope.disabled){
	   				$scope.successfully = true;
	   				$scope.images = Images.query( { adminId: $scope.adminId, pageNum : $scope.currentPage });
	   			}	
					
	   			$scope.spinneractive = true;
	   			
	             if ($scope.spinneractive) {
	                    usSpinnerService.stop('spinner-1');
	             }
             
			}else{  
				
				$scope.spinneractive = true;
             if ($scope.spinneractive) {
                    usSpinnerService.stop('spinner-1');
            }
				
            $scope.showInfo = true;
      		$scope.Info="Image has already uploaded";

			}
		}).error(function(response) {
			
		});               		
  	};
  	}
	
	
	$scope.deleteImage = function(imageId){
		Images.remove({
			adminId: $scope.adminId,
			imageId: imageId,
			pageNum: $scope.currentPage			
		}, function(response){
			if(response != null){
				$scope.images = response.images;
			}			
		});
	};	
}]);


userServices.directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {	
			
			element.bind('change', function() {
				$parse(attrs.fileModel).assign(scope, element[0].files);
				scope.$apply();
			});
		}
	};
	
}]);








