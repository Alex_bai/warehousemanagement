'use strict';

mainApp.factory('AuthService', function ($http, Session) {

	  var authService = { user : undefined }
	  authService.isAuthenticated = function () {
	    return user != undefined;

	  };
	  authService.isAuthorized = function (authorizedRoles) {
	    if (!angular.isArray(authorizedRoles)) {
	      authorizedRoles = [authorizedRoles];
	    }
	    return (authService.isAuthenticated() &&
	      authorizedRoles.indexOf(authService.user.roles) !== -1);

	  };

	  authService.setUser = function( user ) {
		  authService.user = user;

	  }

	  authService.getUser = function( ) { return authService.user; };
	  return authService;

	});



mainApp.service('Session', function () {
	  this.create = function (sessionId, userId, userRole) {
		    this.id = sessionId;
		    this.userId = userId;
		    this.userRole = userRole;
		  };
		  this.destroy = function () {
		    this.id = null;
		    this.userId = null;
		    this.userRole = null;
		  };
		  return this;
		});

mainApp.controller('LoginController', function ($scope, $http, $window, $location, AuthService) {
	$scope.credentials = {
			username: '',
			password: ''
	};
	
	$scope.error = false;
	//$scope.message = undefined;
	
    $scope.login = function(credentials) {        	
    	var tokenValue = $('#csrf_token').val();
    	var tokenHeaderName = $('#csrf_token').attr('data-header');
    	
    	$http.defaults.headers.common[tokenHeaderName] = tokenValue;
    	var payload = 'username=' + credentials.username + '&password=' + credentials.password;
    	$http({
    			method : 'POST',
    			data : payload,
    			url : 'login',
    			headers : { "Content-Type" : 'application/x-www-form-urlencoded' }   
    		})
        	.success(function (data) {
        		console.log('login success');
        		console.log(data);
        		$scope.error = data.status === 'error';
        		$scope.error = data.error;
        		
        		if( !$scope.error ) {
        			AuthService.setUser(data);
        			$window.sessionStorage.setItem('userId', data.id);
        			$location.path('/users/' + data.id+'/boxes');
        		}
        	})
        	.error(function (data) {
        		console.log("login failure");
        		console.log(data);
        		$scope.error = true;
        		$window.sessionStorage.removeItem('token');
            });
    };
});

mainApp.factory('AuthInterceptor', function ($q, $injector) {
	function getCsrfHeaderName(response) {
		return response.headers('X-CSRF-HEADER');
	};
	
	function getCsrfToken(response) {
		return response.headers(getCsrfHeaderName(response));
	};
	
	return {
		response : function(response) {			
			console.log(response.headers('X-CSRF-TOKEN'));
			var $http = $injector.get('$http');
			$http.defaults.headers.common[getCsrfHeaderName(response)] = getCsrfToken(response) ;
			return response || $q.when(response);
		}
	};
});

// Register the previously created AuthInterceptor.
mainApp.config(function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
});