userServices = angular.module('userServices',['ngResource','angularSpinner']);

userServices.factory('User',['$resource',function($resource){
	return $resource('api/users/:userId',{},{
		update:{
			method:'PUT'
		}
	});
}])

userServices.controller('userController', ['$scope', '$routeParams', 'User', 'AuthService','$location',
                                            '$route', function ($scope, $routeParams, User, AuthService,$location, $route) {
	$scope.userId = $routeParams.userId;
	$scope.user = AuthService.user;
	
	$scope.updateProfile = function(){
	User.update({userId:$scope.userId},$scope.user,function(response){
		if(response != null){
			alert("update user successfully");
			$location.path('/users/'+$scope.userId);
		}
		});
	}
	
	$scope.addUser = function(){
	User.save($scope.singleUser, function(response){
		if(response != null){
			alert("create user successfully");
			$location.path('/');
		}
	    });
    };
}]);


userServices.factory('UserBoxNum',['$resource',function($resource){
	return $resource('api/users/:userId/userBoxNum');
}]);

userServices.factory('Boxes',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId',{},{
		update:{
			method:'PUT'
		}
	})
}]);


userServices.factory('updateBoxInfo',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateBoxImage',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/image',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.factory('BoxStatus',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/status',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.controller('boxesController',['$scope','$routeParams','AuthService','$route','$location','Boxes','UserBoxNum','BoxStatus','$http','usSpinnerService','$rootScope',
                                          function($scope,$routeParams,AuthService,$route,$location,Boxes,UserBoxNum,BoxStatus,$http,usSpinnerService,$rootScope){
  	$scope.userId = $routeParams.userId;
  	$scope.boxId = $routeParams.boxId;
  	$scope.user = AuthService.user;
  	
  	$scope.disabled = false;
	
  	$scope.maxSize = 5;
	$scope.currentPage = 1;
	
  	$scope.boxes = Boxes.query({userId:$scope.userId, pageNum:$scope.currentPage });
  	
  	$scope.userBoxNum = UserBoxNum.get({userId:$scope.userId});
	
	$scope.gotoPage = function() {
		$scope.boxes = Boxes.query( { userId: $scope.userId, pageNum : $scope.currentPage });
	}
  	
	$scope.deleteBox = function(boxId){
		Boxes.remove({
			userId:$scope.userId,
			boxId:boxId,
			pageNum: $scope.currentPage
		},function(response){
			if(response != null){
				$scope.boxes = response.userBoxes;
			}else{
				alert("delete failed");
			}
		});
	};

	
	$scope.updateBoxStatus = function(boxId){BoxStatus.update({userId:$scope.userId,boxId:boxId},$scope.box)};
	
	
	$scope.filesChanged = function(elm){
 		$scope.files = elm.files;
 		$scope.$apply();
 	}        
 	
 	$scope.uploadFile = function() {
 		
 		if($scope.files == null){
 			$scope.showInfo = true;
 			$scope.Info = "please choose file";
 		}
 		
 		$scope.spinneractive = false;
 		
 		if($scope.files != null){
 			$scope.disabled = true;
 			if(!$scope.spinneractive){
 				 usSpinnerService.spin('spinner-1');
                 $scope.startcounter++;
 			}
 			
 			
	 		var fd = new FormData();     
	 		angular.forEach($scope.files, function(file){                 			
				fd.append('file', file);                   			
			});
	 		var uploadUrl = 'api/users/'+$scope.userId+'/images';
	 		
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).success(function(response) {
				
				$scope.disabled = false;
				
				
				if(response != null){
					if(!$scope.disabled){
						$scope.successfully = true;
						$location.path('/users/'+$scope.userId+'/boxes/'+response.id+'/items');
					}
					
					$scope.spinneractive = true;
					if($scope.spinneractive){
						usSpinnerService.stop('spinner-1');
					}
				}
				
			}).error(function(response) {
				
			});
 		}
 	};	
}]);


userServices.factory('ChooseImage',['$resource',function($resource){
	return $resource('api/users/:userId/images/:imageId')
}]);

userServices.controller('createBoxController',['$scope','$routeParams','AuthService','$route','$location','Boxes','ChooseImage',
                                               function($scope,$routeParams,AuthService,$route,$location,Boxes,ChooseImage){
	
	$scope.userId = $routeParams.userId;
	$scope.boxId = $routeParams.boxId;
	$scope.user = AuthService.user;
	
	$scope.box = null;
	$scope.maxSize = 5;
	$scope.currentPage = 1;
	
	$scope.images = ChooseImage.query( { userId:$scope.userId,pageNum:$scope.currentPage });
	
	//$scope.userImageNum = TotalImageNum.get({userId:$scope.userId});
	
	$scope.gotoPage = function() {
		$scope.images = ChooseImage.query( {userId:$scope.userId, pageNum : $scope.currentPage });
	}
	
	$scope.addBox = function(){	
		if($scope.box.userImage == null){
			$scope.noImage = 'Image must be choose';
		}else{
			$scope.noImage = '';
			Boxes.save({
				userId: $scope.userId
				}, $scope.box, function(response){
				if(response != null){
					$location.path('/users/'+$scope.userId+'/boxes');
				}else{
					alert("create box failed");
				}
			});
		}			
	};
}])



userServices.controller('editBoxAndItemController',['$scope','$routeParams','AuthService','$route','$location','$uibModal','$log','Items','BoxItemNum','Boxes','ChooseImage','updateBoxInfo','updateBoxImage',
                                                function($scope,$routeParams,AuthService,$route,$location,$uibModal,$log,Items,BoxItemNum,Boxes,ChooseImage,updateBoxInfo,updateBoxImage){
	
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.user = AuthService.user;
		  	
		  	$scope.maxSize = 5;
			$scope.currentPage = 1;			
			
			$scope.animationEnabled = true;
			
			$scope.showImage = false;
			$scope.hasUserImage = true;
			
			$scope.box = Boxes.get({userId:$scope.userId, boxId:$scope.boxId});
		
			$scope.images = ChooseImage.query( {userId:$scope.userId,pageNum:$scope.currentPage });
			
			$scope.items = Items.query( { userId:$scope.userId, boxId:$scope.boxId, pageNum:$scope.currentPage });									
			
			$scope.userItemNum = BoxItemNum.get({userId:$scope.userId,boxId:$scope.boxId});
			
			$scope.gotoPage = function() {
				$scope.items = Items.query( {userId:$scope.userId, boxId:$scope.boxId, pageNum : $scope.currentPage });
			}
			
			$scope.updateBoxInfo = function(){
				updateBoxInfo.update({
					userId:$scope.userId,
					boxId:$scope.boxId,
				},$scope.box)
			}
			
			
			$scope.changeName = function(){
				$scope.updateBoxInfo();
				$scope.form.name.$dirty = false;
			}
			
			$scope.changeDescription = function(){
				$scope.updateBoxInfo();
				$scope.form.description.$dirty = false;
			}
			
			$scope.open = function(size){
				var modalInstance = $uibModal.open({
					animation:$scope.animationEnabled,
					templateUrl:'chooseImage.html',
					controller:'modalInstanceCtrl',
					size:size,
					resolve:{
						images:function(){
							return $scope.images;
						}
					}
				})
				
				
				modalInstance.result.then(function (selectedImage) {
					
				      $scope.box.userImage = selectedImage;				    				      				      				      
				      
				      updateBoxImage.update({
							userId:$scope.userId,
							boxId:$scope.boxId,
						},$scope.box,function(response){
							if(response != null){
								alert("update box info successfully");
							}
						})    
				      		
				      $scope.box = Boxes.get({ userId:$scope.userId, boxId:$scope.boxId});
				      
				      console.log("after");
				}, function () {
				     // $log.info('Modal dismissed at: ' + new Date());
				});
				
				
				$scope.toggleAnimation = function(){
					$scope.animationEnabled = !$scope.animationsEnabled;
				}
						
			}
			
			$scope.createItem = function(){
				if($scope.box.status == false){
					$location.path('/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/newItem');
				}else{
					$scope.infom="Box is full, no more items can be added";
				}
			}
			
			$scope.deleteItem = function(itemId){
				Items.remove({
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:itemId,
					pageNum: $scope.currentPage
				},function(response){
					if(response != null){
						$scope.items = response.userItems;
					}else{
						alert("delete failed");
					}
				});
			};
}]);

userServices.controller('modalInstanceCtrl',['$scope','$routeParams','AuthService','$route','$uibModalInstance', 'images',
                                             function($scope,$routeParams,AuthService,$route,$uibModalInstance, images){
	
			$scope.adminId = $routeParams.adminId;
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.admin = AuthService.user;
			
		  	$scope.maxSize = 5;
			$scope.currentPage = 1;
		  	
			$scope.images = images;
			
			$scope.selected = {
					image: $scope.images[0]
			};	
			
			$scope.ok = function(){
				$uibModalInstance.close($scope.selected.image);
			}
			
			$scope.cancel = function(){
				$uibModalInstance.dismiss('cancel');
			}
	
}]);


userServices.factory('BoxItemNum',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/BoxItemNum');
}]);

userServices.factory('Items',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateItemInfo',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('Photos',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId/photos/:photoId')
}]);

userServices.factory('photoNumPerItem',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId/photoNumPerItem')
}]);



userServices.controller('createItemController', [ '$scope','$routeParams','AuthService','$route','$http','$location',
                                           		function($scope, $routeParams,AuthService, $route,$http,$location) {
                    	$scope.userId = $routeParams.userId;
                    	$scope.boxId = $routeParams.boxId;
                    	$scope.user = AuthService.user;
                    	
                    	$scope.filesChanged = function(elm){
                    		$scope.files = elm.files;
                    		$scope.$apply();
                    	}        
                    	
                    	$scope.addItem = function() {
                    		
                    		var fd = new FormData();                    		                    		
                    		
                    		angular.forEach($scope.myFiles, function(file){                 			
                    			fd.append('files', file);                    			
                    		});
                    		
                    		fd.append("name", $scope.name);
                    		fd.append("description", $scope.description);
                    		
                    		var createItem = 'api/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items';
                    		
                    
                    		
                 		$http.post(createItem, fd, {
                 			transformRequest : angular.identity,
                 			headers : {
                 				'Content-Type' : undefined
                 			}
                 		}).success(function(response) {
                 			if(response != null){
                 				alert("create item successfully");
                 				$location.path('/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
                 			}
                 			
                 		}).error(function(response) {
                 			
                 		});               		
                    	};
      	}]);

userServices.controller('editItemController',['$scope','$routeParams','AuthService','$route','$location','$http','Items','Photos','photoNumPerItem','updateItemInfo','usSpinnerService','$rootScope',
                                              function($scope,$routeParams,AuthService,$route,$location,$http,Items,Photos,photoNumPerItem,updateItemInfo,usSpinnerService,$rootScope){
			
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.user = AuthService.user;
		  	
			$scope.disabled = false;
			
			$scope.maxSize = 5;
			$scope.currentPage = 1;
			
			$scope.photoNumPerItem =  photoNumPerItem.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});
		  	
			$scope.photos = Photos.query({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId,pageNum:$scope.currentPage})
			
			$scope.item = Items.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});	
		  	
		  	$scope.updateItemInfo = function(){
				updateItemInfo.update({
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
				},$scope.item)}
			
			$scope.changeName = function(name){
				$scope.updateItemInfo();
				$scope.form.name.$dirty = false;
			}
			
			$scope.changeDescription = function(){
				$scope.updateItemInfo();
				$scope.form.description.$dirty = false;
			}
		  	
		  	
			$scope.filesChanged = function(elm){
				$scope.showInfo = false;
        		$scope.successfully = false;
				
        		$scope.files = elm.files;
        		$scope.$apply();
        	}        
        	
        	$scope.uploadFile = function() {
        		
        		console.log("sdfsdfsdfsdf");
        		
        		if($scope.files == null){
        			$scope.showInfo = true;
        			$scope.Info = "please choose file";
        		}
        		
        		 $scope.spinneractive = false;
         		
         		if($scope.files != null){
             		$scope.disabled = true;
                     if (!$scope.spinneractive) {
                       usSpinnerService.spin('spinner-1');
                       $scope.startcounter++;
                     }
              
        		

        			var fd = new FormData();               		
            		angular.forEach($scope.files, function(file){                 			
            			fd.append('files', file);              			
            		});
        	
        		
        	var uploadUrl = 'api/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items/'+$scope.itemId+'/photos';
        		
        	console.log("url: "+uploadUrl);
        	
     		$http.post(uploadUrl, fd, {
     			transformRequest : angular.identity,
     			headers : {
     				'Content-Type' : undefined
     			}
     		}).success(function(response) {
     			
     			
     			
     			$scope.disabled = false;
     			if(!$scope.disabled){
     				$scope.successfully = true;
     				//$scope.item = Items.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});
     				$scope.photoNumPerItem =  photoNumPerItem.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});
					$scope.photos = Photos.query({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId,pageNum:$scope.currentPage})
     			}
     			
     			
     			$scope.spinneractive = true;
     			
     			if($scope.spinneractive){
     				usSpinnerService.stop('spinner-1');
     			}
     	    }).error(function(response) {
     			
     		});               		
        	};	
        	}
		  	
		  	
		  	$scope.deletePhoto = function(photoId){
				Photos.remove({
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
					photoId:photoId
				},function(response){
					if(response != null){	
						$scope.item = response;	
						$scope.photoNumPerItem =  photoNumPerItem.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});
						$scope.photos = Photos.query({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId,pageNum:$scope.currentPage})
					}
					
				})
			}   
        	
		  	$scope.editItem = function(){	
					Items.update({
						userId: $scope.userId,
						boxId:$scope.boxId,
						itemId:$scope.itemId
						}, $scope.item, function(response){
							if(response != null){
								$location.path('/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
							}
					});		
			}
}]);


userServices.factory('UserItemNum',['$resource',function($resource){
	return $resource('api/users/:userId/UserItemNum');
}]);

userServices.factory('boxItems',['$resource',function($resource){
	return $resource('api/users/:userId/items')
}]);

userServices.factory('SearchBox',['$resource',function($resource){
	return $resource('api/users/:userId/searchBox');
}]);

userServices.factory('SearchItem',['$resource',function($resource){
	return $resource('api/users/:userId/searchItem');
}]);



userServices.controller('searchController',['$scope','$routeParams','AuthService','$route','$location','SearchBox','SearchItem','Boxes','boxItems','UserBoxNum','UserItemNum','Items',
                                               function($scope,$routeParams,AuthService,$route,$location,SearchBox,SearchItem,Boxes,boxItems,UserBoxNum,UserItemNum,Items){
		
		$scope.userId = $routeParams.userId;
     	
       	$scope.user = AuthService.user;
       	
    	$scope.maxSize = 5;
		$scope.currentPage = 1;
		
		$scope.boxes = Boxes.query({userId:$scope.userId,pageNum:$scope.currentPage});
		
		$scope.items = boxItems.query({userId:$scope.userId,pageNum:$scope.currentPage});
			
		
		$scope.userBoxNum = UserBoxNum.get({userId:$scope.userId});
	
		$scope.userItemNum = UserItemNum.get({userId:$scope.userId});
			
		$scope.gotoPageBox = function() {
				$scope.boxes = Boxes.query( { userId: $scope.userId, pageNum : $scope.currentPage });
		}
		$scope.gotoPageItem = function() {
				$scope.items = boxItems.query( { userId: $scope.userId,  pageNum : $scope.currentPage });	
		}
		
		$scope.deleteBox = function(boxId){
			Boxes.remove({
				userId:$scope.userId,
				boxId:boxId,
				pageNum: $scope.currentPage
			},function(response){
				if(response != null){
					$scope.boxes = response.userBoxes;
				}else{
					alert("delete failed");
				}
			});
		};
		
		
		
	
		$scope.deleteItem = function(boxId,itemId){
			Items.remove({
				userId:$scope.userId,
				boxId:boxId,
				itemId:itemId,
				pageNum:$scope.currentPage
			},function(response){
				$scope.items = response.userItems;
			})
		}
		
   
    	$scope.searchProduct = function(search){
    		if(search==null || search==""){
    			console.log("sdfsdfsdfnull");
    			$scope.boxes = Boxes.query({userId:$scope.userId,pageNum:$scope.currentPage});	
    			$scope.items = boxItems.query({userId:$scope.userId,pageNum:$scope.currentPage});
    		}else{
    			SearchBox.query({
	    			userId:$scope.userId,
	    			search:search
	    		},function(response){
	    			if(response != null){
	    				$scope.boxes = response;
	    			}
	    		})
	    		
	    		SearchItem.query({
	    			userId:$scope.userId,
	    			search:search
	    		},function(response){
	    			if(response != null){
	    				$scope.items = response;
	    			}
	    		})
	    	}
    	};
}]);


userServices.directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {	
			
			element.bind('change', function() {
				$parse(attrs.fileModel).assign(scope, element[0].files);
				scope.$apply();
			});
		}
	};
} ]);



