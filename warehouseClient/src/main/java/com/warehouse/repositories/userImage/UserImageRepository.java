package com.warehouse.repositories.userImage;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.warehouse.domain.UserImage;

public interface UserImageRepository extends MongoRepository<UserImage,String>,UpdateableUserImageRepository{
	
	UserImage findById(String id);
	UserImage findByUrl(String url);
	Long deleteByBoxId(String boxId);
}
