package com.warehouse.repositories.userImage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.warehouse.domain.Image;
import com.warehouse.domain.UserImage;

public class UserImageRepositoryImpl implements UpdateableUserImageRepository{

	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	private Update getUpdate(UserImage x, UserImage y){
		Update update = new Update();
		update.set("url", y.getUrl());
		//update.set("shapeContext", y.getShapeContext());
		return update;
	}
	
	@Override
	public void update(UserImage userImage){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(userImage.getId()));
		UserImage old = mongo.findOne(query, UserImage.class);
		mongo.updateFirst(query,getUpdate(old,userImage), Image.class);
	}
}
