package com.warehouse.repositories.image;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.warehouse.domain.Image;

public interface ImageRepository extends MongoRepository<Image,String>,UpdateableImageRepository{
	Image findById(String id);
}
