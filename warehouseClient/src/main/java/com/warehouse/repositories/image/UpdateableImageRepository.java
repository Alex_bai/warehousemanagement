package com.warehouse.repositories.image;

import com.warehouse.domain.Image;

public interface UpdateableImageRepository {
	public void update(Image image);
}
