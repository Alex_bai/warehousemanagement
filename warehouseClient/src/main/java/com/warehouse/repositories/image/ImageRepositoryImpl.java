package com.warehouse.repositories.image;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;


import com.warehouse.domain.Image;

public class ImageRepositoryImpl implements UpdateableImageRepository{

	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	private Update getUpdate(Image x, Image y){
		Update update = new Update();
		update.set("url", y.getUrl());
		//update.set("shapeContext", y.getShapeContext());
		return update;
	}
	
	@Override
	public void update(Image image){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(image.getId()));
		Image old = mongo.findOne(query, Image.class);
		mongo.updateFirst(query,getUpdate(old,image), Image.class);
	}
}
