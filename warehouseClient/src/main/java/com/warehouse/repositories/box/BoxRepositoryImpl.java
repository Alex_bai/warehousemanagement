package com.warehouse.repositories.box;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import com.warehouse.domain.Box;

@PropertySource("classpath:config.properties")
public class BoxRepositoryImpl implements UpdateableBoxRepository {
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	@Override
	public void update(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		Box oldBox = mongo.findOne(query,  Box.class);
		oldBox.setStatus(!oldBox.isStatus());
		mongo.save(oldBox);
	} 
}