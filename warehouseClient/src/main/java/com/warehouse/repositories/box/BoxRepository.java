package com.warehouse.repositories.box;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.warehouse.domain.Box;


public interface BoxRepository extends MongoRepository<Box, String>, UpdateableBoxRepository{
	Box findById(String id);
	List<Box> findByOwnerId(String ownerId, Pageable pageable);	
	Long deleteByOwnerId(String userId);
}