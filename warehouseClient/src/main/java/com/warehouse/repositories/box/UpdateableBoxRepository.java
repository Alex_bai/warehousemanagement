package com.warehouse.repositories.box;

public interface UpdateableBoxRepository {
	public void update(String id);
}