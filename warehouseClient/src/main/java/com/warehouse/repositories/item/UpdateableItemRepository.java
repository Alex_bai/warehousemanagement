package com.warehouse.repositories.item;

public interface UpdateableItemRepository {
	public void update(String id);
}
