package com.warehouse.repositories.item;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.warehouse.domain.Item;


public interface ItemRepository extends MongoRepository<Item, String>{
	Item findById(String id);
	List<Item> findByBoxId(String boxId,Pageable pageable);
	Long deleteByBoxId(String boxId);
}
