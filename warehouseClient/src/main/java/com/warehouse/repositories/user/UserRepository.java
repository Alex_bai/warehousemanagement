package com.warehouse.repositories.user;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.warehouse.domain.User;


public interface UserRepository extends MongoRepository<User, String>, UpdateableUserRepository {
	User findByUserName(String username);
	User findById(String id);
	
}
