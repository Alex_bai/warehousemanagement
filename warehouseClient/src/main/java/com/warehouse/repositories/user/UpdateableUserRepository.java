package com.warehouse.repositories.user;

import com.warehouse.domain.User;

public interface UpdateableUserRepository {
	public void update(String id);
}

