package com.warehouse.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.warehouse.domain.Image;
import com.warehouse.domain.Item;
import com.warehouse.domain.Photo;
import com.warehouse.match.factory.ImageFactory;
import com.warehouse.repositories.photo.PhotoRepository;

@Service
@PropertySource("classpath:config.properties")
public class PhotoService {
	@Autowired
	private PhotoRepository photoRepository;
	
	@Autowired
	private ItemService itemService;
	
	@Value("${savePhotoPath}")
	private String savePhotoPath;
	
	@Value("${extensionName}")
	private String extensionName = "png";
	
	public Photo savePhoto(String itemId,MultipartFile file) throws IOException{
		ImageFactory imageFactory = new ImageFactory();
		String name = UUID.randomUUID().toString()+"."+extensionName;
		String path = savePhotoPath+name;	
		Photo photo = new Photo.Builder()
					.name(name)
					.url(path)
					.itemId(itemId)
					.build();
		imageFactory.writeImageToLocal(file, path);
		return photoRepository.save(photo);
	}		
	
	public Item deletePhoto(String itemId, String photoId){
		
		//delete from photoRepository
		Photo photo = photoRepository.findById(photoId);
		
		//delete from local file
		String photoPath = photo.getUrl();
		//delete the photo from local file
		if(new File(photoPath).delete()){
			photoRepository.delete(photoId);
		}	
		//delete the photo ref
		return	itemService.removePhotoInItem(itemId, photoId);
	}	
		
	public byte[] getPhotoData(String photoId) throws IOException {
			
			Photo photo = photoRepository.findById(photoId);
			String photoPath = photo.getUrl();
			
			File f = new File( photoPath);
	
			if( !f.exists() )
			{
				return null;
			} 
	
			byte[] buffer = null;
			try {
				FileInputStream fis = new FileInputStream(f);			
				ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
				byte[] b = new byte[1000];
				int n;
				while((n = fis.read(b)) != -1)
				{
					bos.write(b, 0, n);
				}
				fis.close();
				bos.close();
				buffer = bos.toByteArray();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			return buffer;
		}
	
}
