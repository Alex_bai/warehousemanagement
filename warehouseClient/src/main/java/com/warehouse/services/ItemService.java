package com.warehouse.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.warehouse.domain.Box;
import com.warehouse.domain.Image;
import com.warehouse.domain.Item;
import com.warehouse.domain.Photo;
import com.warehouse.domain.UserImage;
import com.warehouse.exceptions.DuplicateItemException;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.photo.PhotoRepository;

@Service
public class ItemService {
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private PhotoService photoService;
	
	@Autowired
	private PhotoRepository photoRepository;
	
	@Autowired
	private BoxService boxService;
	
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
		
	//public Item addItem( String boxId,String name, String description, List<Photo> photos) throws DuplicateItemException{
	public Item addItem(Item item){
		
		
		Box box = boxRepository.findById(item.getBoxId());
		itemRepository.save(item);
		
		List<Item> items = new ArrayList<Item>();
		items.add(item);
		if(box.getItems() == null)
		{			
			box.setItems(items);
		}else{
			box.getItems().addAll(items);
		}
		boxRepository.save(box);
		
		return itemRepository.findOne(item.getId());
	}
	
	
	public Item updateItemInfo(Item item){
		Item i = itemRepository.findById(item.getId());
		i.setName(item.getName());
		i.setDescription(item.getDescription());
		return itemRepository.save(i);
	}
	
	//delete item(delete the item photo and also delete from the folder) and then remove this item in the box
	public List<Item> deleteItem(String boxId,String itemId){
		
		//delete all the photos in the table corresponding to the item 	
		Item item = itemRepository.findById(itemId);
		if(item.getPhotos() != null){
			for(Photo photo : item.getPhotos())
			{
				photoService.deletePhoto(itemId, photo.getId());
			}	
		}
				
		//delete item in item table
		itemRepository.delete(itemId);				
		//delete item in the box
		Box box = boxService.removeItemInBox(boxId, itemId);
		return box.getItems();
	}
	
	public Long deleteByBoxId(String boxId)
	{
		return itemRepository.deleteByBoxId(boxId);
	}
	
	public Item removePhotoInItem(String itemId,String photoId)
	{	
		Item item = itemRepository.findById(itemId);
		Photo photo = photoRepository.findById(photoId);
		item.getPhotos().remove(photo);		
		itemRepository.save(item);
		return itemRepository.findById(itemId);
	}
	
	public Item addPhotoInItem(String itemId,Photo photo){
		Item item = itemRepository.findById(itemId);
		item.getPhotos().add(photo);
		itemRepository.save(item);
		return itemRepository.findById(itemId);
	}
	
	public List<Item> getItems(List<String> boxIds, int pageNum, int pageSize){
		
		if(!boxIds.isEmpty() && boxIds != null){			
			Query query = new Query();
			Criteria criteria = Criteria.where("boxId").in(boxIds);
			query.addCriteria(criteria).with(new PageRequest(pageNum-1, pageSize));
			return mongo.find(query, Item.class);
		}
		return null;
	}	
	
	public int searchItemNum(String boxId, String searchKey){
		List<Item> items = new ArrayList<Item>();		
		Query query = new Query();

		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		criteriaName.andOperator(Criteria.where("name").regex(searchKey));
		criteriaDesc.andOperator(Criteria.where("description").regex(searchKey));
		Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
		Criteria criteria = new Criteria();
		criteria.orOperator(criteriaArr).and("boxId").is(boxId);		
		query.addCriteria(criteria);
		items = mongo.find(query, Item.class);
		int num = items.size();
		return num;
	}
	
	public List<Item> searchItemByInfo(List<String> boxIds, String searchKey,int pageNum, int pageSize){
		List<Item> items = new ArrayList<Item>();		
		Query query = new Query();

		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		criteriaName.andOperator(Criteria.where("name").regex(searchKey));
		criteriaDesc.andOperator(Criteria.where("description").regex(searchKey));
		
		Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
		
		Criteria criteria = new Criteria();		
		criteria.orOperator(criteriaArr).and("boxId").in(boxIds);
		query.addCriteria(criteria).with(new PageRequest(pageNum-1,pageSize));
				
		items = mongo.find(query, Item.class);
		return items;
	}
	
}
