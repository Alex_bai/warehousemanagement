package com.warehouse.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.warehouse.domain.AreaAndContext;
//import com.warehouse.domain.AreaAndContext;
import com.warehouse.domain.Image;
import com.warehouse.match.factory.ImageFactory;
//import com.warehouse.match.factory.MatchImage;
import com.warehouse.repositories.image.ImageRepository;

@Service
@PropertySource("classpath:config.properties")
public class ImageService {
	@Autowired
	private ImageRepository imageRepository;		
	
	@Value("${saveImagePath}")
	private String saveImagePath;
	
	@Value("${extensionName}")
	private String extensionName = "png";
	
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	public boolean saveImage(MultipartFile file) throws IOException{
		
		ImageFactory matchImage = new ImageFactory();
						
		String name = UUID.randomUUID().toString()+"."+extensionName;
		String path = saveImagePath+name;
		List<AreaAndContext> aas = matchImage.getAacs(file);
		
		Image image = new Image.Builder()
				.name(name)
				.url(path)
				.aacs(aas)
				.build();
		
		matchImage.writeImageToLocal(file, path);
		imageRepository.save(image);
		Image srcImage = imageRepository.findOne(image.getId());
		if(srcImage != null)
		{
			return true;
		}		
		return false;
	}		
	
	public byte[] getImageData(String imageId) throws IOException {
		
		Image image = imageRepository.findById(imageId);
		String imagePath = image.getUrl();
		
		File f = new File( imagePath);

		if( !f.exists() ) return null;

		byte[] buffer = null;
		try {
			FileInputStream fis = new FileInputStream(f);			
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int n;
			while((n = fis.read(b)) != -1)
			{
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			buffer = bos.toByteArray();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return buffer;
	}
	
	
 public List<Image> getImages(List<String> imageNames, int pageNum, int pageSize){
		
	if(!imageNames.isEmpty() && imageNames != null){			
		Query query = new Query();
		Criteria criteria = Criteria.where("name").nin(imageNames);
		query.addCriteria(criteria).with(new PageRequest(pageNum-1, pageSize));
		return mongo.find(query, Image.class);
	}else{
		Page<Image> page = imageRepository.findAll(new PageRequest(pageNum-1,pageSize));
		return page.getContent();
	}
	}	
}

