package com.warehouse.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.warehouse.domain.Box;
import com.warehouse.domain.Item;
import com.warehouse.domain.QrCode;
import com.warehouse.domain.User;
import com.warehouse.domain.UserImage;
import com.warehouse.exceptions.DuplicateBoxException;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.qrCode.QrCodeRepository;
import com.warehouse.repositories.user.UserRepository;
import com.warehouse.repositories.userImage.UserImageRepository;

@Service
@PropertySource("classpath:config.properties")
public class BoxService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private UserImageRepository userImageRepository;
	
	@Autowired
	private UserDataService userDataService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private UserImageService userImageService;
	
	@Autowired
	private QrCodeService qrCodeService;
	
	@Autowired
	QrCodeRepository qrCodeRepository;
	
	@Value("${saveQrCodePath}")
	private String saveQRCodePath;
	
	@Value("${extensionName}")
	private String extensionName;
	
	@Value("${qrCodeSize}")
	private int qrCodeSize;
	
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
	
	public Box addBox(String userId, Box box){
		//find the user
		User user = userRepository.findById(userId);				
		//find the userImage that the box will be added
		UserImage userImage = box.getUserImage();
		String userImageId = UUID.randomUUID().toString();
		userImage.setId(userImageId);
		//generate boxId
	    String boxId = UUID.randomUUID().toString();
	    userImage.setBoxId(boxId);
	    //save user images in the table
	    
	    userImageRepository.save(userImage);
	    System.out.println(userImage.getId());
	    
		QrCode qrCode = qrCodeService.createQrCode(userId, boxId);
		if(qrCode != null)
		{
			qrCodeRepository.save(qrCode);			
			box.setId(boxId);
			box.setQrCode(qrCode);
		    box.setOwnerId(userId);	
			box.setUserImage(userImage);
			box.setStatus(false);
			boxRepository.save(box);		
			List<Box> boxes = new ArrayList<Box>();		
			if(user.getBoxes() == null)
			{
				boxes.add(box);
			}else{
				boxes.addAll(user.getBoxes());
				boxes.add(box);
			}						
			//save the box in user
			user.setBoxes(boxes);
			userRepository.save(user);					
			//map.put("box", boxRepository.findOne(box.getId()));
			}	
			return boxRepository.findById(boxId);			
	}
	
	
	public Box updateBoxInfo(Box box){
		Box b = boxRepository.findById(box.getId());
		b.setName(box.getName());
		b.setDescription(box.getDescription());
		return boxRepository.save(b);
	}
	
	
	public Box updateBoxImage(String userId,String userImageId, Box box){	
		//get the new image
		UserImage userImage = userImageService.updateImage(box.getId(), box,userImageId);
		box.setUserImage(userImage);
		boxRepository.save(box);
		return box;		
	}
	
	public long deleteBox(String userId, String boxId){
		
		Box box = boxRepository.findById(boxId);
		
		//delete all items in the box
		List<Item> items = box.getItems();
		if(items != null)
		{
			for(Item item : box.getItems())
			{
				itemService.deleteItem(boxId, item.getId());			
			}
		}
		
		//delete userImage in the box
		if(box.getUserImage() != null){
			UserImage userImage = box.getUserImage();
			userImageService.deleteUserImage(boxId,userImage.getId());
		}
		qrCodeService.deleteQrCode(box.getQrCode());
		//delete the box
		boxRepository.delete(boxId);
		userDataService.removeBoxInUser(userId, boxId);
		return boxRepository.count();
	}
	
	
	public Box removeImageInBox(String boxId,String imageId)
	{
		Box box = boxRepository.findById(boxId);
		box.setUserImage(null);
		boxRepository.save(box);
		return boxRepository.findById(boxId);
	}
	
	public Box removeItemInBox(String boxId,String itemId){
		Box box = boxRepository.findById(boxId);
		Item item = itemRepository.findById(itemId);
		box.getItems().remove(item);
		boxRepository.save(box);
		System.out.println("sss");
		
		return boxRepository.findById(boxId);
	}
	
	public int searchBoxNum(String uid, String searchKey){
		List<Box> boxes = new ArrayList<Box>();		
		Query query = new Query();

		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		criteriaName.andOperator(Criteria.where("name").regex(searchKey));
		criteriaDesc.andOperator(Criteria.where("description").regex(searchKey));
		Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
		Criteria criteria = new Criteria();
		criteria.orOperator(criteriaArr).and("ownerId").is(uid);		
		query.addCriteria(criteria);
		boxes = mongo.find(query, Box.class);
		int num = boxes.size();
		return num;
	}
	
	public List<Box> searchBoxByInfo(String uid, String searchKey,int pageNum, int pageSize){
		List<Box> boxes = new ArrayList<Box>();		
		Query query = new Query();
		Criteria criteriaName = new Criteria();
		Criteria criteriaDesc = new Criteria();
		criteriaName.andOperator(Criteria.where("name").regex(searchKey));
		criteriaDesc.andOperator(Criteria.where("description").regex(searchKey));
		Criteria criteriaArr[] = new Criteria[]{criteriaName, criteriaDesc};
		
		Criteria criteria = new Criteria();
		criteria.orOperator(criteriaArr).and("ownerId").is(uid);		
		query.addCriteria(criteria).with(new PageRequest(pageNum-1,pageSize));
		boxes = mongo.find(query, Box.class);
		return boxes;
	} 
	
	
}
