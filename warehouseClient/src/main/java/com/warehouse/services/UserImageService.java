package com.warehouse.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.warehouse.domain.Box;
import com.warehouse.domain.UserImage;
import com.warehouse.repositories.userImage.UserImageRepository;

@Service
@PropertySource("classpath:config.properties")
public class UserImageService {
	@Autowired
	private UserImageRepository userImageRepository;	
	
	@Autowired
	private BoxService boxService;
	
	
	/**
	 * @param box the new box
	 * @param userImageId the old user image id
	 * @return
	 */
	public UserImage updateImage(String boxId, Box box, String userImageId){
		UserImage image = box.getUserImage();
		image.setBoxId(boxId);
		if(!userImageId.equals("")){
			UserImage toBeDeleted = userImageRepository.findById(userImageId);
			userImageRepository.delete(toBeDeleted);
		}
		return userImageRepository.save(image);
	}
	
	
	public Box deleteUserImage(String boxId, String imageId)
	{
		//delete user image need to delete the image and delete the image from the box
		userImageRepository.delete(imageId);
		//delete the image in the box
		return boxService.removeImageInBox(boxId, imageId);
	}
	
	public byte[] getImageData(String imageId) throws IOException {
		
		UserImage image = userImageRepository.findById(imageId);
		if(image == null){
			return null;
		}
		String imagePath = image.getUrl();
		
		File f = new File( imagePath);

		if( !f.exists() ) return null;

		byte[] buffer = null;
		try {
			FileInputStream fis = new FileInputStream(f);			
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int n;
			while((n = fis.read(b)) != -1)
			{
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			buffer = bos.toByteArray();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return buffer;
	}
	
	
	
	
}

