package com.warehouse.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.warehouse.domain.Box;
import com.warehouse.domain.User;
import com.warehouse.domain.UserImage;
import com.warehouse.exceptions.DuplicateUserException;
import com.warehouse.match.factory.ImageFactory;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.user.UserRepository;
import com.warehouse.repositories.userImage.UserImageRepository;

@Service
public class UserDataService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BoxRepository boxRepository;
	
	@Autowired
	private UserImageRepository userImageRepository;
	
	public User saveUser(User user) throws DuplicateUserException{
		//userName = StringUtils.capitalize(userName);
		User test = userRepository.findByUserName(user.getUserName());
		if(test != null){
			throw new DuplicateUserException("User with user name '" + user.getUserName() + "' already exists.");
		}
		
//		if(user.getUserName().matches("^[A-z][A-z0-9]*$")&&user.getPassword().matches("(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])")){
			List<String> roles = Arrays.asList(new String[]{"ROLE_USER"});
			boolean active = true;
			//role怎么搞定??
			User u = new User.Builder().email(user.getEmail()).firstName(user.getFirstName())
					.lastName(user.getFirstName()).password(user.getPassword()).userName(user.getUserName())
					.roles(roles)
					.status(active)
					.build();
			userRepository.save(u);
			return this.userRepository.findOne(u.getId());
//		}
//		return null;
	}
	
	/**
	 * 
	 * @param file
	 * @param userId
	 * @return boxId
	 */
	public String matchImage(MultipartFile file, String userId)
	{
		String url = null;
		//get all user's images
		List<Box> boxes = userRepository.findById(userId).getBoxes();
		List<UserImage> images = new ArrayList<UserImage>();
		for(Box box : boxes)
		{
			UserImage userImage = box.getUserImage();			
			if(userImage!=null){
				images.add(box.getUserImage());
			}			
		}
		ImageFactory matchImage = new ImageFactory();		
		try {
			url =  matchImage.matchImage(file, images);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		System.out.println("url in matchImage: "+url);
		UserImage userImage = userImageRepository.findByUrl(url);
		return userImage.getBoxId();
	}

	public List<String> matchAndroidImage(MultipartFile file, String userId)
	{
		List<String> urls = new ArrayList<String>();
		List<String> boxIds = new ArrayList<String>();
		
		//get all user's images
		List<Box> boxes = userRepository.findById(userId).getBoxes();
		List<UserImage> images = new ArrayList<UserImage>();
		for(Box box : boxes)
		{
			images.add(box.getUserImage());
		}
		ImageFactory matchImage = new ImageFactory();		
		try {
			urls =  matchImage.matchAndroidImage(file, images);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		if(urls!=null && !urls.isEmpty()){
			for(String url : urls){
				UserImage userImage = userImageRepository.findByUrl(url);
				boxIds.add(userImage.getBoxId());
			}
		}				
		return boxIds;
	}
	
	public User updateUser(User user){
//		if(user.getNewPassword().matches("(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])")){
			System.out.println("come in");
			User u = userRepository.findById(user.getId());
			u.setFirstName(user.getFirstName());
			u.setLastName(user.getLastName());
			u.setEmail(user.getEmail());
			u.setPassword(user.getNewPassword());
			userRepository.save(u);		
			return userRepository.findOne(u.getId());
//		}
//		return null;
	}
	
	public void removeBoxInUser(String userId, String boxId)
	{
		User user = userRepository.findById(userId);
		Box box = boxRepository.findById(boxId);
		user.getBoxes().remove(box);
		userRepository.save(user);
	}
	
	 public List<User> updateUserStatus(String userId)
	    {
	    	User user = userRepository.findById(userId);
	    	boolean status = user.getStatus();    	    	
	    	user.setStatus(!status);    	    	
	    	userRepository.save(user);    	
	    	return getUsers();
	    }
	
	public User updateAdmin(User admin){
		User user = userRepository.findById(admin.getId());
			
				user.setFirstName(admin.getFirstName());
				user.setLastName(admin.getLastName());
				user.setEmail(admin.getEmail());
				user.setPassword(admin.getPassword());
				userRepository.save(user);			
				
				System.out.println("admin's password"+admin.getPassword());
			
			User temp = userRepository.findOne(user.getId());
		return temp;
	}
	
	public List<User> getUsers(){
		return this.userRepository.findAll();
	}
	
    public User getUser(String userid){
    	return this.userRepository.findOne(userid);
    }
    
    public long deleteUser(String userId){
    	userRepository.delete(userId);
    	return userRepository.count();
    }
    
    public User getUsers(String username){
    	User results = this.userRepository.findByUserName(username);
    	return results;  	
    }

}
