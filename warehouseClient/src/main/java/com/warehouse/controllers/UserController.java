package com.warehouse.controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.warehouse.annotations.CacheControl;
import com.warehouse.annotations.CachePolicy;
import com.warehouse.document.GeneratePDF;
import com.warehouse.domain.Box;
import com.warehouse.domain.Image;
import com.warehouse.domain.Item;
import com.warehouse.domain.Photo;
import com.warehouse.domain.User;
import com.warehouse.domain.UserImage;
import com.warehouse.exceptions.DuplicateItemException;
import com.warehouse.exceptions.DuplicateUserException;
import com.warehouse.repositories.box.BoxRepository;
import com.warehouse.repositories.image.ImageRepository;
import com.warehouse.repositories.item.ItemRepository;
import com.warehouse.repositories.photo.PhotoRepository;
import com.warehouse.repositories.user.UserRepository;
import com.warehouse.repositories.userImage.UserImageRepository;
import com.warehouse.services.BoxService;
import com.warehouse.services.ImageService;
import com.warehouse.services.ItemService;
import com.warehouse.services.PhotoService;
import com.warehouse.services.QrCodeService;
import com.warehouse.services.UserDataService;
import com.warehouse.services.UserImageService;

@RestController
@RequestMapping("/api")
@JsonFormat
@CacheControl(maxAge = 300, policy = { CachePolicy.PUBLIC })
@PropertySource("classpath:config.properties")
public class UserController {
	@Autowired
	private UserDataService userDataService;

	@Autowired
	private BoxService boxService;

	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private UserImageService userImageService;
	
	@Autowired
	private PhotoService photoService;
	
	@Autowired
	private QrCodeService qrCodeService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BoxRepository boxRepository;

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private PhotoRepository photoRepository;
	
	@Autowired
	private UserImageRepository userImageRepository;	
	
	@Value("${pageSize}")
	private int pageSize;	
	
	public List<String> rolesAsList(String... roles) {
		return Arrays.asList(roles);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/profile")
	public User login(Principal principal) {
		System.out.println("dao le me2?");
		User user = userRepository.findByUserName(principal.getName());
		// user.setKey(null);
		user.setPassword(null);
		return user;
	}

	
	@RequestMapping(method = RequestMethod.POST, value = "/logout")
	@ResponseBody
	public Map<String,String> logout(HttpServletRequest req) {
		req.getSession().invalidate();
		Map<String, String> result = new HashMap<>();
		result.put("status",  "ok");
		return result;
	}
	
	/**
	 * get the user information
	 * @param userId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}")
	@PreAuthorize("principal.id == #userId")
	public User getUser(@PathVariable String userId) {
		return userRepository.findById(userId);
	}
	
	/**
	 * update the user information
	 * @param userId
	 * @param user
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}")
	@PreAuthorize("principal.id == #userId")
	public User updateUser(@PathVariable String userId, @RequestBody User user, HttpServletResponse response) {
		
		System.out.println("new password "+user.getNewPassword());
		
		return userDataService.updateUser(user);
	}

	/**
	 * for a user to register
	 * @param user
	 * @return
	 * @throws DuplicateUserException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/users")
	public User register(@RequestBody User user)throws DuplicateUserException {
		return userDataService.saveUser(user);
	}

	/**
	 * get the box number of the user
	 * @param userId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/userBoxNum")
	@PreAuthorize("principal.id == #userId")	
	@ResponseBody
	public Map<String,Integer> getUserBoxNum(@PathVariable String userId,@RequestParam String search) {	
		Map<String,Integer> map = new HashMap<String,Integer>();
		if(search == null || search ==""){
			List<Box> boxes= userRepository.findById(userId).getBoxes();
			if(boxes == null){
				map.put("userBoxNum", 0);
				return map;
			}else{
				int num = boxes.size();
				map.put("userBoxNum", num);
				return map;
			}
		}else{
			int num = boxService.searchBoxNum(userId, search);
			map.put("userBoxNum", num);
			return map;
		}
	}
	
	/**
	 * get the boxes belong to a user
	 * @param userId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/boxes")
	@PostAuthorize("principal.id == #userId")
	public List<Box> getBoxes(@PathVariable String userId,@RequestParam String pageNum) {
		List<Box> boxes = boxRepository.findByOwnerId(userId, new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		return boxes;
	}

	
	
	/**
	 * change the status of a box
	 * @param userId
	 * @param boxId
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/boxes/{boxId}/status")
	@PostAuthorize("principal.id == #userId")
	public void changeBoxStatus(@PathVariable String userId, @PathVariable String boxId) {
		boxRepository.update(boxId);
	}

	/**
	 * create a new box
	 * @param userId
	 * @param box
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/boxes")
	@PreAuthorize("principal.id == #userId")	
	public Box createBox(@PathVariable String userId,@RequestBody Box box){		
		return boxService.addBox(userId, box);
	}

	/**
	 * get the box information
	 * @param userId
	 * @param boxId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/boxes/{boxId}")
	@PostAuthorize("principal.id == #userId")
	@ResponseBody
	public Box BoxInfo(@PathVariable String userId, @PathVariable String boxId) {
		Box box = boxRepository.findById(boxId);
		return box;
	}

	/**
	 * delete a box
	 * @param userId
	 * @param boxId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}/boxes/{boxId}")
	@PostAuthorize("principal.id == #userId")
	public Map<String,List<Box>> deleteBox(@PathVariable String userId,@PathVariable String boxId,@RequestParam String pageNum) {
		Map<String, List<Box>> map = new HashMap<String,List<Box>>();
		boxService.deleteBox(userId, boxId);
		List<Box> boxes = boxRepository.findByOwnerId(userId, new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		map.put("userBoxes",boxes);
		return map;
	}
	
	/**
	 * update box name and description
	 * @param userId
	 * @param boxId
	 * @param box
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/boxes/{boxId}/info")
	@PreAuthorize("principal.id == #userId")
	public Box updateBoxInfo(@PathVariable String userId,@PathVariable String boxId,@RequestBody Box box){
		return boxService.updateBoxInfo(box);
	}
	
	
	/**
	 * update box image
	 * @param userId
	 * @param boxId
	 * @param box
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/boxes/{boxId}/image")
	@PreAuthorize("principal.id == #userId")
	public Box updateBoxImage( @PathVariable String userId,@PathVariable String boxId,@RequestBody Box box){
		Box b = boxRepository.findById(boxId);	
		Box res = null;
		if(b.getUserImage() == null){
			res = boxService.updateBoxImage(userId,"",box);
		}else{
			//get old image id
			String userImageId = b.getUserImage().getId();
			res = boxService.updateBoxImage(userId,userImageId,box);
		}
		
		return res;
	}
	

	/**
	 * get the number of the item belong to a box
	 * @param userId
	 * @param boxId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/boxes/{boxId}/BoxItemNum")
	@PreAuthorize("principal.id == #userId")	
	@ResponseBody
	public Map<String,Integer> boxItemNum(@PathVariable String userId,@PathVariable String boxId) {		
		Map<String, Integer> map = new HashMap<String, Integer>();		
		List<Item> items= boxRepository.findById(boxId).getItems();
		if(items == null){
			map.put("boxItemNum", 0);
			return map;
		}else{
			int num = items.size();
			map.put("boxItemNum", num);
			return map;
		}
	}
	
	
	
	
	/**
	 * get the items
	 * @param userId
	 * @param boxId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/boxes/{boxId}/items")
	@PostAuthorize("principal.id == #userId")	
	public List<Item> getItems(@PathVariable String userId,@PathVariable String boxId, @RequestParam String pageNum) {
		List<Item> items = itemRepository.findByBoxId(boxId,new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		return items;
	}
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/items")
	@PostAuthorize("principal.id == #userId")	
	public List<Item> getUserItems(@PathVariable String userId,@RequestParam String pageNum) {
		
		List<Box> boxes = userRepository.findById(userId).getBoxes();
		List<String> boxIds = new ArrayList<String>();
		for(Box box:boxes){
			boxIds.add(box.getId());
		}
		List<Item> items = itemService.getItems(boxIds, Integer.parseInt(pageNum), pageSize);
		return items;
	}
	
    /**
     * get the information of the item
     * @param userId
     * @param boxId
     * @param itemId
     * @return
     */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/boxes/{boxId}/items/{itemId}")
	@PostAuthorize("principal.id == #userId")	
	public Item itemInfo(@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId) {
		Item item = itemRepository.findById(itemId);
		return item;
	}	
	
	/**
	 * delete the item
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}/boxes/{boxId}/items/{itemId}")
	@PreAuthorize("principal.id == #userId")	
	public Map<String, List<Item>>  deleteItem(@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId,@RequestParam String pageNum) {
		Map<String, List<Item>> map = new HashMap<String,List<Item>>();
		itemService.deleteItem(boxId, itemId);
		List<Item> items = itemRepository.findByBoxId(boxId,new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
		System.out.println("item size: "+items.size());
		map.put("userItems",items);
		return map;
	}	
	
	
	
	
	/**
	 * create a new item
	 * @param userId
	 * @param boxId
	 * @param name
	 * @param description
	 * @param files
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/boxes/{boxId}/items")
	@PreAuthorize("principal.id == #userId")
	public Item createItem(@PathVariable String userId,  @PathVariable String boxId,@RequestParam String name, 
			@RequestParam String description,
			@RequestParam(value="files", required=true) MultipartFile[] files){
		
		List<Photo> photos = new ArrayList<Photo>();
		
		String itemId = UUID.randomUUID().toString();
		
		if(files != null && files.length != 0)
		{			
			System.out.println("photos not null");
			for(int i=0;i<files.length;i++)
			{
				MultipartFile file = files[i];
				
				try {
					Photo photo = photoService.savePhoto(itemId,file);
					photos.add(photo);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}else{
			System.out.println("photos are null");
		}
		
		
		Item item = new Item.Builder()
					.id(itemId)
					.name(name)
					.description(description)
					.boxId(boxId)
					.photos(photos)
					.build();	
		
			 item =  itemService.addItem(item);
		
			 return item;
	}
	
		
	@RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/boxes/{boxId}/items/{itemId}/photos")
	@PostAuthorize("principal.id == #userId")			
	public Item uploadPhoto(@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId,  
			@RequestParam(value="files", required=true) MultipartFile[] files){									

		if(files != null && files.length != 0)
		{											
			for(int i=0;i<files.length;i++)
			{
				MultipartFile file = files[i];			
				try {															
					   Photo photo = photoService.savePhoto(itemId,file);
					   itemService.addPhotoInItem(itemId, photo);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}	
		}
		return itemRepository.findById(itemId);
	}
	
	
	/**
	 * get the photos of items
	 * @param userId
	 * @param boxId
	 * @param itemId
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/boxes/{boxId}/items/{itemId}/photos")
	@PostAuthorize("principal.id == #userId")	
	public List<Photo> getPhotos(@PathVariable String userId,@PathVariable String boxId,
							@PathVariable String itemId){
		List<Photo> photos = photoRepository.findByItemId(itemId);
		return photos;
	}
	
	
	@RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/boxes/{boxId}/items/{itemId}/info")
	@PreAuthorize("principal.id == #userId")
	public Item updateBoxInfo( @PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId,@RequestBody Item item){
		return itemService.updateItemInfo(item);
	}

	
	@RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}/boxes/{boxId}/items/{itemId}/photos/{photoId}")
	@PreAuthorize("principal.id == #userId")	
	public Item deletePhoto(@PathVariable String userId,@PathVariable String boxId,@PathVariable String itemId, @PathVariable String photoId) {
		Item item = photoService.deletePhoto(itemId,photoId);
		return item;
	}
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/searchBox")
	@PostAuthorize("principal.id == #userId")	
	public List<Box> searchBoxes(@PathVariable String userId, @RequestParam String search,@RequestParam String pageNum) {
		Collection<Box> boxesColl = new ArrayList<Box>();
		HashSet<Box> boxes = new HashSet<Box>();
		User user = userRepository.findById(userId);
		boxes.addAll(boxService.searchBoxByInfo(user.getId(), search, Integer.parseInt(pageNum), pageSize));
		boxesColl.addAll(boxes);
		return (List<Box>) boxesColl;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/userItemNum")
	@PreAuthorize("principal.id == #userId")	
	@ResponseBody
	public Map<String,Integer> userItemNum(@PathVariable String userId, @RequestParam String search) {		
		Map<String, Integer> map = new HashMap<String, Integer>();
		if(search==null||search==""){
			List<Box> boxes = userRepository.findById(userId).getBoxes();
			List<Item> items = new ArrayList<>();
			if(boxes != null){
				for(Box box: boxes){
					if(box.getItems() != null){
						List<Item> i = box.getItems();
						items.addAll(i);
					}
				}
				int num = items.size();
				map.put("userItemNum", num);
				return map;
			}else{
				map.put("userItemNum", 0);
				return map;
			}
		}else{
			int num2=0;
			List<Box> boxes = userRepository.findById(userId).getBoxes();
			if(boxes != null){
				for(Box box: boxes){
					if(box.getItems() != null){
						num2 += itemService.searchItemNum(box.getId(), search);
					}
				}
				map.put("userItemNum", num2);
			}
			return map;
		}
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/searchItem")
	@PostAuthorize("principal.id == #userId")	
	public List<Item> searchItems(@PathVariable String userId, @RequestParam String search, @RequestParam String pageNum) {
		User user = userRepository.findById(userId);
		List<String> boxIds = new ArrayList<String>();
		for(Box box:user.getBoxes()){
			boxIds.add(box.getId());
		}
		List<Item >items = itemService.searchItemByInfo(boxIds, search, Integer.parseInt(pageNum), pageSize);
		return items;
	}
	
	

	
	/**
	 * update image to find the box
	 * @param userId
	 * @param file
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/images")
	@PostAuthorize("principal.id == #userId")
	public Box matchImage(@PathVariable String userId,  
			@RequestParam(value="file", required=true) MultipartFile file){		
		String boxId = userDataService.matchImage(file, userId);	
		Box box = boxRepository.findById(boxId);
		return box;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/androidImages")
	@PostAuthorize("principal.id == #userId")
	@ResponseBody
	public List<Box> matchAndroidImage(@PathVariable String userId,  
			@RequestParam(value="file", required=true) MultipartFile file){		
		List<Box> boxes = new ArrayList<Box>();
		List<String> boxIds = userDataService.matchAndroidImage(file, userId);	
		
		if(boxIds!=null && !boxIds.isEmpty()){
			for(String boxId : boxIds){
				Box box = boxRepository.findById(boxId);
				boxes.add(box);
			}
		}		
		
		return boxes;
	}
	
	/**
	 * the image num that can be choose
	 * @param userId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/imageNumCanChoose")
	@PostAuthorize("principal.id == #userId")
	public Map<String,Integer> imageNumCanChoose(@PathVariable String userId){		
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		List<Box> boxes = userRepository.findById(userId).getBoxes();	
		if(boxes == null || boxes.isEmpty()){						
			int num = (int) imageRepository.count();
			map.put("imageNumCanChoose", num);
			return map;
		}		
		
		List<String> imageDel = new ArrayList<String>();
		for(Box box: boxes){
			if(box.getUserImage() != null){				
				UserImage image = box.getUserImage();				
				imageDel.add(image.getName());	
			}													
		}
		int num = (int)imageRepository.count()-imageDel.size();
		map.put("imageNumCanChoose", num);
		return map;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "users/{userId}/images" )
	@PreAuthorize("principal.id == #userId")
	public List<Image> getImages(@PathVariable String userId,@RequestParam String pageNum){
						
		List<Box> boxes = userRepository.findById(userId).getBoxes();	
		
		if(boxes == null || boxes.isEmpty()){						
			Page<Image> page = imageRepository.findAll(new PageRequest(Integer.parseInt(pageNum)-1,pageSize));
			List<Image> images = page.getContent();
			return  images;
		}		
		
		List<String> imageDel = new ArrayList<String>();
		for(Box box: boxes){
			if(box.getUserImage() != null){				
				UserImage image = box.getUserImage();				
				imageDel.add(image.getName());			
			}													
		}
		List<Image> images = imageService.getImages(imageDel, Integer.parseInt(pageNum), pageSize);
		return images;	
	}
	


	/**
	 * get the image from local file
	 * @param userId
	 * @param imageId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}/images/{imageId}/image" )		
	public ResponseEntity<byte[]> getImage(@PathVariable String userId, 			
			@PathVariable String imageId) throws Exception {
		
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( imageService.getImageData(imageId), headers, HttpStatus.OK );
	}
	
	/**
	 * get the image from local file
	 * @param userId
	 * @param imageId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}/userImage/{imageId}" )	
	public ResponseEntity<byte[]> getUserImage(@PathVariable String userId, 			
			@PathVariable String imageId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( userImageService.getImageData(imageId), headers, HttpStatus.OK );
	}
	
	/**
	 * get the photo from local file
	 * @param userId
	 * @param photoId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}/photos/{photoId}/photo" )		
	public ResponseEntity<byte[]> getPhoto(@PathVariable String userId, 			
			@PathVariable String photoId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( photoService.getPhotoData(photoId), headers, HttpStatus.OK );
	}
	
	/**
	 * get the qrCode from local file
	 * @param userId
	 * @param qrCodeId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}/qrCodes/{qrCodeId}/qrCode" )		
	public ResponseEntity<byte[]> getQrCode(@PathVariable String userId, 			
			@PathVariable String qrCodeId) throws Exception {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
	    return new ResponseEntity<byte[]>( qrCodeService.getQrCodeData(qrCodeId), headers, HttpStatus.OK );
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}/box/{boxId}/labels" )	
	public ResponseEntity<byte[]> getPdf(@PathVariable String userId, @PathVariable String boxId) throws Exception {
		Box box = boxRepository.findById(boxId); 
	    Map<String, String> iwq = new HashMap<String, String>();
	    iwq.put(box.getUserImage().getUrl(), box.getQrCode().getUrl());
	    
	    GeneratePDF getPdf = new GeneratePDF();
	    String fileName = getPdf.getImages(iwq);
		
		 FileInputStream fi = new FileInputStream(new File(fileName));
	     ByteArrayOutputStream baos = new ByteArrayOutputStream();

	        int c;
	        while ((c = fi.read()) != -1) {
	            baos.write(c);
	        }

	        fi.close();
	        byte[] pdf = baos.toByteArray();
	        baos.close();
		   
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentLength(pdf.length);
		      headers.setContentType(MediaType.parseMediaType("application/pdf"));
		        headers.set("Content-Disposition", "inline; filename=test.pdf");
		        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		        ResponseEntity<byte[]> responseE = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
		        return responseE;
	}	
	
	
	@RequestMapping(method=RequestMethod.GET, value="/users/{userId}/labels" )	
	public ResponseEntity<byte[]> getPdfs(@PathVariable String userId,@RequestParam String boxes) throws Exception{
		
		Map<String,String> imageWithQrCode = new HashMap<String,String>();
		GeneratePDF getPdf = new GeneratePDF();
		String[] boxIds = boxes.split(",");
		
		if(!boxes.equals("")){
			
			for(String id: boxIds){
				Box box = boxRepository.findById(id);
				String imageUrl = box.getUserImage().getUrl();;							
				String qrCodeUrl = box.getQrCode().getUrl();
				imageWithQrCode.put(imageUrl, qrCodeUrl);
			}
			String fileName = getPdf.getImages(imageWithQrCode);
			FileInputStream fi = new FileInputStream(new File(fileName));
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();

		        int c;
		        while ((c = fi.read()) != -1) {
		            baos.write(c);
		        }

		        fi.close();
		        byte[] pdf = baos.toByteArray();
		        baos.close();
			   
			    HttpHeaders headers = new HttpHeaders();
			    headers.setContentLength(pdf.length);
			    headers.setContentType(MediaType.parseMediaType("application/pdf"));
			    headers.set("Content-Disposition", "inline; filename=test.pdf");
			    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			    ResponseEntity<byte[]> responseE = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
			    return responseE;
		}
		return null;
	}
}	

