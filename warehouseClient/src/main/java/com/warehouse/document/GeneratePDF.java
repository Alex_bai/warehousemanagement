package com.warehouse.document;

import java.io.FileOutputStream;
import java.util.Map;
import java.util.UUID;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

public class GeneratePDF {
	private int rowNum;
	private int colNum;
	private int pages;
	private int imagesInOnePage;
	private final int imageWidth = 100;
	private final int TOP_MARGIN = 30;
	private final int LEFT_MARGIN = 30;
	private final String path = "/home/centos/warehouseData/pdfFile/";

	public String getImages(Map<String,String> pdfInfor){		
		String name = UUID.randomUUID().toString();
		String fileName = path+name+".pdf";
		Document document = new Document(PageSize.A4);
		float width = document.getPageSize().getWidth();
		float height = document.getPageSize().getHeight();	
		getRowAndColNum(width, height);
		imagesInOnePage = this.rowNum * this.colNum;
		pages = pdfInfor.size()/imagesInOnePage;
		try {
			 FileOutputStream output = new FileOutputStream(fileName);
			 PdfWriter.getInstance(document,output);
			 document.open();
			 int i = 0;
			 float initialHeightVal = height-imageWidth-TOP_MARGIN;
			 float heightVal = initialHeightVal;
			 int rowNum = 0;
			 for(String imageUrl : pdfInfor.keySet()){				 
				 if(i >= imagesInOnePage){					 
					 document.newPage();
			         heightVal = initialHeightVal;
			         i = 0;
			         rowNum = 0;
				 }				 
				 Image image1 = Image.getInstance(imageUrl);
			        image1.scaleAbsoluteWidth(imageWidth);
					image1.scaleAbsoluteHeight(imageWidth);			        				        		
				    int index = i % imagesInOnePage;
				    int row = index / this.colNum;
				    int col = index % this.colNum;			        	
				    int x = col * (imageWidth + LEFT_MARGIN) + LEFT_MARGIN;				   
				    if(row != rowNum){
				        heightVal = heightVal-imageWidth-TOP_MARGIN;
				       	rowNum = row;
				    }			        					    
				    image1.setAbsolutePosition(x, heightVal);		
				    document.add(image1);
				       i++;
				       String qrCodeUrl = pdfInfor.get(imageUrl);
				       Image image2 = Image.getInstance(qrCodeUrl);
			           image2.scaleAbsoluteWidth(imageWidth);
					   image2.scaleAbsoluteHeight(imageWidth);			        				        		
				       index = i % imagesInOnePage;
				       row = index / this.colNum;
				       col = index % this.colNum;			        	
				       x = col * (imageWidth + LEFT_MARGIN) + LEFT_MARGIN;				      
				        	
				       image2.setAbsolutePosition(x, heightVal);		
				       document.add(image2);
				       i++;				 
			       }
			       document.close(); 
			    } catch(Exception e){
			      e.printStackTrace();
			    }
			return fileName;
		}	

	
	public void getRowAndColNum(float width, float height){
		this.rowNum = (int) (height/(imageWidth+TOP_MARGIN));
		this.colNum = (int) (width/(imageWidth+LEFT_MARGIN));
	}
}
