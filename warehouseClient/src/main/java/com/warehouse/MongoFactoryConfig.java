package com.warehouse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories
public class MongoFactoryConfig extends AbstractMongoConfiguration {
    @Value("warehouse") 
    private String dbName;

    @Bean
    protected MongoDbFactory defaultMongoDbFactory() throws Exception {
        return new SimpleMongoDbFactory(mongo(), dbName);
    }

    @Bean
    protected MongoTemplate defaultMongoTemplate() throws Exception {
        return new MongoTemplate(defaultMongoDbFactory());
    }

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient();
    }

    @Override
    //Return the base package to scan for mapped Documents. 返回基础包扫描映射文件
    protected String getMappingBasePackage() {
        return "warehouse";
    }

    @Override
    public MongoTemplate mongoTemplate() throws Exception {
        return defaultMongoTemplate();
    }    
}