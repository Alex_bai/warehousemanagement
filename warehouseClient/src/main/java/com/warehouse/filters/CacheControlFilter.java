package com.warehouse.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class CacheControlFilter { //implements javax.servlet.Filter {

	//@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse sr = (HttpServletResponse)response;
		sr.setHeader("Cache-Control", "max-age=300");
		System.out.println("added cache-control header");
		chain.doFilter(request, response);
		
		// TODO Auto-generated method stub
		
	}

	//@Override
	public void destroy() {
		// TODO Auto-generated method stub			
	}    	
}