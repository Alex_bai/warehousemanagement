package com.warehouse.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


//@JsonAutoDetect(getterVisibility = Visibility.NONE, fieldVisibility = Visibility.ANY)
//@JsonFilter(value="tag")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class Box {
	private String name;
	private String description;
	@DBRef
	private QrCode qrCode;
	@DBRef
	private UserImage userImage;

	@DBRef
	private List<Item> items;
		
	private String ownerId;
	
	@Indexed
	private boolean status;
	
	@Id
	private String id;		
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public QrCode getQrCode() {
		return qrCode;
	}

	public void setQrCode(QrCode qrCode) {
		this.qrCode = qrCode;
	}
	
	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}



	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public UserImage getUserImage() {
		return userImage;
	}

	public void setUserImage(UserImage userImage) {
		this.userImage = userImage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Box(){}
	
	private Box(Box.Builder builder) {
		this.name = builder.name;
		this.description = builder.description;
		this.qrCode = builder.qrCode;
		this.userImage = builder.userImage;		
		this.items = builder.items;
		this.ownerId = builder.ownerId;
		this.status = builder.status;
		this.id = builder.id;
		
		
//		if(this.salt == null) {
//			this.salt = BCrypt.gensalt(14);
//		}
//		
//		if(this.password == null) {
//			this.password = BCrypt.hashpw(this.password, this.salt);
//		}
	}
	
	public static class Builder {
		private String name;
		private String description;
		private QrCode qrCode;
		private UserImage userImage;	
		private List<Item> items;
		private String ownerId;
		private boolean status;
		private String id;
		
		
		public Builder() {			
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder description(String description) {
			this.description = description;
			return this;
		}
		
		public Builder qrCode(QrCode qrCode) {
			this.qrCode = qrCode;
			return this;
		}
		
		public Builder userImages(UserImage userImage) {
			this.userImage = userImage;
			return this;
		}				
		
		public Builder items(List<Item> items) {
			this.items = items;
			return this;
		}
		
		public Builder ownerId(String ownerId){
			this.ownerId = ownerId;
			return this;
		}
		
		public Builder status(boolean status) {
			this.status = status;
			return this;
		}
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
		
		public Box build() {
			return new Box(this);
		}
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Box other = (Box) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}

