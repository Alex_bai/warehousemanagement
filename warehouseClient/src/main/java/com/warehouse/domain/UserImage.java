package com.warehouse.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

//@JsonAutoDetect(getterVisibility = Visibility.NONE, fieldVisibility = Visibility.ANY)
//@JsonFilter(value="tag")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class UserImage {
	
	private String boxId;
	private String name;
	private String url;	

	private List<AreaAndContext> aacs;
	
	@Id
	private String id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<AreaAndContext> getAacs() {
		return aacs;
	}

	public void setAacs(List<AreaAndContext> aacs) {
		this.aacs = aacs;
	}

	public String getBoxId() {
		return boxId;
	}

	public void setBoxId(String boxId) {
		this.boxId = boxId;
	}

	public UserImage(){}
	
	private UserImage(UserImage.Builder builder){
		this.boxId = builder.boxId;
		this.name = builder.name;
		this.url = builder.url;
		//this.shapeContext = builder.shapeContext;		
		this.id = builder.id;
		this.aacs = builder.aacs;
	}
	
	public static class Builder{
		private String boxId;
		private String name;
		private String url;
		private String id;		
		private List<AreaAndContext> aacs;
	
	
	public Builder(){
		
	}
	
	public Builder boxId(String boxId)
	{
		this.boxId = boxId;
		return this;
	}
	
	public Builder name(String name){
		this.name = name;
		return this;
	}
	
	public Builder url(String url){
		this.url = url;
		return this;
	}
	
	public Builder aacs(List<AreaAndContext> aacs)
	{
		this.aacs = aacs;
		return this;
	}
	
	public Builder id(String id){
		this.id = id;
		return this;
	}
	
	public UserImage build(){
		return new UserImage(this);
	}
  }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserImage other = (UserImage) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
