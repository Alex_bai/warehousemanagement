package com.warehouse.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document
public class QrCode {
	private String name;
	private String url;

	@Id
	private String id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public QrCode(){}
	
	private QrCode(QrCode.Builder builder){
		this.name = builder.name;
		this.url = builder.url;
		this.id = builder.id;
	}
	
	public static class Builder{
		private String name;
		private String url;
		private String id;
	
	
	public Builder(){
		
	}
	
	public Builder name(String name){
		this.name = name;
		return this;
	}
	
	public Builder url(String url){
		this.url = url;
		return this;
	}
	

	public Builder id(String id){
		this.id = id;
		return this;
	}
	
	public QrCode build(){
		return new QrCode(this);
	}
	
  }
}
