package com.warehouse;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.WebApplicationInitializer;

@Configuration
@ComponentScan(basePackages={"com.warehouse.controllers", "com.warehouse.filters", "com.warehouse.security", "com.warehouse.services"}) 
@EnableAutoConfiguration
@Import({MongoFactoryConfig.class, SecurityConfiguration.class})
public class WarehouseApplication extends SpringBootServletInitializer implements WebApplicationInitializer {
	
    public static void main(String[] args) {
        SpringApplication.run(WarehouseApplication.class, args);
    }
    
            
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {    	
        return application.sources(applicationClass);
    }
        
     
    @Override
    public void onStartup(ServletContext sc) throws ServletException {
    	super.onStartup(sc);
    }
        
    private static Class<WarehouseApplication> applicationClass = WarehouseApplication.class;   
}
