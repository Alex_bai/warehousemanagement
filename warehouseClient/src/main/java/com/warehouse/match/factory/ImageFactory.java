package com.warehouse.match.factory;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

import com.warehouse.domain.AreaAndContext;
import com.warehouse.domain.Image;
import com.warehouse.domain.UserImage;
import com.warehouse.match.domain.ConnectedComponent;
import com.warehouse.match.domain.ShapeContext;
import com.warehouse.match.ops.PreProcessOp;


public class ImageFactory {
		
	public int circles = 5; 
	public int bins = 12;
	public double alpha = 1;
	public double sigma = 0.73;		
	public int medianFilterWidth = 3;
	public int medianFilterHeight = 3;		
	//the maximum value for difference between two shapes
	public int maxDiff = circles*bins*255;
	//the number of comparing shape context
	public final int shapeContextCount = 3;
	private final double scale = 0.8;
				
	/**
	 * write image to local file
	 * @param file
	 * @param path
	 * @throws IOException
	 */
	public void writeImageToLocal(MultipartFile file, String path) throws IOException
	{			
		 byte[] bytes = file.getBytes();
		 InputStream in = new ByteArrayInputStream(bytes);
		 BufferedImage bImageFromConvert = ImageIO.read(in);
		 BufferedImage dest = this.rescaleImage(bImageFromConvert);
		 ImageIO.write(dest, "png", new File(path));		 
//         File imageFile = new File(path);
//                  
//         BufferedOutputStream stream = 
//                 new BufferedOutputStream(new FileOutputStream(imageFile));
//         stream.write(bytes);                                                                
//         stream.close();          
	}
	
	/**
	 * get area and context
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public List<AreaAndContext> getAacs(MultipartFile file) throws IOException {		
		byte[] bytes = file.getBytes();
		InputStream is = new ByteArrayInputStream(bytes);
		BufferedImage src = ImageIO.read(is);		
				
		//generate shape context
		List<ShapeContext> contexts = generateShapeContext(src);
		
		//get area and context
		List<AreaAndContext> aacs = new ArrayList<AreaAndContext>();
		for(ShapeContext shapeContext : contexts)
		{
			float area = shapeContext.getConnectedComponent().getArea();
			int context[] = shapeContext.getContext();
			AreaAndContext aac = new AreaAndContext.Builder()
					.area(area)
					.context(context)
					.build();
					
			aacs.add(aac);
		}		
		return aacs;		
	}
	
	/**
	 * generate shape context of the image
	 * @param src
	 * @return
	 */
	public List<ShapeContext> generateShapeContext(BufferedImage temp)
	{
		//rescale image
		BufferedImage src = rescaleImage(temp);
		//do gaussian blur and median filter
		PreProcessOp preProcessOp = new PreProcessOp(alpha, sigma, medianFilterWidth, medianFilterHeight);		
		BufferedImage dest = preProcessOp.filter(src, null);		
		//get connected component
		List<ConnectedComponent> components = new ArrayList<ConnectedComponent>(
				ConnectedComponentFactory.getComponents(dest));
		//sort the components
		Collections.sort(components);
		List<ShapeContext> shapeContexts = new ArrayList<ShapeContext>();				
		if(components.size() > 1){
			//remove the background 
			components.remove(0);								
			//generate shape context
			shapeContexts = ShapeContextFactory.getShapeContext(components, circles, bins);
		}				
		return shapeContexts;
	}
	
	public BufferedImage rescaleImage(BufferedImage src){
		
		int newHeight = (int) (src.getHeight()*(1-scale));
		int newWidth = (int) (src.getWidth()*(1-scale));
		BufferedImage dest = new BufferedImage(newWidth, newHeight, src.getType());
		for(int row=0;row<newHeight;row++){
			for(int col=0;col<newWidth;col++){						
				int newRow = (int) (row/(1-scale));					
				int newCol = (int) (col/(1-scale));	
				int rgb = src.getRGB(newCol, newRow);					
				dest.setRGB(col, row, rgb);
			}
		}
		return dest;
	}
	
	/**
	 * analyze image information, match with images in the database,
	 * and return most similar image URL
	 * @param image
	 * @return
	 * @throws IOException 
	 */
	public String matchImage(MultipartFile file, List<UserImage> images) throws IOException {
		// TODO Auto-generated method stub		
		String resultUrl = null;
		
		byte[] bytes = file.getBytes();
		InputStream is = new ByteArrayInputStream(bytes);
		BufferedImage src = ImageIO.read(is);
		
		//generate shape context
		List<ShapeContext> contexts = generateShapeContext(src);	
		
		//calculate difference between source photo and images in the database
		Map<String, Integer> diffData = new HashMap<String, Integer>();
		for(UserImage image : images)
		{
			int diff = calculateDifference(contexts, image);
			diffData.put(image.getUrl(), diff);
		}
		
		List<Map.Entry<String, Integer>> result = 
				new ArrayList<Map.Entry<String, Integer>>(diffData.entrySet());
		
		//sort the <url, diffVal> based on the diffVal
		Collections.sort(result, new Comparator<Map.Entry<String, Integer>>(){
			@Override
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				// TODO Auto-generated method stub
				return o1.getValue() - o2.getValue();
			}			
		});
		
		//get the most similar image's url
		resultUrl = result.get(0).getKey();		
		
		return resultUrl;
	}
	
	public List<String> matchAndroidImage(MultipartFile file, List<UserImage> images) throws IOException {
		// TODO Auto-generated method stub		
		List<String> resultUrls = new ArrayList<String>();
		
		byte[] bytes = file.getBytes();
		InputStream is = new ByteArrayInputStream(bytes);
		BufferedImage src = ImageIO.read(is);
		
		//generate shape context
		List<ShapeContext> contexts = generateShapeContext(src);	
		
//		if(!contexts.isEmpty() && contexts!=null){
//			
//		}	
		//calculate difference between source photo and images in the database
		Map<String, Integer> diffData = new HashMap<String, Integer>();
		for(UserImage image : images)
		{
			int diff = calculateDifference(contexts, image);
			diffData.put(image.getUrl(), diff);
		}
		
		List<Map.Entry<String, Integer>> result = 
				new ArrayList<Map.Entry<String, Integer>>(diffData.entrySet());
		
		//sort the <url, diffVal> based on the diffVal
		Collections.sort(result, new Comparator<Map.Entry<String, Integer>>(){
			@Override
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				// TODO Auto-generated method stub
				return o1.getValue() - o2.getValue();
			}			
		});
		
		//get the most similar image's url
		//resultUrl = result.get(0).getKey();	
		int maxSize = Math.min(5, result.size());		
		List<Map.Entry<String, Integer>> infor = result.subList(0, maxSize);
		for(int i=0;i<infor.size();i++){
			resultUrls.add(infor.get(i).getKey());
		}
				
		return resultUrls;
	}
	
	/**
	 * calculate difference between initial image and images in the database
	 * @param initial
	 * @param image
	 * @return
	 */
	public int calculateDifference(List<ShapeContext> initial, UserImage image)
	{	
		int difference = 0;
		
		//get area and context of the image		
		List<AreaAndContext> tacs = image.getAacs();
		
		//if the number of initial image's shapeContext is less than 
		//destination image's shapeContext, then it is not the same
		if(initial.size() < tacs.size())
		{
			return maxDiff;
		}
		//get the number of minimum shapeContext
		int count = Math.min(initial.size(), shapeContextCount);
		
		for(int i=0;i<count;i++)
		{			
			ShapeContext context = initial.get(i);
			
			//get the similar shapeContext based on the area size
			Map<Integer, AreaAndContext> similar = getSimilarArea(context, image);
			int diff = 0;
			
			//if there is no similar area, then they are not the same
			if(similar.isEmpty() || similar == null)
			{
				diff = maxDiff;
			}else{		
				//if the similar areas contains corresponding shapeContext,
				//add the difference
				if(i < tacs.size() && similar.keySet().contains(i))				
				{					
					diff = context.getDifference(tacs.get(i).getContext());
				}else{
					diff = maxDiff;
				}			
			}
			difference += diff;
		}	
		return difference/(initial.size());
	}
	
	/**
	 * get similar shapes based on the area
	 * @param context
	 * @param image
	 * @return
	 */
	public Map<Integer, AreaAndContext> getSimilarArea(ShapeContext context, UserImage image)
	{
		List<AreaAndContext> l2 = image.getAacs();
		Map<Integer, AreaAndContext> result = new HashMap<Integer, AreaAndContext>();
		
		for(int i=0;i<l2.size();i++)
		{
			AreaAndContext aac = l2.get(i);
			if(aac.isAreaSimilar(context))
			{
				result.put(i, aac);
			}
		}
		return result;
	}	
}
