package com.warehouse.match.factory;

import java.util.ArrayList;
import java.util.List;

import com.warehouse.match.domain.ConnectedComponent;
import com.warehouse.match.domain.ShapeContext;

public class ShapeContextFactory {

	private static final float areaThreshold = 0.05f;
	
	/**
	 * get shape context based on the components
	 * @param components
	 * @return
	 */
	public static List<ShapeContext> getShapeContext(List<ConnectedComponent> components, 
			int circles, int bins)
	{
		//calculate component area
		calculateComponentArea(components);
		
		//calculate component's edge
		ConnectedComponentFactory.getEdge(components);
		
		//calculate shape context for each component
		List<ShapeContext> contexts = new ArrayList<ShapeContext>();
		for(ConnectedComponent component : components)
		{				
			ShapeContext sc = new ShapeContext(circles, bins, component);
			contexts.add(sc);
		}			
		
		return contexts;
	}
	
	
	/**
	 * calculate area for each component
	 * @param list_comps
	 */
	public static void calculateComponentArea(List<ConnectedComponent> list_comps)
	{	
		ConnectedComponent largest = list_comps.get(0);		
		
		int maxArea = largest.getPoints().size();
		largest.setArea(1);		
		
		for(int i=1;i<list_comps.size();i++)
		{
			ConnectedComponent cc = list_comps.get(i);
			int area = cc.getPoints().size();
			float value = (float)area/maxArea;
			if(value > areaThreshold)
			{
				cc.setArea(value);
			}			
		}
	}
}
