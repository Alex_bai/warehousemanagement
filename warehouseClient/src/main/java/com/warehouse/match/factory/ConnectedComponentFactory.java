package com.warehouse.match.factory;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.warehouse.match.domain.ConnectedComponent;
import com.warehouse.match.domain.Point;

import pixeljelly.scanners.Location;
import pixeljelly.scanners.RasterScanner;

public class ConnectedComponentFactory {
		
	//if the difference between two colors is less than colorThreshold,
	//they are the same color
	private static final int colorThreshold = 10;
	//if the number of points is less than eliminatedThreshold,
	//delete it
	private static final int eliminatedThreshold = 100;
	private static int parent[];	
	private static int imageWidth;
	private static int imageHeight;
	
	/**
	 * generate the components of the image and eliminate the components
	 * @param src
	 * @return
	 */
	public static Collection<ConnectedComponent> getComponents(BufferedImage src)
	{
		imageWidth = src.getWidth();
		imageHeight = src.getHeight();		
		parent = new int[imageHeight*imageWidth];
		for(int i=0;i<parent.length;i++)
		{
			parent[i] = i;
		}		
		//get component
		getEightConnected(src);		
		Map<Integer, ConnectedComponent> comps = new HashMap<Integer, ConnectedComponent>();
		for(int i=0;i<parent.length;i++)
		{
			int label = parent[i];
			if(!comps.containsKey(label))
			{
				comps.put(label, new ConnectedComponent());
			}			
			//get the corresponding point
			Point point = new Point(i-(i/imageWidth)*imageWidth,i/imageWidth,label);			
			//add point to the component
			comps.get(label).getPoints().add(point);
		}		
		List<ConnectedComponent> ccs = new ArrayList<ConnectedComponent>(comps.values());
		return eliminatedMinComponents(ccs);				
	}
	
	/**
	 * get one dimensional index from two dimensional index
	 * @param height
	 * @param width
	 * @param col
	 * @param row
	 * @return
	 */
	private static int rasterIndex(int height, int width, int col, int row)
	{
		int index = row*width+col;
		index = index<0 ? 0 : (index>=height*width ? height*width-1 : index);
		return index;
	}
	
	/**
	 * generate the image based on the component
	 * @param comps
	 * @param dest
	 * @return
	 */
	public BufferedImage toImage(List<ConnectedComponent> comps, BufferedImage dest)
	{		
		for(ConnectedComponent c : comps)
		{
			Color color = new Color((int)(Math.random()*256), (int)(Math.random()*256), (int)(Math.random()*256));
			for(Point point : c.getPoints())
			{			
				dest.setRGB(point.col, point.row, color.getRGB());
			}
		}
		return dest;
	}
	
	/**
	 * get eight-connected component
	 * @param src
	 */
	private static void getEightConnected(BufferedImage src)
	{
		int offsets[][] = {{-1, -1}, {0, -1}, {1, -1}, {-1, 0}};		
		for(Location loc : new RasterScanner(src, false))
		{
			int rgb = src.getRGB(loc.col, loc.row);
			for(int i=0;i<offsets.length;i++)
			{				
				int col = loc.col+offsets[i][0];
				int row = loc.row+offsets[i][1];
				int temp_rgb = getRGB(src, col, row);
				if(isSimilarColor(temp_rgb, rgb))
				{
					union(rasterIndex(imageHeight, imageWidth, col, row), 
							(rasterIndex(imageHeight, imageWidth, loc.col, loc.row)));
				}
			}
		}
			
		for(Location loc : new RasterScanner(src, false))
		{
			find(rasterIndex(imageHeight, imageWidth, loc.col, loc.row));
		}			
	}
	
	private static int find(int index)
	{
		if(parent[index] == -1 || parent[index]== index)
		{
			return parent[index];
		}else{
			parent[index] = find(parent[index]);
			return parent[index];
		}
	}
	
	private static void union(int p1, int p2)
	{
		int root1 = find(p1);
		int root2 = find(p2);
		parent[root2] = root1;
	}
	
	/**
	 * get RGB values 
	 * @param src
	 * @param col
	 * @param row
	 * @return
	 */
	private static int getRGB(BufferedImage src, int col, int row)
	{	
		col = col<0 ? col+imageWidth : (col>=imageWidth ? col-imageWidth : col);
		row = row<0 ? row+imageHeight : (row>=imageHeight ? row-imageHeight : row);		
		int rgb = src.getRGB(col, row);
		
		return rgb;
	}
	
	/**
	 * judge if the two point has the similar color
	 * @param startPoint
	 * @param point
	 * @return
	 */
	public static boolean isSimilarColor(int startRGB, int rgb)
	{		
		Color startColor = new Color(startRGB);
		int start_red = startColor.getRed();
		int start_green = startColor.getGreen();
		int start_blue = startColor.getBlue();
			
		Color color = new Color(rgb);
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		
		int dis_red = start_red - red;
		int dis_green = start_green - green;
		int dis_blue = start_blue - blue;
			
		double result = Math.sqrt(Math.pow(dis_red, 2)+Math.pow(dis_green, 2)+Math.pow(dis_blue, 2));
			
		return result < colorThreshold;		
	}
	
	/**
	 * get the edge of image
	 * @param comps
	 * @return		//should return shape context
	 */
	public static void getEdge(List<ConnectedComponent> comps)
	{											
		//[col, row]		
		int offsets[][] = {{0, -1},{-1, 0},{0, 1},{1, 0}};
		for(ConnectedComponent cc : comps)
		{							
			Set<Point> points = new HashSet<Point>(cc.getPoints());					
			//the edge points
			List<Point> edgePoints = new ArrayList<Point>();
			for(Point point : points)
			{
				boolean isEdge = false;
				for(int i=0;i<offsets.length;i++)
				{
					int col = point.col+offsets[i][0];
					int row = point.row+offsets[i][1];
					if(!points.contains(new Point(col,row)))
					{
						isEdge = true;
						break;
					}
				}
				if(isEdge)
				{
					edgePoints.add(point);					
				}
			}
			cc.setEdgePoints(edgePoints);
		}
	}
	
	/**
	 * eliminate components which are less than eliminatedThreshold
	 * @param comps
	 * @return
	 */
	public static List<ConnectedComponent> eliminatedMinComponents(List<ConnectedComponent> comps)
	{
		List<ConnectedComponent> edges = new ArrayList<ConnectedComponent>();	
		
		for(ConnectedComponent cc : comps)
		{			
			if(cc.getPoints().size() >= eliminatedThreshold)
			{
				edges.add(cc);
			}
		}
		
		return edges;		
	}
}