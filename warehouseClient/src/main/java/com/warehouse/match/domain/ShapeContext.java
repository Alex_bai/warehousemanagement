package com.warehouse.match.domain;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShapeContext{

	//the total circles for each component
	public int circles;
	//the total bins for each circle
	public int bins;			
	//component
	public ConnectedComponent connectedComponent;	
	//shape context data
	public int[] context;
	
	public ShapeContext(){}
	
	public ShapeContext(int circles, int bins, ConnectedComponent connectedComponent) {		
		this.circles = circles;
		this.bins = bins;
		this.connectedComponent = connectedComponent;	
		this.context = new int[circles*bins];
		
		//generate shapeContext
		generateShapeContext();
	}		
	
	/**
	 * get the shape context of each shape
	 */
	public void generateShapeContext()
	{		
		//get all the points in this shape(only the edge)
		List<Point> points = connectedComponent.getEdgePoints();
		
		//get the value of radius
		int radius[] = new int[circles];
		radius[radius.length-1] = getBiggestRadius();		
		for(int i=radius.length-2;i>=0;i--)
		{
			radius[i] = radius[i+1]/2;
		}
		//get the value of angle
		double angle[] = new double[bins];
		for(int i=0;i<angle.length;i++)
		{
			angle[i] = (2*Math.PI)*((double)(i+1)/bins);
		}						
		
		for(Point point : points)
		{						
			for(Point p : points)
			{
				if(!point.equals(p))
				{
					int num = pointInBin(point, p, radius, angle);
					if(num != -1)
					{
						context[num]++;
					}					
				}				
			}				
		}
		//normalize the data in the context, from 0 to 255
		normalize();	
	}
	
	/**
	 * judge which bin the point belongs to
	 * @param original
	 * @param point
	 * @return
	 */
	public int pointInBin(Point original, Point point, int radius[], double angle[])
	{
		int col_distance = point.col-original.col;
		int row_distance = point.row-original.row;		
		double distance = Math.sqrt(Math.pow(col_distance, 2) + Math.pow(row_distance, 2));
		int binIndex = -1;
		
		//if the point is in the circles
		if(distance<radius[radius.length-1])
		{
			//check which circle the point in			
			for(int i=0;i<radius.length;i++)
			{
				if(distance < radius[i])
				{
					binIndex = i*bins;
					break;
				}
			}				
			double theta = Math.atan2(row_distance, col_distance);
			theta = theta<0 ? -theta : Math.PI*2-theta;
			
			//check which bin the point in
			for(int i=0;i<angle.length;i++)
			{
				if(theta <= angle[i])
				{
					binIndex += i;
					break;
				}
			}					
		}
		return binIndex;		
	}
	
	/**
	 * get biggest radius based on the minimum row, maximum row, 
	 * minimum column and maximum column
	 * @return
	 */
	public int getBiggestRadius()
	{		
		List<Point> points = connectedComponent.getPoints();

		//get minimum row and maximum row
		Collections.sort(points, new Comparator<Point>(){
			@Override
			public int compare(Point o1, Point o2) {							
				return o1.row-o2.row;
			}									
		});
		Point minRowPoint = points.get(0);
		Point maxRowPoint = points.get(points.size()-1);
		int height = maxRowPoint.row - minRowPoint.row+1;

		//get minimum column and maximum column
		Collections.sort(points, new Comparator<Point>(){

			@Override
			public int compare(Point o1, Point o2) {
				return o1.col-o2.col;
			}
			
		});
		Point minColPoint = points.get(0);
		Point maxColPoint = points.get(points.size()-1);
		int width = maxColPoint.col - minColPoint.col+1;
		
		int biggestRadius = (int) Math.ceil(Math.sqrt((height*height+width*width)));
		
		return biggestRadius;
	}
	
	/**
	 * normalize the data in the context
	 */
	public void normalize()
	{
		int colorValue[] = new int[context.length];
		colorValue = context.clone();
		Arrays.sort(colorValue);
		
		//get max value in the data array
		int maxValue = colorValue[context.length-1];
		
		for(int i=0;i<context.length;i++)
		{
			context[i] = (int)(((double)context[i]/maxValue)*255);			
		}				
	}
	
	/**
	 * get different between the two shapeContexts
	 * @param shape
	 */
	public int getDifference(int[] shape)
	{				
		double result = 0;
		for(int i=0;i<context.length;i++)
		{			
			int distance = context[i]-shape[i];
			
			double molecule = distance*distance;			
			double denominator = context[i]+shape[i];
			
			if(denominator != 0)
			{				
				result += molecule/denominator;
			}				
		}
		return (int) (result/2);
	}
	
	public ConnectedComponent getConnectedComponent() {
		return connectedComponent;
	}

	public void setConnectedComponent(ConnectedComponent connectedComponent) {
		this.connectedComponent = connectedComponent;
	}

	public int[] getContext() {
		return context;
	}

	public int getCircles() {
		return circles;
	}
	
	public void setCircles(int circles) {
		this.circles = circles;
	}
	
	public int getBins() {
		return bins;
	}
	
	public void setBins(int bins) {
		this.bins = bins;
	}

	public void setContext(int[] context) {
		this.context = context;
	}
}
