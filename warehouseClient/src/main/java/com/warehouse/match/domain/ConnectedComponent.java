package com.warehouse.match.domain;
import java.util.ArrayList;
import java.util.List;

public class ConnectedComponent implements Comparable<ConnectedComponent>{

	//the label of this component
	private int label;	
	//all the points in the component
	private List<Point> points;	
	//edge points
	private List<Point> edgePoints;
	//component area
	private float area;	
	
	public ConnectedComponent(){
		points = new ArrayList<Point>();
	}
	
	public ConnectedComponent(int label, List<Point> points) {
		this.label = label;
		this.points = points;
	}

	public float getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public List<Point> getEdgePoints() {
		return edgePoints;
	}

	public void setEdgePoints(List<Point> edgePoints) {
		this.edgePoints = edgePoints;
	}

	@Override
	public int compareTo(ConnectedComponent o) {
		// TODO Auto-generated method stub
		return o.points.size()- this.points.size();
	}
}
