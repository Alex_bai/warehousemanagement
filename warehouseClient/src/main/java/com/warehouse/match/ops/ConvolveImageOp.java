package com.warehouse.match.ops;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import pixeljelly.ops.ConvolutionOp;
import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;
import pixeljelly.utilities.SeperableKernel;

public class ConvolveImageOp extends NullOp implements PluggableImageOp{

	private float[] kernelVal;		
	private int width;
	private int height;
	
	public ConvolveImageOp(float kernelVal[])
	{		
		this.kernelVal = kernelVal;		
	}
	
	/**
	 * convolve image
	 * @param src
	 * @param dest
	 * @return
	 */
	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{	
		width = src.getWidth();
		height = src.getHeight();
		
		if(dest == null)
		{
			dest = new BufferedImage(width, height, src.getType());
		}
		//get separable kernel
		SeperableKernel nsk = new SeperableKernel(kernelVal, kernelVal);
		//convolve image with separable kernel
		ConvolutionOp op = new ConvolutionOp(nsk, false);		
		return op.filter(src, dest);
	}

	@Override
	public String getAuthorName() {
		// TODO Auto-generated method stub
		return "Chenguang Bai";
	}

	@Override
	public BufferedImageOp getDefault(BufferedImage arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
