userServices = angular.module('userServices',['ngResource','UserValidation','tqq.ui']);

angular.module('UserValidation', []).directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.form.password.$viewValue
                ctrl.$setValidity('noMatch', !noMatch)
            })
        }
    }
})

userServices.factory('User',['$resource',function($resource){
	return $resource('api/users/:userId',{},{
		update:{
			method:'PUT'
		}
	});
}])

userServices.factory('logout',['$resource',function($resource){
	return $resource('api/logout')
}]);

userServices.controller('userController', ['$scope', '$routeParams', 'User', 'AuthService','$location','logout',
                                            '$route', function ($scope, $routeParams, User, AuthService,$location, $route,logout) {
	if(sessionStorage.getItem("userId")==null){
		$location.path('/');		
	}
	
	$scope.userId = $routeParams.userId;
	$scope.user = AuthService.getUser();
	$scope.username = $scope.user.userName;
	
	$scope.user = User.get({ userId:$scope.userId});
	
	
	$scope.updateProfile = function(){
	User.update({userId:$scope.userId},$scope.user,function(response){
		if(response == null){
			alert("please check your password");
		}else{
			$location.path('/users/'+$scope.userId+'/boxes');
		}
		});
	}
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
}]);


userServices.controller('userRegisterController',['$scope','$routeParams','$location','User',
                                                  function($scope,$routeParams,$location,User){
	$scope.register = function(){
		User.save($scope.user, function(response){
			if(response == null){
				alert("username and password not appropriate");
			}else{
				$location.path('/');
			}
		    });
	    };
}])


userServices.factory('Boxes',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateBoxInfo',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateBoxImage',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/image',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.factory('BoxStatus',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/status',{},{
		update:{
			method:'PUT'
		}
	})
}])

userServices.controller('boxesController',['$scope','$routeParams','AuthService','$route','$location','Boxes','UserBoxNum','BoxStatus','$http','$rootScope','loading','logout',
                                          function($scope,$routeParams,AuthService,$route,$location,Boxes,UserBoxNum,BoxStatus,$http,$rootScope,loading,logout){
	if(sessionStorage.getItem("userId")==null){
		$location.path('/');		
	}
	
	$scope.userId = $routeParams.userId;
  	$scope.boxId = $routeParams.boxId;
  	$scope.user = AuthService.getUser();
	$scope.username = $scope.user.userName;
  	
	$scope.currentPage = 1;
	$scope.userBoxNum = 0;
	$scope.search = "";
	
    $scope.boxes = Boxes.query({userId:$scope.userId, pageNum:$scope.currentPage });
	
  	
  	UserBoxNum.get({userId:$scope.userId,search:$scope.search},function(response){
  		$scope.userBoxNum  = response.userBoxNum;
  		console.log("user box num",$scope.userBoxNum);
  	});
	
	$scope.gotoPage = function() {
		$scope.boxes = Boxes.query( { userId: $scope.userId, pageNum : $scope.currentPage });
	}
  	
	$scope.deleteBox = function(boxId){
		Boxes.remove({
			userId:$scope.userId,
			boxId:boxId,
			pageNum: $scope.currentPage
		},function(response){
			if(response != null){
				$scope.boxes = response.userBoxes;
			}else{
				alert("delete failed");
			}
		});
	};
	
	$scope.updateBoxStatus = function(boxId){BoxStatus.update({userId:$scope.userId,boxId:boxId},$scope.box)};
	
	
	$scope.filesChanged = function(elm){
		$scope.showInfo = false;
 		$scope.files = elm.files;
 		$scope.$apply();
 	}        
 	
 	$scope.uploadFile = function() {
 		loading.show();
 		if($scope.files == null){
 			$scope.showInfo = true;
 			$scope.Info = "please choose file";
 		}
 		if($scope.files != null){
 			var fd = new FormData();     
	 		angular.forEach($scope.files, function(file){                 			
				fd.append('file', file);                   			
			});
	 		var uploadUrl = 'api/users/'+$scope.userId+'/images';
	 		
			$http.post(uploadUrl, fd, {
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				}
			}).success(function(response) {
				if(response != null){
					if(!$scope.disabled){
						$location.path('/users/'+$scope.userId+'/boxes/'+response.id+'/items');
					}
				}
				loading.hide();
			}).error(function(response) {
				
			});
 		}
 	};	
 	
 	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
}]);


userServices.factory('ChooseImage',['$resource',function($resource){
	return $resource('api/users/:userId/images/:imageId')
}]);

userServices.factory('ImageNumCanChoose',['$resource',function($resource){
	return $resource('api/users/:userId/imageNumCanChoose')
}]);

userServices.controller('createBoxController',['$scope','$routeParams','AuthService','$route','$location','Boxes','ChooseImage','ImageNumCanChoose','logout',
                                               function($scope,$routeParams,AuthService,$route,$location,Boxes,ChooseImage,ImageNumCanChoose,logout){
	if(sessionStorage.getItem("userId")==null){
		$location.path('/');		
	}
	
	$scope.userId = $routeParams.userId;
	$scope.boxId = $routeParams.boxId;
	$scope.user = AuthService.getUser();
	$scope.username = $scope.user.userName;
	
	$scope.imageNumCanChoose = 0;
	$scope.currentPage = 1;
	
	$scope.images = ChooseImage.query( { userId:$scope.userId,pageNum:$scope.currentPage });
	
	ImageNumCanChoose.get({userId:$scope.userId},function(response){
		$scope.imageNumCanChoose = response.imageNumCanChoose;
	});
	
	$scope.gotoPage = function() {
		$scope.images = ChooseImage.query( {userId:$scope.userId, pageNum : $scope.currentPage });
	}
	
	$scope.addBox = function(){	
		if($scope.box.userImage == null){
			$scope.noImage = 'Image must be choose';
		}else{
			$scope.noImage = '';
			Boxes.save({
				userId: $scope.userId
				}, $scope.box, function(response){
					if(response != null){
						$scope.box = response;
						$location.path('/users/'+$scope.userId+'/boxes/'+$scope.box.id+'/items');
					}else{
						alert("create box failed");
					}
			});
		}			
	};
	
	$scope.logout = function(){		
		logout.save();
		sessionStorage.clear();
		$location.path('/');	
	}
}])



userServices.controller('editBoxAndItemController',['$scope','$routeParams','AuthService','$route','$location','$uibModal','$log','Items','BoxItemNum','Boxes','ChooseImage','updateBoxInfo','ImageNumCanChoose','updateBoxImage','logout',
                                                function($scope,$routeParams,AuthService,$route,$location,$uibModal,$log,Items,BoxItemNum,Boxes,ChooseImage,updateBoxInfo,ImageNumCanChoose,updateBoxImage,logout){
	
			if(sessionStorage.getItem("userId")==null){
				$location.path('/');		
			}
	
		  	$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.user = AuthService.getUser();
			$scope.username = $scope.user.userName;
		  	
			$scope.currentPage = 1;	
			$scope.boxItemNum = 0;
			$scope.imageNumCanChoose = 0;
			var initialBoxName;
			var initialBoxDescription;
			
			$scope.animationEnabled = true;
			
			$scope.hasUserImage = true;
			
		    Boxes.get({userId:$scope.userId, boxId:$scope.boxId},function(response){
		    	$scope.box = response;
		    	if($scope.box.userImage != null){
		    		$scope.imageAddress="api/users/"+$scope.userId+"/userImage/"+$scope.box.userImage.id;
		    	}
		    	initialBoxName = $scope.box.name;
				initialBoxDescription = $scope.box.description;
		    });
		
			$scope.images = ChooseImage.query( {userId:$scope.userId,pageNum:$scope.currentPage });
			
			$scope.items = Items.query( { userId:$scope.userId, boxId:$scope.boxId, pageNum:$scope.currentPage });									
			
			
			BoxItemNum.get({userId:$scope.userId,boxId:$scope.boxId},function(response){
				$scope.boxItemNum = response.boxItemNum;
			});
			
			$scope.gotoPage = function() {
				$scope.items = Items.query( {userId:$scope.userId, boxId:$scope.boxId, pageNum : $scope.currentPage });
			}
			
			$scope.gotoPageImage = function() {
				$scope.images = ChooseImage.query( {userId:$scope.userId, pageNum : $scope.currentPage });
			}
			
			$scope.updateBoxInfo = function(){
				updateBoxInfo.update({
					userId:$scope.userId,
					boxId:$scope.boxId,
				},$scope.box)
			};
			
			
			$scope.changeName = function(){
				$scope.updateBoxInfo();
				initialBoxName = $scope.box.name;
				$scope.change = false;
			}
			$scope.cancelName = function(){			
				$scope.box.name = initialBoxName;
				$scope.change = false;
			}
			
			$scope.changeDescription = function(){
				$scope.updateBoxInfo();
				initialBoxDescription = $scope.box.description;
				$scope.changeDes = false;
			}
			
			$scope.cancelDescription = function(){
				$scope.box.description = initialBoxDescription;
				$scope.changeDes = false;
			}
			
			$scope.selectedImage = function(image){
				$scope.boxImage = image;
			}
			
			$scope.updateImage = function(){
				$scope.box.userImage = $scope.boxImage;
				updateBoxImage.update({
					userId:$scope.userId,
					boxId:$scope.boxId
				},$scope.box, function(response){
					if(response != null){
						$scope.box = response;
						$scope.imageAddress="api/users/"+$scope.userId+"/userImage/"+$scope.box.userImage.id;
						$('#myModal').modal('hide')
					}
				});
			}
			
			$scope.tanModal = function(){
				$('#myModal').modal();
				$scope.images = ChooseImage.query( {userId:$scope.userId,pageNum:$scope.currentPage });
				ImageNumCanChoose.get({userId:$scope.userId},function(response){
					$scope.imageNumCanChoose = response.imageNumCanChoose;
				});
			}
		
			
			$scope.createItem = function(){
				if($scope.box.status == false){
					$location.path('/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/newItem');
				}else{
					$scope.infom="Box is full, no more items can be added";
				}
			}
			
			$scope.deleteItem = function(itemId){
				Items.remove({
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:itemId,
					pageNum: $scope.currentPage
				},function(response){
					if(response != null){
						$scope.items = response.userItems;
					}else{
						alert("delete failed");
					}
				});
			};		
			
			$scope.logout = function(){		
				logout.save();
				sessionStorage.clear();
				$location.path('/');	
			}
}]);



userServices.factory('BoxItemNum',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/BoxItemNum');
}]);

userServices.factory('Items',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('updateItemInfo',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId/info',{},{
		update:{
			method:'PUT'
		}
	})
}]);

userServices.factory('Photos',['$resource',function($resource){
	return $resource('api/users/:userId/boxes/:boxId/items/:itemId/photos/:photoId')
}]);

userServices.controller('createItemController', [ '$scope','$routeParams','AuthService','$route','$http','$location','logout',
                                           		function($scope, $routeParams,AuthService, $route,$http,$location,logout) {
                    	
						if(sessionStorage.getItem("userId")==null){
							$location.path('/');		
						}
	
						$scope.userId = $routeParams.userId;
                    	$scope.boxId = $routeParams.boxId;
                    	$scope.user = AuthService.getUser();
                    	$scope.username = $scope.user.userName;
                    	
                    	$scope.filesChanged = function(elm){
                    		$scope.files = elm.files;
                    		$scope.$apply();
                    	}        
                    	
                    	$scope.addItem = function() {
                    		
                    		var fd = new FormData();                    		                    		
                    		
                    		angular.forEach($scope.myFiles, function(file){                 			
                    			fd.append('files', file);                    			
                    		});
                    		
                    		fd.append("name", $scope.name);
                    		fd.append("description", $scope.description);
                    		
                    		var createItem = 'api/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items';
                    		
                    
                    		
                 		$http.post(createItem, fd, {
                 			transformRequest : angular.identity,
                 			headers : {
                 				'Content-Type' : undefined
                 			}
                 		}).success(function(response) {
                 			if(response != null){
                 				$location.path('/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
                 			}
                 			
                 		}).error(function(response) {
                 			
                 		});               		
                    	};
                    	
                    	$scope.logout = function(){		
                    		logout.save();
                    		sessionStorage.clear();
                    		$location.path('/');	
                    	}
      	}]);

userServices.controller('editItemController',['$scope','$routeParams','AuthService','$route','$location','$http','Items','Photos','updateItemInfo','$rootScope','Boxes','logout',
                                              function($scope,$routeParams,AuthService,$route,$location,$http,Items,Photos,updateItemInfo,$rootScope,Boxes,logout){
					
			if(sessionStorage.getItem("userId")==null){
				$location.path('/');		
			}
			
			$scope.userId = $routeParams.userId;
		  	$scope.boxId = $routeParams.boxId;
		  	$scope.itemId = $routeParams.itemId;
		  	$scope.user = AuthService.getUser();
			$scope.username = $scope.user.userName;
			var initialItemName;
			var initialItemDescription;
			
			$scope.currentPage = 1;
			
			$scope.gotoPage = function() {
				$scope.photos = Photos.query( {userId:$scope.userId, boxId:$scope.boxId, itemId:$scope.itemId, pageNum : $scope.currentPage });
			}
			
		  	
			$scope.photos = Photos.query({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId})
			
			Items.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId},function(response){
				$scope.item = response;
				initialItemName = $scope.item.name;
				initialItemDescription = $scope.item.description;
			});
			
			Boxes.get({userId:$scope.userId,boxId:$scope.boxId}, function(response){
				if(response.userImage != null ){
					$scope.userImageId = response.userImage.id;
					$scope.boxImage = 'api/users/'+$scope.userId+'/userImage/'+$scope.userImageId;
				}
				
			});
		  	
		  	$scope.updateItemInfo = function(){
				updateItemInfo.update({
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
				},$scope.item)}
			
			$scope.changeName = function(){
				$scope.updateItemInfo();
				initialItemName = $scope.item.name;
				$scope.change = false;
			}
			
			$scope.cancelName = function(){
				$scope.item.name = initialItemName;
				$scope.change = false;
			}
			
			
			$scope.changeDescription = function(){
				$scope.updateItemInfo();
				initialItemDescription = $scope.item.description;
				$scope.changeDes = false;
			}
			$scope.cancelDescription = function(){
				$scope.item.description = initialItemDescription;
				$scope.changeDes = false;
			}
		  	
		  	
		  	
			$scope.filesChanged = function(elm){
				$scope.showInfo = false;
				
        		$scope.files = elm.files;
        		$scope.$apply();
        	}        
        	
        	$scope.uploadFile = function() {
        		if($scope.files == null){
        			$scope.showInfo = true;
        			$scope.Info = "please choose file";
        		}
        		
         		
         		if($scope.files != null){
             	var fd = new FormData();               		
            	angular.forEach($scope.files, function(file){                 			
            		fd.append('files', file);              			
            	});
        	
        		
        	var uploadUrl = 'api/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items/'+$scope.itemId+'/photos';
        		
        	console.log("url: "+uploadUrl);
        	
     		$http.post(uploadUrl, fd, {
     			transformRequest : angular.identity,
     			headers : {
     				'Content-Type' : undefined
     			}
     		}).success(function(response) {
     			
     			
//     			if(!$scope.disabled){
//     				$scope.item = Items.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId});
//     				photoNumPerItem.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId},function(response){
//     					$scope.photoNumPerItem =  response.photoNumPerItem;
//     				});
					$scope.photos = Photos.query({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId})
//     			}
     	    }).error(function(response) {
     			
     		});               		
        	};	
        	}
		  	
		  	
		  	$scope.deletePhoto = function(photoId){
				Photos.remove({
					userId:$scope.userId,
					boxId:$scope.boxId,
					itemId:$scope.itemId,
					photoId:photoId
				},function(response){
					if(response != null){	
						$scope.item = response;	
//						photoNumPerItem.get({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId},function(response){
//	     					$scope.photoNumPerItem =  response.photoNumPerItem;
//	     				});
						$scope.photos = Photos.query({userId:$scope.userId,boxId:$scope.boxId,itemId:$scope.itemId})
					}
					
				})
			}   
        	
		  	$scope.editItem = function(){	
					Items.update({
						userId: $scope.userId,
						boxId:$scope.boxId,
						itemId:$scope.itemId
						}, $scope.item, function(response){
							if(response != null){
								$location.path('/users/'+$scope.userId+'/boxes/'+$scope.boxId+'/items');
							}
					});		
			}	
		  	
		  	$scope.logout = function(){		
				logout.save();
				sessionStorage.clear();
				$location.path('/');	
			}
}]);


userServices.factory('UserBoxNum',['$resource',function($resource){
	return $resource('api/users/:userId/userBoxNum');
}]);

userServices.factory('UserItemNum',['$resource',function($resource){
	return $resource('api/users/:userId/userItemNum');
}]);

userServices.factory('userItems',['$resource',function($resource){
	return $resource('api/users/:userId/items')
}]);

userServices.factory('SearchBox',['$resource',function($resource){
	return $resource('api/users/:userId/searchBox');
}]);

userServices.factory('SearchItem',['$resource',function($resource){
	return $resource('api/users/:userId/searchItem');
}]);


userServices.controller('searchController',['$scope','$routeParams','AuthService','$route','$location','SearchBox','SearchItem','Boxes','userItems','UserBoxNum','UserItemNum','Items','logout',
                                               function($scope,$routeParams,AuthService,$route,$location,SearchBox,SearchItem,Boxes,userItems,UserBoxNum,UserItemNum,Items,logout){
		
		if(sessionStorage.getItem("userId")==null){
			$location.path('/');		
		}
	
		$scope.userId = $routeParams.userId;
		$scope.user = AuthService.getUser();
		$scope.username = $scope.user.userName;
       	
		$scope.currentBoxPage = 1;
		$scope.currentItemPage = 1;
		$scope.userBoxNum = 0;
		$scope.userItemNum = 0;
		$scope.search="";
		
		
		$scope.boxes = Boxes.query({userId:$scope.userId,pageNum:$scope.currentBoxPage});
		
		$scope.items = userItems.query({userId:$scope.userId,pageNum:$scope.currentItemPage});
			
		UserBoxNum.get({userId:$scope.userId,search:$scope.search},function(response){
			$scope.userBoxNum = response.userBoxNum; 
		});
	
	    UserItemNum.get({userId:$scope.userId,search:$scope.search},function(response){
	    	$scope.userItemNum = response.userItemNum; 
	    });
	    
			
		$scope.gotoBoxPage = function() {
				$scope.checkModel=[];
			    SearchBox.query({
	    			userId:$scope.userId,
	    			search:$scope.search,
	    			pageNum:$scope.currentBoxPage
	    		},function(response){
	    			if(response != null){
	    				$scope.boxes = response;
	    			}
	    		})
		}
		
		$scope.gotoItemPage = function() {
			  $scope.checkModel=[];
				SearchItem.query({
	    			userId:$scope.userId,
	    			search:$scope.search,
	    			pageNum:$scope.currentItemPage
	    		},function(response){
	    			if(response != null){
	    				$scope.items = response;
	    			}
	    		})
		}
		
		$scope.deleteBox = function(boxId){
			Boxes.remove({
				userId:$scope.userId,
				boxId:boxId,
				pageNum: $scope.currentBoxPage
			},function(response){
				if(response != null){
					$scope.boxes = response.userBoxes;
				}else{
					alert("delete failed");
				}
			});
		};
	
	
		$scope.deleteItem = function(boxId,itemId){
			Items.remove({
				userId:$scope.userId,
				boxId:boxId,
				itemId:itemId,
				pageNum:$scope.currentItemPage
			},function(response){
				$scope.items = response.userItems;
			})
		}
		
    	$scope.searchProduct = function(){
    		
    		if($scope.search==null || $scope.search==""){
    			$scope.boxes = Boxes.query({userId:$scope.userId,pageNum:$scope.currentBoxPage});	
    			$scope.items = userItems.query({userId:$scope.userId,pageNum:$scope.currentItemPage});
    		}else{
    			
    			UserBoxNum.get({userId:$scope.userId,search:$scope.search},function(response){
    				$scope.userBoxNum = response.userBoxNum; 
    			});
    			
    			SearchBox.query({
	    			userId:$scope.userId,
	    			search:$scope.search,
	    			pageNum:$scope.currentBoxPage
	    		},function(response){
	    			if(response != null){
	    				$scope.boxes = response;
	    			}
	    		})
	    		
	    		
	    		
	    		
	    		 UserItemNum.get({userId:$scope.userId,search:$scope.search},function(response){
	    			 	$scope.userItemNum = response.userItemNum; 
	    		 });
    			
	    		SearchItem.query({
	    			userId:$scope.userId,
	    			search:$scope.search,
	    			pageNum:$scope.currentItemPage
	    		},function(response){
	    			if(response != null){
	    				$scope.items = response;
	    			}
	    		})
	    	}
    	};
    
     	
    	$scope.checkArr = [];
     	$scope.checkModel_a={};
 	    $scope.syanCheckClick=function(box){ 	    
 	    if(box.userImage != undefined){
 	    	$scope.checkModel_a[box.id] = !$scope.checkModel_a[box.id];
	         if($scope.checkModel_a[box.id]===true){  // 判断这个选项是true
	             if($scope.checkArr.indexOf(box.id)===-1){ //  这里判断数组里没有这个id，要把他加到数组里；
	                 $scope.checkArr.push(box.id)
	             }
	         }else{ 
	             if($scope.checkArr.indexOf(box.id)!==-1){//数组里有这个id，将其删除。
	                  $scope.checkArr.splice($scope.checkArr.indexOf(box.id),1);
	             }
	         }
	        console.log($scope.checkArr);
	       $scope.urlAddress='api/users/'+$scope.userId+'/labels'+'?boxes='+$scope.checkArr;
 	    }   	 	    	 	    	 
 	   }
     	
 	   $scope.logout = function(){		
 			logout.save();
 			sessionStorage.clear();
 			$location.path('/');	
 		}
}]);


userServices.directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {	
			
			element.bind('change', function() {
				$parse(attrs.fileModel).assign(scope, element[0].files);
				scope.$apply();
			});
		}
	};
} ]);



