var mainApp = angular.module('mainApp', ['ngSanitize', 'ngRoute', 'userServices','ui.bootstrap','ngAnimate']); //, 'spring-security-csrf-token-interceptor']);

mainApp.controller('homeControl', [function() {}]);

mainApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
	when('/', {
		templateUrl : 'views/login.html',
		controller : 'LoginController',
		activeTab : ''
	}).
	when('/users/addUser', {
		templateUrl: 'views/register.html',
		controller: 'userRegisterController',
		activeTab : 'home'
	}).
	when('/users/:userId/profile', {
		templateUrl: 'views/profile.html',
		controller: 'userController',
		activeTab : 'home'
	}).
	when('/users/:userId/boxes', {
		templateUrl : 'views/userBoxes.html',
		controller : 'boxesController',
		activeTab : 'home'
	}).
	when('/users/:userId/newBox',{
		templateUrl:'views/newBox.html',
		controller: 'createBoxController',
		activeTab:'home'
	}).
	when('/users/:userId/boxes/:boxId',{
		templateUrl:'views/newBox.html',
		controller:'createBoxController',
		activeTab:'home'
	}).
	when('/users/:userId/boxes/:boxId/items',{
		templateUrl:'views/boxItems.html',
		controller:'editBoxAndItemController',
		activeTab:'home'
	}).
	when('/users/:userId/boxes/:boxId/newItem',{
		templateUrl:'views/newItem.html',
		controller:'createItemController',
		activeTab:'home'
	}).
	when('/users/:userId/search',{
		templateUrl:'views/search.html',
		controller:'searchController',
		activeTab:'home'
	}).
	when('/users/:userId/boxes/:boxId/items/:itemId/editItem',{
		templateUrl:'views/editItem.html',
		controller:'editItemController',
		activeTab:'home'
	})
}]);
 